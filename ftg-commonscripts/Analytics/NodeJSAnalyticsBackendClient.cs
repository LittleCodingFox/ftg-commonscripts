﻿#if USE_JSONNET
using Newtonsoft.Json;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Analytics
{
    public class NodeJSAnalyticsBackendClient
    {
        [System.Serializable]
        private class Configuration
        {
            public string baseURL;
            public string newSessionPath;
            public string endSessionPath;
            public string eventPath;
            public int siteId;
            public bool debug = false;
        }

        [System.Serializable]
        private class Response
        {
            public bool success;
        }

        private Configuration configuration;

        public bool HasConfiguration
        {
            get
            {
                return configuration != null;
            }
        }

        public string MakeSessionID()
        {
            return System.Guid.NewGuid().ToString();
        }

        public bool LoadConfiguration(string path)
        {
            try
            {
                var contents = System.IO.File.ReadAllText(path);

#if USE_JSONNET
                configuration = JsonConvert.DeserializeObject<Configuration>(contents);
#else
                configuration = JsonUtility.FromJson<Configuration>(contents);
#endif

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public void NewSession(string sessionId, System.Action successHandler, System.Action failureHandler)
        {
            if (!HasConfiguration || sessionId?.Length == 0)
            {
                failureHandler?.Invoke();

                return;
            }

            CoroutineHelper.Instance.StartCoroutine(NewSessionCoroutine(sessionId, successHandler, failureHandler));
        }

        public void EndSession(string sessionId, System.Action successHandler, System.Action failureHandler)
        {
            if (!HasConfiguration || sessionId?.Length == 0)
            {
                failureHandler?.Invoke();

                return;
            }

            CoroutineHelper.Instance.StartCoroutine(EndSessionCoroutine(sessionId, successHandler, failureHandler));
        }

        public void TrackEvent(string sessionId, string type, Dictionary<string, object> parameters, System.Action successHandler, System.Action failureHandler)
        {
            if (!HasConfiguration || sessionId?.Length == 0)
            {
                failureHandler?.Invoke();

                return;
            }

            CoroutineHelper.Instance.StartCoroutine(TrackEventCoroutine(sessionId, type, parameters, successHandler, failureHandler));
        }

        #region Coroutines
        private IEnumerator NewSessionCoroutine(string sessionId, System.Action successHandler, System.Action failureHandler)
        {
            var request = UnityWebRequest.Post($"{configuration.baseURL}{configuration.newSessionPath}", new Dictionary<string, string>()
            {
                { "siteId", configuration.siteId.ToString() },
                { "sessionId", sessionId }
            });

            if(configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Requesting {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)}");
            }

            yield return request.SendWebRequest();

            if(request.isNetworkError || request.isHttpError)
            {
                failureHandler?.Invoke();

                if (configuration.debug)
                {
                    Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                }

                yield break;
            }

            var jsonData = request.downloadHandler.text;

            if (configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Request {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)} received response {jsonData}");
            }

            try
            {
#if USE_JSONNET
                var response = JsonConvert.DeserializeObject<Response>(jsonData);
#else
                var response = JsonUtility.FromJson<Response>(jsonData);
#endif
                if (response == null || response.success == false)
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                    }

                    failureHandler?.Invoke();
                }
                else
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} succeeded");
                    }

                    successHandler?.Invoke();
                }
            }
            catch (System.Exception)
            {
                if (configuration.debug)
                {
                    Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                }

                failureHandler?.Invoke();
            }
        }

        private IEnumerator EndSessionCoroutine(string sessionId, System.Action successHandler, System.Action failureHandler)
        {
            var request = UnityWebRequest.Post($"{configuration.baseURL}{configuration.endSessionPath}", new Dictionary<string, string>()
            {
                { "siteId", configuration.siteId.ToString() },
                { "sessionId", sessionId }
            });

            if (configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Requesting {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)}");
            }

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                failureHandler?.Invoke();

                yield break;
            }

            var jsonData = request.downloadHandler.text;

            if (configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Request {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)} received response {jsonData}");
            }

            try
            {
#if USE_JSONNET
                var response = JsonConvert.DeserializeObject<Response>(jsonData);
#else
                var response = JsonUtility.FromJson<Response>(jsonData);
#endif
                if (response == null || response.success == false)
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                    }

                    failureHandler?.Invoke();
                }
                else
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} succeeded");
                    }

                    successHandler?.Invoke();
                }
            }
            catch (System.Exception)
            {
                if (configuration.debug)
                {
                    Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                }

                failureHandler?.Invoke();
            }
        }

        private IEnumerator TrackEventCoroutine(string sessionId, string type, Dictionary<string, object> parameters, System.Action successHandler, System.Action failureHandler)
        {
            var form = new Dictionary<string, string>()
            {
                { "siteId", configuration.siteId.ToString() },
                { "sessionId", sessionId },
                { "type", type },
            };

            try
            {
#if USE_JSONNET
                var payload = JsonConvert.SerializeObject(parameters);
#else
                var payload = JsonUtility.ToJson(parameters);
#endif

                form.Add("data", payload);
            }
            catch (System.Exception)
            {
                if (configuration.debug)
                {
                    Debug.Log($"[{GetType().Name}: Failed to encode payload for Track Event");
                }

                failureHandler?.Invoke();

                yield break;
            }

            var request = UnityWebRequest.Post($"{configuration.baseURL}{configuration.eventPath}", form);

            if (configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Requesting {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)}");
            }

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                failureHandler?.Invoke();

                yield break;
            }

            var jsonData = request.downloadHandler.text;

            if (configuration.debug)
            {
                Debug.Log($"[{GetType().Name}: Request {request.url} with form {System.Text.Encoding.UTF8.GetString(request.uploadHandler.data)} received response {jsonData}");
            }

            try
            {
#if USE_JSONNET
                var response = JsonConvert.DeserializeObject<Response>(jsonData);
#else
                var response = JsonUtility.FromJson<Response>(jsonData);
#endif
                if (response == null || response.success == false)
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                    }

                    failureHandler?.Invoke();
                }
                else
                {
                    if (configuration.debug)
                    {
                        Debug.Log($"[{GetType().Name}: Request {request.url} succeeded");
                    }

                    successHandler?.Invoke();
                }
            }
            catch (System.Exception)
            {
                if (configuration.debug)
                {
                    Debug.Log($"[{GetType().Name}: Request {request.url} failed");
                }

                failureHandler?.Invoke();
            }
        }
        #endregion
    }
}
