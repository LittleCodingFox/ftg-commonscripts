﻿#if DISABLE_ANALYTICS
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Analytics
{
    internal static class DisableAnalytics
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void DisableAll()
        {
            UnityEngine.Analytics.Analytics.initializeOnStartup = false;
            UnityEngine.Analytics.Analytics.enabled = false;
            UnityEngine.Analytics.Analytics.deviceStatsEnabled = false;
            UnityEngine.Analytics.PerformanceReporting.enabled = false;
        }
    }
}
#endif
