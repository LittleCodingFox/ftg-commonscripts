﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Analytics
{
    /// <summary>
    /// Google analytics for apps.
    /// using the new "measurement protocol"
    /// see https://developers.google.com/analytics/devguides/collection/protocol/v1/
    /// 
    /// Requires a Google Analytics Account and an app profile for the tracking.
    /// see https://support.google.com/analytics/answer/2614741?hl=en
    /// </summary>
    public class GoogleAnalyticsManager : MonoBehaviour
    {
        public string appVersion;
        public string appName;
        public string trackingId;

        private string customerId;
        private string screenResolution;
        private string viewportSize;
        private string userLanguage;

        private Dictionary<int, int> customMetrics = new Dictionary<int, int>();
        private Dictionary<int, string> customDimensions = new Dictionary<int, string>();

        private static readonly string googleURL = "http://www.google-analytics.com/collect";

        public static GoogleAnalyticsManager Instance
        {
            get;
            private set;
        }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);

                customerId = SystemInfo.deviceUniqueIdentifier;

                //note: this happens on BB10 for example when the "Device ID" flag was not set in the player
                if (customerId.Length == 0)
                {
                    var pseudoRandom = (Time.time * 1000.0f).ToString() + (new System.Random()).Next();

                    customerId = "rnd." + pseudoRandom.GetHashCode().ToString();
                }

                screenResolution = Screen.width + "x" + Screen.height;
                viewportSize = Screen.currentResolution.width + "x" + Screen.currentResolution.height;
                userLanguage = Application.systemLanguage.ToString();
            }
        }

        /// <summary>
        /// Sets the customer identifier by hand. Default is using SystemInfo.deviceUniqueIdentifier as the customerId
        /// </summary>
        /// <param name='customerId'>
        /// _customer identifier.
        /// </param>
        public void SetCustomerId(string customerId)
        {
            this.customerId = customerId;
        }

        public void SetVersion(string version)
        {
            this.appVersion = version;
        }

        /// <summary>
        /// Tracks an event.
        /// 
        /// for detailed parameter desc:
        /// https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#events
        /// </param>
        public void TrackEvent(string category, string label, string action, int value)
        {
            if(Application.isEditor)
            {
                Debug.LogFormat("[GoogleAnalytics] - Tracking Event in Editor - type: {0} category: {1} label: {2} action: {3} value: {4}", "event", category, label, action, value);

                return;
            }

            var postParams = new WWWForm();

            AddDefaultFields(ref postParams);

            postParams.AddField("t", "event");
            postParams.AddField("ec", category);
            postParams.AddField("el", label);
            postParams.AddField("ea", action);
            postParams.AddField("ev", value);

            MakeRequest(postParams);
        }

        public void TrackSession(bool start)
        {
            if (Application.isEditor)
            {
                Debug.LogFormat("[GoogleAnalytics] - Tracking Session {0} in Editor", start ? "Start" : "End");

                return;
            }

            var postParams = new WWWForm();

            AddDefaultFields(ref postParams);

            if (start)
            {
                postParams.AddField("sc", "start");
            }
            else
            {
                postParams.AddField("sc", "end");
            }

            MakeRequest(postParams);
        }

        public void TrackAppview(string contentDescription)
        {
            if (Application.isEditor)
            {
                Debug.LogFormat("[GoogleAnalytics] - Tracking Appview in Editor - {0}", contentDescription);

                return;
            }

            var postParams = new WWWForm();

            AddDefaultFields(ref postParams);

            postParams.AddField("t", "appview");
            postParams.AddField("cd", contentDescription);

            MakeRequest(postParams);
        }

        // not working correctly: this appears as a separat user
        public void TrackTiming(string category, string userTimingVarName, string timingLabel, int timeInMS)
        {
            if (Application.isEditor)
            {
                Debug.LogFormat("[GoogleAnalytics] - Tracking Timing in Editor - type: {0} category: {1} user timing var: {2} user timing label {3} user timing time: {4}", "timing", category, userTimingVarName, timingLabel, timeInMS);

                return;
            }

            var postParams = new WWWForm();

            AddDefaultFields(ref postParams);

            postParams.AddField("t", "timing");
            postParams.AddField("utc", category);
            postParams.AddField("utv", userTimingVarName);
            postParams.AddField("utl", timingLabel);
            postParams.AddField("utt", timeInMS);

            MakeRequest(postParams);
        }

        public void SetCustomMetric(int metricId, int metricValue)
        {
            customMetrics[metricId] = metricValue;
        }

        public void SetCustomDimension(int dimensionId, string dimensionValue)
        {
            if (dimensionValue.Length > 150)
                throw new Exception("Google Analytics dimension value too long, limit is 150 bytes");

            customDimensions[dimensionId] = dimensionValue;
        }

        private void AddDefaultFields(ref WWWForm form)
        {
            form.AddField("v", 1);
            form.AddField("tid", trackingId);
            form.AddField("an", appName);
            form.AddField("av", appVersion);
            form.AddField("cid", customerId);
            form.AddField("sr", screenResolution);
            form.AddField("vp", viewportSize);
            form.AddField("ul", userLanguage);
            form.AddField("dp", "/index");

            foreach (var pair in customMetrics)
            {
                form.AddField("cm" + pair.Key.ToString(), pair.Value);
            }

            customMetrics.Clear();

            foreach (var pair in customDimensions)
            {
                form.AddField("cd" + pair.Key.ToString(), pair.Value);
            }

            customDimensions.Clear();
        }

        private void MakeRequest(WWWForm form)
        {
            var request = UnityWebRequest.Post(googleURL, form);

            StartCoroutine(WaitForRequest(request));
        }

        private IEnumerator WaitForRequest(UnityWebRequest request)
        {
            yield return request.SendWebRequest();

            // check for errors
            if (request.error != null)
            {
                Debug.LogWarningFormat("Google Analytics Error: {0}", request.error);
            }

            if(request.downloadHandler.text != null)
            {
                //Debug.LogFormat("Google Analytics Result: {0}", request.downloadHandler.text);
            }
        }
    }
}
