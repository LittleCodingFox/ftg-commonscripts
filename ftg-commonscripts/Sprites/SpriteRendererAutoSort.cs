﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SpriteRendererAutoSort : MonoBehaviour
    {
        private SpriteRenderer[] spriteRenderers;

        private void Start()
        {
            Refresh();
        }

        private void FixedUpdate()
        {
            if(spriteRenderers != null)
            {
                foreach(var renderer in spriteRenderers)
                {
                    if(renderer == null)
                    {
                        continue;
                    }

                    renderer.sortingOrder = (int)(renderer.transform.position.y * -100);
                }
            }
        }

        public void Refresh()
        {
            spriteRenderers = transform.GetComponentsInSelfAndChildren<SpriteRenderer>();
        }
    }
}
