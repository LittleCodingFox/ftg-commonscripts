﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    [ExecuteInEditMode]
    public class SpriteQuadAssigner : MonoBehaviour
    {
        [Serializable]
        public class SpriteInfo
        {
            public Sprite sprite;
            public MeshRenderer meshRenderer;
        }

        public List<SpriteInfo> items = new List<SpriteInfo>();
        public Material material;

        private void OnEnable()
        {
            Refresh();
        }

        public void Refresh()
        {
            foreach (var item in items)
            {
                if (item.meshRenderer == null || item.sprite == null)
                {
                    continue;
                }

                var newMaterial = new Material(material);

                newMaterial.SetTexture("_MainTex", item.sprite.texture);
                newMaterial.SetTextureOffset("_MainTex", new Vector2(item.sprite.textureRect.x / item.sprite.texture.width,
                    item.sprite.textureRect.y / item.sprite.texture.height));
                newMaterial.SetTextureScale("_MainTex", new Vector2(item.sprite.textureRect.width / item.sprite.texture.width,
                    item.sprite.textureRect.height / item.sprite.texture.height));

                item.meshRenderer.sharedMaterial = newMaterial;
            }
        }
    }
}
