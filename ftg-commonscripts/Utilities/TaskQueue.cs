using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public interface ITaskQueueItem
    {
        WeakReference<object> DistinctObject { get; }

        bool IsValid();

        void Process();
    }

    public class TaskQueue : MonoBehaviour
    {
        public delegate void TaskQueueCompleteHandler();

        private const float targetFramerate = 30 / 1000.0f;

        public static float TaskDelay(float spentTime, float budget)
        {
            if(spentTime > budget)
            {
                return spentTime - budget;
            }

            return 0.01f;
        }

        public static TaskQueue Get(string name, bool distinctObjects)
        {
            var gameObject = GameObject.Find(name);

            if(gameObject != null)
            {
                return gameObject.GetComponent<TaskQueue>();
            }

            gameObject = new GameObject(name);

            var taskQueue = gameObject.AddComponent<TaskQueue>();

            taskQueue.distinctObjects = distinctObjects;

            return taskQueue;
        }

        private List<ITaskQueueItem> queue = new List<ITaskQueueItem>();
        private bool distinctObjects = false;

        public int TaskCount = 0;

        public TaskQueueCompleteHandler OnComplete;

        private void Awake()
        {
            StartCoroutine(QueueCoroutine());
        }

        public void Queue(ITaskQueueItem item)
        {
            object objectA = null;
            object objectB = null;

            if (item.DistinctObject == null || item.DistinctObject.TryGetTarget(out objectA) == false || objectA == null)
            {
                return;
            }

            if(distinctObjects)
            {
                for (var i = queue.Count - 1; i >= 0; i--)
                {
                    var remove = queue[i].DistinctObject == null || queue[i].DistinctObject.TryGetTarget(out objectB) == false || objectA == objectB;

                    if (remove)
                    {
                        queue.RemoveAt(i);

                        continue;
                    }
                }
            }

            queue.Add(item);
        }

        private IEnumerator QueueCoroutine()
        {
            for(; ; )
            {
                TaskCount = queue.Count;

                var time = Time.realtimeSinceStartup;

                while (queue.Count > 0)
                {
                    var item = queue.FirstOrDefault();

                    queue.RemoveAt(0);

                    try
                    {
                        if (item.IsValid())
                        {
                            item.Process();

                            if (queue.Count == 0)
                            {
                                OnComplete?.Invoke();
                            }

                            break;
                        }
                    }
                    catch(System.Exception e)
                    {
                        Debug.LogError($"[TaskQueue] Task failed with exception: {e}");

                        break;
                    }
                }

                var elapsedTime = Time.realtimeSinceStartup - time;

                yield return new WaitForSecondsRealtime(TaskDelay(elapsedTime, targetFramerate));
            }
        }
    }
}
