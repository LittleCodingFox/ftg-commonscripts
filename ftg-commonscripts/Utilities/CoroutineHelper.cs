﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FlamingTorchGames.CommonScripts
{
    [ExecuteInEditMode]
    public class CoroutineHelper : MonoBehaviour
    {
        public static CoroutineHelper Instance
        {
            get
            {
                var helper = FindObjectOfType<CoroutineHelper>();

                if(helper == null)
                {
                    var instanceObject = new GameObject("Coroutine Helper");
                    instanceObject.hideFlags = HideFlags.HideAndDontSave;

                    if(Application.isPlaying)
                    {
                        DontDestroyOnLoad(instanceObject);
                    }

                    helper = instanceObject.AddComponent<CoroutineHelper>();
                }

                return helper;
            }
        }

        private static List<IEnumerator> pendingCoroutines = new List<IEnumerator>();

        private void OnEnable()
        {
#if UNITY_EDITOR
            EditorApplication.update += ExecuteCoroutine;
#endif
        }

        private void OnDisable()
        {
#if UNITY_EDITOR
            EditorApplication.update -= ExecuteCoroutine;
#endif
        }

        public new Coroutine StartCoroutine(IEnumerator coroutine)
        {
            if(Application.isPlaying)
            {
                return base.StartCoroutine(coroutine);
            }
            else
            {
                pendingCoroutines.Add(coroutine);

                return null;
            }
        }

#if UNITY_EDITOR
        static int currentExecute = 0;
        private static void ExecuteCoroutine()
        {
            if(pendingCoroutines.Count == 0)
            {
                return;
            }

            currentExecute = (currentExecute + 1) % pendingCoroutines.Count;

            var finished = !pendingCoroutines[currentExecute].MoveNext();

            if(finished)
            {
                pendingCoroutines.RemoveAt(currentExecute);
            }
        }
#endif
    }
}
