﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public static class TimedLog
    {
        public static bool enabled = true;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string Message(string message)
        {
            return $"[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}] {message}";
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Log(string message)
        {
            if(enabled)
            {
                Debug.Log(Message(message));
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogWarning(string message)
        {
            if (enabled)
            {
                Debug.LogWarning(Message(message));
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LogError(string message)
        {
            if (enabled)
            {
                Debug.LogError(Message(message));
            }
        }
    }
}
