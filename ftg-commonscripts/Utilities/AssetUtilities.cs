﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class AssetUtilities
        {
            /// <summary>
            /// Gets the current asset path
            /// </summary>
            /// <returns>The current asset path</returns>
            public static string CurrentAssetPath()
            {
                string path = AssetDatabase.GetAssetPath(Selection.activeObject);

                if (path == "")
                {
                    path = "Assets";
                }
                else if (Path.GetExtension(path) != "")
                {
                    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
                }

                return path;
            }

            /// <summary>
            /// Creates a scriptable object asset
            /// </summary>
            /// <typeparam name="T">The type of the asset</typeparam>
            /// <param name="path">The path for the asset</param>
            /// <param name="name">The name of the asset, minus the extension</param>
            /// <returns>The created object or null on failure</returns>
            public static T CreateScriptableObjectAsset<T>(string path, string name) where T : ScriptableObject
            {
                T asset = ScriptableObject.CreateInstance<T>();

                if(asset != null)
                {
                    string realPath = (path + "/" + name + ".asset").Replace("//", "/");

                    string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(realPath);

                    AssetDatabase.CreateAsset(asset, assetPathAndName);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();

                    asset = AssetDatabase.LoadAssetAtPath<T>(assetPathAndName);

                    return asset;
                }

                return null;
            }
        }
    }
}
#endif
