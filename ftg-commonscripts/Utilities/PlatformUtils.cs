﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FlamingTorchGames.CommonScripts
{
    public enum PlatformType
    {
        PC,
        Phone,
        Tablet,
        WebGL
    }

    public static class PlatformUtils
    {
        public static PlatformType? SimulatedPlatform
        {
            get
            {
#if UNITY_EDITOR
                if (System.Enum.TryParse<PlatformType>(EditorUserSettings.GetConfigValue("FTG_SIMULATED_PLATFORM"), out var platform))
                {
                    return platform;
                }
#endif

                return null;
            }
        }

        public static bool CurrentPlatformIsMobile
        {
            get
            {
                var currentPlatform = CurrentPlatform;

                return currentPlatform == PlatformType.Phone || currentPlatform == PlatformType.Tablet;
            }
        }

        public static PlatformType CurrentPlatform
        {
            get
            {
                var platform = Application.platform;

#if UNITY_EDITOR
                if(SimulatedPlatform != null)
                {
                    return SimulatedPlatform.Value;
                }

                switch (UnityEditor.EditorUserBuildSettings.activeBuildTarget)
                {
                    case UnityEditor.BuildTarget.Android:
                        platform = RuntimePlatform.Android;

                        break;

                    case UnityEditor.BuildTarget.iOS:
                        platform = RuntimePlatform.IPhonePlayer;

                        break;

                    case UnityEditor.BuildTarget.WebGL:
                        platform = RuntimePlatform.WebGLPlayer;

                        break;
                }
#endif

                switch (platform)
                {
                    case RuntimePlatform.Android:
                    case RuntimePlatform.IPhonePlayer:
                        if (Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height) / Screen.dpi >= 6.8f)
                        {
                            return PlatformType.Tablet;
                        }

                        return PlatformType.Phone;

                    case RuntimePlatform.LinuxEditor:
                    case RuntimePlatform.LinuxPlayer:
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.OSXPlayer:
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.WindowsPlayer:
                        return PlatformType.PC;

                    case RuntimePlatform.WebGLPlayer:
                        return PlatformType.WebGL;

                    default:
                        return PlatformType.PC;
                }
            }
        }
    }
}
