﻿#if USE_JSONNET
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Json
{
    public class ColorConverter : JsonConverter
    {
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Color) || objectType == typeof(Color32);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if(reader.TokenType == JsonToken.Null)
            {
                return new Color();
            }

            var jObject = JObject.Load(reader);

            var rValue = jObject.GetValue("r");
            var gValue = jObject.GetValue("g");
            var bValue = jObject.GetValue("b");
            var aValue = jObject.GetValue("a");

            Color32 color = new Color32();
            var isColor32 = false;
            var isColor = false;

            if(existingValue != null)
            {
                if(existingValue.GetType() == typeof(Color))
                {
                    color = (Color32)((Color)existingValue);
                    isColor = true;
                }
                else
                {
                    color = (Color32)existingValue;
                    isColor32 = true;
                }
            }

            color.r = (byte)((rValue == null || rValue.Type != JTokenType.Integer) ? 0 : rValue.Value<int>());
            color.g = (byte)((gValue == null || gValue.Type != JTokenType.Integer) ? 0 : gValue.Value<int>());
            color.b = (byte)((bValue == null || bValue.Type != JTokenType.Integer) ? 0 : bValue.Value<int>());
            color.a = (byte)((aValue == null || aValue.Type != JTokenType.Integer) ? 0 : aValue.Value<int>());

            if(isColor32)
            {
                return color;
            }

            if(isColor)
            {
                return (Color)color;
            }

            return color;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Color32 colorValue;

            if (value.GetType() == typeof(Color))
            {
                colorValue = (Color32)((Color)value);
            }
            else
            {
                colorValue = (Color32)value;
            }

            writer.WriteStartObject();
            writer.WritePropertyName("r");
            writer.WriteValue(colorValue.r);
            writer.WritePropertyName("g");
            writer.WriteValue(colorValue.g);
            writer.WritePropertyName("b");
            writer.WriteValue(colorValue.b);
            writer.WritePropertyName("a");
            writer.WriteValue(colorValue.a);
            writer.WriteEndObject();
        }
    }

    public class Vector2Converter : JsonConverter
    {
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Vector2);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return Vector2.zero;
            }

            var jObject = JObject.Load(reader);

            var xValue = jObject.GetValue("x");
            var yValue = jObject.GetValue("y");

            var vector2 = existingValue != null ? (Vector2)existingValue : new Vector2();

            vector2.x = (xValue == null || xValue.Type != JTokenType.Float) ? 0 : xValue.Value<float>();
            vector2.y = (yValue == null || yValue.Type != JTokenType.Float) ? 0 : yValue.Value<float>();

            return vector2;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector2Value = (Vector2)value;

            writer.WriteStartObject();
            writer.WritePropertyName("x");
            writer.WriteValue(vector2Value.x);
            writer.WritePropertyName("y");
            writer.WriteValue(vector2Value.y);
            writer.WriteEndObject();
        }
    }

    public class Vector3Converter : JsonConverter
    {
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Vector3);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return Vector3.zero;
            }

            var jObject = JObject.Load(reader);

            var xValue = jObject.GetValue("x");
            var yValue = jObject.GetValue("y");
            var zValue = jObject.GetValue("z");

            var vector3 = existingValue != null ? (Vector3)existingValue : new Vector3();

            vector3.x = (xValue == null || xValue.Type != JTokenType.Float) ? 0 : xValue.Value<float>();
            vector3.y = (yValue == null || yValue.Type != JTokenType.Float) ? 0 : yValue.Value<float>();
            vector3.z = (zValue == null || zValue.Type != JTokenType.Float) ? 0 : zValue.Value<float>();

            return vector3;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector3Value = (Vector3)value;

            writer.WriteStartObject();
            writer.WritePropertyName("x");
            writer.WriteValue(vector3Value.x);
            writer.WritePropertyName("y");
            writer.WriteValue(vector3Value.y);
            writer.WritePropertyName("z");
            writer.WriteValue(vector3Value.z);
            writer.WriteEndObject();
        }
    }
}

#endif