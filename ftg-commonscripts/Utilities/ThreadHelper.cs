﻿using System.Threading;

namespace FlamingTorchGames.CommonScripts
{
    public static class ThreadHelper
    {
        private static Thread mainThread = Thread.CurrentThread;

        public static bool IsMainThread => mainThread == Thread.CurrentThread;
    }
}
