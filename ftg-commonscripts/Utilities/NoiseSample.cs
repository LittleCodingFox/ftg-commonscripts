﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    /// <summary>
    /// Noise util sample
    /// Copied from tutorial by Catlike Coding: https://catlikecoding.com/unity/tutorials/simplex-noise/
    /// </summary>
    public struct NoiseUtilSample
    {
        public float value;
        public Vector3 derivative;

        public static NoiseUtilSample operator +(NoiseUtilSample a, float b)
        {
            a.value += b;
            return a;
        }

        public static NoiseUtilSample operator +(float a, NoiseUtilSample b)
        {
            b.value += a;
            return b;
        }

        public static NoiseUtilSample operator +(NoiseUtilSample a, NoiseUtilSample b)
        {
            a.value += b.value;
            a.derivative += b.derivative;
            return a;
        }

        public static NoiseUtilSample operator -(NoiseUtilSample a, float b)
        {
            a.value -= b;
            return a;
        }

        public static NoiseUtilSample operator -(float a, NoiseUtilSample b)
        {
            b.value = a - b.value;
            b.derivative = -b.derivative;
            return b;
        }

        public static NoiseUtilSample operator -(NoiseUtilSample a, NoiseUtilSample b)
        {
            a.value -= b.value;
            a.derivative -= b.derivative;
            return a;
        }

        public static NoiseUtilSample operator *(NoiseUtilSample a, float b)
        {
            a.value *= b;
            a.derivative *= b;
            return a;
        }

        public static NoiseUtilSample operator *(float a, NoiseUtilSample b)
        {
            b.value *= a;
            b.derivative *= a;
            return b;
        }

        public static NoiseUtilSample operator *(NoiseUtilSample a, NoiseUtilSample b)
        {
            a.derivative = a.derivative * b.value + b.derivative * a.value;
            a.value *= b.value;
            return a;
        }
    }
}