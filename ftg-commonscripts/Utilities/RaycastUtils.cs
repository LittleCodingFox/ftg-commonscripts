﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public static class RaycastUtils
    {
        public static Vector2 MoveCharacter(GameObject self, Vector2 movement, BoxCollider2D collider, int collisionLayers, int horizontalRays = 4, int verticalRays = 4)
        {
            var position = self.transform.position;

            var moveX = movement.x;
            var moveY = movement.y;

            position = MoveHorizontal(self, position, collider, moveX, collisionLayers, horizontalRays);
            position = MoveVertical(self, position, collider, moveY, collisionLayers, verticalRays);

            return position;
        }

        public static GameObject[] RaycastCharacterHorizontal(GameObject self, Vector2 selfPosition, BoxCollider2D collider, float distance, int collisionLayers, int rayCount)
        {
            var direction = Mathf.Sign(distance);
            distance = Mathf.Abs(distance);

            var stepSize = collider.size.y * collider.transform.lossyScale.y / rayCount;

            var outObjects = new List<GameObject>();

            for (float step = -rayCount / 2; step <= rayCount / 2; step++)
            {
                var position = new Vector2(selfPosition.x + collider.offset.x * collider.transform.lossyScale.x, selfPosition.y + collider.offset.y * collider.transform.lossyScale.y + stepSize * step);

                Debug.DrawRay(position, new Vector2(direction * (distance + collider.size.x * collider.transform.lossyScale.x / 2), 0), Color.blue);

                var hits = Physics2D.RaycastAll(position, new Vector2(direction, 0), distance + collider.size.x * collider.transform.lossyScale.x / 2.0f, collisionLayers)
                    .OrderBy(x => x.distance)
                    .ToArray();

                foreach (var hit in hits)
                {
                    if (hit.collider.gameObject != self)
                    {
                        outObjects.Add(hit.collider.gameObject);
                    }
                }
            }

            return outObjects.ToArray();
        }

        public static GameObject[] RaycastCharacterVertical(GameObject self, Vector2 selfPosition, BoxCollider2D collider, float distance, int collisionLayers, int rayCount)
        {
            var direction = Mathf.Sign(distance);
            distance = Mathf.Abs(distance);

            var stepSize = collider.size.x * collider.transform.lossyScale.x / rayCount;

            var outObjects = new List<GameObject>();

            for (float step = -rayCount / 2; step <= rayCount / 2; step++)
            {
                var position = new Vector2(selfPosition.x + collider.offset.x * collider.transform.lossyScale.x + stepSize * step, selfPosition.y + collider.offset.y * collider.transform.lossyScale.y);

                Debug.DrawRay(position, new Vector2(0, direction * (distance + collider.size.y * collider.transform.lossyScale.y / 2)), Color.red);

                var hits = Physics2D.RaycastAll(position, new Vector2(0, direction), distance + collider.size.y * collider.transform.lossyScale.y / 2.0f, collisionLayers)
                    .OrderBy(x => x.distance)
                    .ToArray();

                foreach (var hit in hits)
                {
                    if (hit.collider.gameObject != self)
                    {
                        outObjects.Add(hit.collider.gameObject);
                    }
                }
            }

            return outObjects.ToArray();
        }

        private static Vector2 MoveHorizontal(GameObject self, Vector2 selfPosition, BoxCollider2D collider, float distance, int collisionLayers, int rayCount)
        {
            var hitObjects = RaycastCharacterHorizontal(self, selfPosition, collider, distance, collisionLayers, rayCount);

            var direction = Mathf.Sign(distance);
            distance = Mathf.Abs(distance);

            if (hitObjects.Length != 0)
            {
                return selfPosition;
            }

            return new Vector2(selfPosition.x + distance * direction, selfPosition.y);
        }

        private static Vector2 MoveVertical(GameObject self, Vector2 selfPosition, BoxCollider2D collider, float distance, int collisionLayers, int rayCount)
        {
            var hitObjects = RaycastCharacterVertical(self, selfPosition, collider, distance, collisionLayers, rayCount);

            var direction = Mathf.Sign(distance);
            distance = Mathf.Abs(distance);

            if (hitObjects.Length != 0)
            {
                return selfPosition;
            }

            return new Vector2(selfPosition.x, selfPosition.y + distance * direction);
        }
    }
}
