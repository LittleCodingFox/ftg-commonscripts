﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class PlatformSpecificActivator : MonoBehaviour
    {
        public List<PlatformType> invalidPlatforms = new List<PlatformType>();

        private void Awake()
        {
            var isValid = !invalidPlatforms.Any(x => x == PlatformUtils.CurrentPlatform);

            gameObject.SetActive(isValid);
        }
    }
}
