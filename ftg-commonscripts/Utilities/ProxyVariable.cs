﻿using System;

namespace FlamingTorchGames.CommonScripts
{
    public class ProxyVariable<T>
    {
        private Func<T> getter;
        private Action<T> setter;

        public ProxyVariable(Func<T> getFunction, Action<T> setFunction)
        {
            this.getter = getFunction;
            this.setter = setFunction;
        }

        public T Value
        {
            get
            {
                return getter();
            }

            set
            {
                setter?.Invoke(value);
            }
        }
    }
}
