﻿#if USING_DISCORD_RICH_PRESENCE
using Discord;
#endif
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class DiscordRichPresence : MonoBehaviour
    {
#if USING_DISCORD_RICH_PRESENCE
        private Discord.Discord discord;
        private Activity richPresence = new Activity();

        [HideInInspector]
        public User joinRequest;

        public System.Action<User> onJoinRequest;
        public System.Action<string> onJoin;
#endif

        public long applicationId;
        public uint optionalSteamId;

        public static DiscordRichPresence Instance
        {
            get
            {
                return FindObjectOfType<DiscordRichPresence>();
            }
        }

#if USING_DISCORD_RICH_PRESENCE
        public long StartTimestamp
        {
            get
            {
                if (discord == null)
                {
                    return 0;
                }

                return richPresence.Timestamps.Start;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Timestamps.Start = value;
            }
        }

        public string State
        {
            get
            {
                if (discord == null)
                {
                    return "";
                }

                return richPresence.State;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.State = value;
            }
        }

        public string Details
        {
            get
            {
                if (discord == null)
                {
                    return "";
                }

                return richPresence.Details;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Details = value;
            }
        }

        public string LargeImageKey
        {
            get
            {
                if (discord == null)
                {
                    return "";
                }

                return richPresence.Assets.LargeImage;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Assets.LargeImage = value;
            }
        }

        public string PartyId
        {
            get
            {
                if (discord == null)
                {
                    return "";
                }

                return richPresence.Party.Id;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Party.Id = value;
            }
        }

        public int PartySize
        {
            get
            {
                if (discord == null)
                {
                    return 0;
                }

                return richPresence.Party.Size.CurrentSize;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Party.Size.CurrentSize = value;
            }
        }

        public int PartyMax
        {
            get
            {
                if (discord == null)
                {
                    return 0;
                }

                return richPresence.Party.Size.MaxSize;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Party.Size.MaxSize = value;
            }
        }

        public string PartyJoinSecret
        {
            get
            {
                if (discord == null)
                {
                    return "";
                }

                return richPresence.Secrets.Join;
            }

            set
            {
                if (discord == null)
                {
                    return;
                }

                richPresence.Secrets.Join = value;
            }
        }

        public void UpdatePresence()
        {
            if(discord == null)
            {
                return;
            }

            discord.GetActivityManager().UpdateActivity(richPresence, (result) => { });
        }

        public void RequestRespondYes()
        {
            if (discord == null)
            {
                return;
            }

            Debug.Log("Discord: responding yes to Ask to Join request");
            discord.GetActivityManager().SendRequestReply(joinRequest.Id, ActivityJoinRequestReply.Yes, (result) => { });
        }

        public void RequestRespondNo()
        {
            if (discord == null)
            {
                return;
            }

            Debug.Log("Discord: responding no to Ask to Join request");
            discord.GetActivityManager().SendRequestReply(joinRequest.Id, ActivityJoinRequestReply.No, (result) => { });
        }

        public void ReadyCallback()
        {
            if (discord == null)
            {
                return;
            }

            var currentUser = discord.GetUserManager().GetCurrentUser();

            Debug.Log(string.Format("Discord: connected to {0}#{1}: {2}", currentUser.Username, currentUser.Discriminator, currentUser.Id));
        }

        public void DisconnectedCallback(int errorCode, string message)
        {
            Debug.Log(string.Format("Discord: disconnect {0}: {1}", errorCode, message));
        }

        public void ErrorCallback(int errorCode, string message)
        {
            Debug.Log(string.Format("Discord: error {0}: {1}", errorCode, message));
        }

        public void JoinCallback(string secret)
        {
            Debug.Log(string.Format("Discord: join ({0})", secret));

            onJoin?.Invoke(secret);
        }

        public void RequestCallback(ref User request)
        {
            onJoinRequest?.Invoke(request);

            Debug.Log(string.Format("Discord: join request {0}#{1}: {2}", request.Username, request.Discriminator, request.Id));
            joinRequest = request;
        }

        public void LeaveLobby()
        {
        }

        private void Update()
        {
            if (discord == null)
            {
                return;
            }

            try
            {
                discord.RunCallbacks();
            }
            catch(System.Exception)
            {
            }
        }

        private void OnEnable()
        {
            try
            {
                discord = new Discord.Discord(applicationId, (ulong)CreateFlags.NoRequireDiscord);
                discord.GetUserManager().OnCurrentUserUpdate += ReadyCallback;
                discord.GetActivityManager().OnActivityJoinRequest += RequestCallback;
                discord.GetActivityManager().OnActivityJoin += JoinCallback;

                if (optionalSteamId > 0)
                {
                    discord.GetActivityManager().RegisterSteam(optionalSteamId);
                }
            }
            catch(System.Exception)
            {
            }

            DontDestroyOnLoad(gameObject);
        }

        private void OnDisable()
        {
            if (discord == null)
            {
                return;
            }

            discord.Dispose();
        }
#endif
    }
}
