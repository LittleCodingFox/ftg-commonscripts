﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class TypeUtils
        {
            public static TargetType[] AllUnityObjectsMatchingQuery<TargetType>(Func<TargetType, bool> Query) where TargetType : UnityEngine.Object
            {
                TargetType[] AllObjects = Resources.FindObjectsOfTypeAll<TargetType>();

                return AllObjects.Where(Query).ToArray();
            }

            public static Lazy<Type[]> LastCachedTypes = new Lazy<Type[]>(() =>
            {
                return AppDomain.CurrentDomain.GetAssemblies().SelectMany(y =>
                {
                    try
                    {
                        return y.GetTypes();
                    }
                    catch(ReflectionTypeLoadException e)
                    {
                        Debug.LogError($"Failed to load types for assembly {y.FullName}: {string.Join("\n", e.LoaderExceptions.Select(x => x))}");

                        return new Type[0];
                    }
                }).ToArray();
            });

            public static Type[] AllTypesSubclassingClass<TargetType>(bool useCache = true)
            {
                if(useCache)
                {
                    return LastCachedTypes.Value.Where(x => (x.IsSubclassOf(typeof(TargetType)) || typeof(TargetType).IsAssignableFrom(x)) && !x.IsInterface &&
                        x != typeof(TargetType)).ToArray();
                }

                return AppDomain.CurrentDomain.GetAssemblies().SelectMany(y =>
                {
                    try
                    {
                        return y.GetTypes();
                    }
                    catch (ReflectionTypeLoadException e)
                    {
                        Debug.LogError($"Failed to load types for assembly {y.FullName}: {string.Join("\n", e.LoaderExceptions.Select(x => x))}");

                        return new Type[0];
                    }
                })
                .Where(x => (x.IsSubclassOf(typeof(TargetType)) || typeof(TargetType).IsAssignableFrom(x)) && !x.IsInterface &&
                    x != typeof(TargetType))
                .ToArray();
            }

            public static TargetType InstanceTypeWithDefaultConstructor<TargetType>(Type type)
            {
                return (type.IsSubclassOf(typeof(TargetType)) || type == typeof(TargetType)) ? (TargetType)Activator.CreateInstance(type) : default(TargetType);
            }
        }
    }
}

