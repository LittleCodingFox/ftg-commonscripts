﻿#if USING_MOONSHARP
using UnityEngine;
using MoonSharp.Interpreter;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class MoonSharpScriptFunction
        {
            public DynValue functionHandle
            {
                get;
                private set;
            }

            public Script owner
            {
                get;
                private set;
            }

            public string error
            {
                get;
                private set;
            }

            public bool hasError
            {
                get
                {
                    return error != null;
                }
            }

            public MoonSharpScriptFunction(Script script, DynValue functionValue)
            {
                if (script == null)
                {
                    throw new System.ArgumentNullException("script");
                }

                if (functionValue != null && functionValue.Type != DataType.Nil && functionValue.Type != DataType.ClrFunction && functionValue.Type != DataType.Function)
                {
                    throw new System.ArgumentException("functionValue");
                }

                owner = script;

                functionHandle = functionValue;
            }

            public MoonSharpScriptFunction(Script script, string name, string parameters, string code)
            {
                if (script == null)
                {
                    throw new System.ArgumentNullException("script");
                }

                owner = script;

                string actualCode = "function " + name + "(" + parameters + ")\n" + code + "\nend";

                try
                {
                    owner.DoString(actualCode);

                    functionHandle = owner.Globals.Get(name);
                }
                catch (InterpreterException e)
                {
                    error = "Failure creating a script function '" + name + "(" + parameters + ")': " +
                        (e.DecoratedMessage == null || e.DecoratedMessage.Length == 0 ? e.Message : e.DecoratedMessage);

                    Debug.LogError(error);
                }
                catch (System.Exception e)
                {
                    error = "Failure creating a script function '" + name + "(" + parameters + ")': " + e.ToString();

                    Debug.LogError(error);
                }
            }

            public DynValue Run(params object[] parameters)
            {
                if (hasError)
                {
                    return null;
                }

                if (functionHandle != null && (functionHandle.Type == DataType.Function || functionHandle.Type == DataType.ClrFunction))
                {
                    try
                    {
                        DynValue result = owner.Call(functionHandle, parameters);

                        return result;
                    }
                    catch (InterpreterException e)
                    {
                        error = "Error running a script function: " +
                            (e.DecoratedMessage == null || e.DecoratedMessage.Length == 0 ? e.Message : e.DecoratedMessage);

                        Debug.LogError(error);

                        return null;
                    }
                    catch (System.Exception e)
                    {
                        error = "Error running a script function: " + e.ToString();

                        Debug.LogError(error);

                        return null;
                    }
                }

                return null;
            }
        }
    }
}
#endif
