﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public struct HashableVector3 : IEquatable<HashableVector3>
    {
        public readonly float x;
        public readonly float y;
        public readonly float z;

        public HashableVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public HashableVector3(Vector3 value)
        {
            x = value.x;
            y = value.y;
            z = value.z;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            return other is HashableVector3 && Equals((HashableVector3)other);
        }

        public bool Equals(HashableVector3 other)
        {
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // https://stackoverflow.com/questions/1835976/what-is-a-sensible-prime-for-hashcode-calculation
                int hash = this.x.GetHashCode() * 486187739;
                hash = HashHelper.Combine(hash * 486187739, this.y.GetHashCode());
                hash = HashHelper.Combine(hash * 486187739, this.z.GetHashCode());

                return hash;
            }
        }
    }
}
