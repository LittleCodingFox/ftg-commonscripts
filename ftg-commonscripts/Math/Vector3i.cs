﻿#if USING_PROTOBUF
using ProtoBuf;
#endif
using System;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    [System.Serializable]
#if USING_PROTOBUF
    [ProtoContract(AsReferenceDefault = true, ImplicitFields = ImplicitFields.AllFields)]
#endif
    public struct Vector3i : IEquatable<Vector3i>, IComparable<Vector3i>
    {
        public int x;
        public int y;
        public int z;

        public Vector3i(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3i(int x, int y)
        {
            this.x = x;
            this.y = y;

            z = 0;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 47;

                hash = hash * 227 + x.GetHashCode();
                hash = hash * 227 + y.GetHashCode();
                hash = hash * 227 + z.GetHashCode();

                return hash * 227;
            }
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public static bool operator ==(Vector3i a, Vector3i b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        public static bool operator !=(Vector3i a, Vector3i b)
        {
            return a.x != b.x || a.y != b.y || a.z != b.z;
        }

        public static Vector3i operator -(Vector3i a)
        {
            return new Vector3i(-a.x, -a.y, -a.z);
        }

        public static Vector3i operator -(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static Vector3i operator +(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Vector3i operator *(int i, Vector3i p)
        {
            return new Vector3i(p.x * i, p.y * i, p.z * i);
        }

        public static Vector3i operator *(Vector3i p, int i)
        {
            return i * p;
        }

        public static Vector3i operator*(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static Vector3i operator /(int i, Vector3i p)
        {
            return new Vector3i(p.x / i, p.y / i, p.z / i);
        }

        public static Vector3i operator /(Vector3i p, int i)
        {
            return i / p;
        }

        public static implicit operator Vector3(Vector3i p)
        {
            return new Vector3(p.x, p.y, p.z);
        }

        public static implicit operator Vector3i(Vector3 v)
        {
            return new Vector3i(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
        }

        public static implicit operator Vector2(Vector3i p)
        {
            return new Vector2(p.x, p.y);
        }

        public static implicit operator Vector3i(Vector2 v)
        {
            return new Vector3i(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
        }

        public static int Distance(Vector3i a, Vector3i b)
        {
            var x = Mathf.Abs(a.x - b.x);
            var y = Mathf.Abs(a.y - b.y);
            var z = Mathf.Abs(a.z - b.z);

            return x + y + z;
        }

        public bool Equals(Vector3i other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public int CompareTo(Vector3i other)
        {
            if(x < other.x)
            {
                return -1;
            }

            if (x > other.x)
            {
                return 1;
            }

            if (y < other.y)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }

            if (z < other.z)
            {
                return -1;
            }

            if (z > other.z)
            {
                return 1;
            }

            return 0;
        }

        public static Vector3i zero
        {
            get
            {
                return new Vector3i(0, 0, 0);
            }
        }

        public static Vector3i one
        {
            get
            {
                return new Vector3i(1, 1, 1);
            }
        }

        public int this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    case 2:
                        return z;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        break;
                    case 1:
                        y = value;
                        break;
                    case 2:
                        z = value;
                        break;
                    default:
                        return;
                }
            }
        }
    }
}
