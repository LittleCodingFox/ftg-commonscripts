﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class MathUtils
    {
        public static float PowerCurve(float total, float target, float min, float max)
        {
            var B = Mathf.Log(max / min) / (total - 1.0f);
            var A = min / (Mathf.Exp(B) - 1.0f);

            return Mathf.Round(A * Mathf.Exp(B * target)) - Mathf.Round(A * Mathf.Exp(B * (target - 1)));
        }
    }
}
