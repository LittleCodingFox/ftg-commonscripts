﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public static class NetworkExtensions
    {
        #region Reader
        public static MessageType ReadMessage<MessageType>(this INetworkReader reader) where MessageType : INetworkMessage
        {
            try
            {
                var messageInstance = System.Activator.CreateInstance<MessageType>();

                if (messageInstance == null)
                {
                    return default;
                }

                messageInstance.Deserialize(reader);

                return messageInstance;
            }
            catch (System.Exception e)
            {
                Debug.LogWarning($"Failed to deserialize a message of type {typeof(MessageType).Name}: {e}");

                return default;
            }
        }

        public static Quaternion ReadQuaternion(this INetworkReader reader)
        {
            var x = reader.ReadFloat();
            var y = reader.ReadFloat();
            var z = reader.ReadFloat();

            var w = Mathf.Sqrt(1.0f - (x * x + y * y + z * z));

            return new Quaternion(x, y, z, w);
        }

        public static Color ReadColor(this INetworkReader reader)
        {
            return reader.ReadColor32();
        }

        public static Color32 ReadColor32(this INetworkReader reader)
        {
            return new Color32
            {
                r = reader.ReadByte(),
                g = reader.ReadByte(),
                b = reader.ReadByte(),
                a = reader.ReadByte()
            };
        }

        public static short ReadVarInt16(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToInt16(bytes);
        }

        public static int ReadVarInt32(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToInt32(bytes);
        }

        public static long ReadVarInt64(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToInt64(bytes);
        }

        public static ushort ReadVarUInt16(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToUInt16(bytes);
        }

        public static uint ReadVarUInt32(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToUInt32(bytes);
        }

        public static ulong ReadVarUInt64(this INetworkReader reader)
        {
            byte count = reader.ReadByte();
            var bytes = reader.ReadBytesAndSize(count);

            return VarintBitConverter.ToUInt64(bytes);
        }

        public static Vector2 ReadVector2(this INetworkReader reader)
        {
            var outValue = Vector2.zero;

            outValue.x = reader.ReadFloat();
            outValue.y = reader.ReadFloat();

            return outValue;
        }

        public static Vector2i ReadVector2i(this INetworkReader reader)
        {
            var outValue = Vector2i.zero;

            outValue.x = reader.ReadVarInt32();
            outValue.y = reader.ReadVarInt32();

            return outValue;
        }

        public static Vector3 ReadVector3(this INetworkReader reader)
        {
            var outValue = Vector3.zero;

            outValue.x = reader.ReadFloat();
            outValue.y = reader.ReadFloat();
            outValue.z = reader.ReadFloat();

            return outValue;
        }

        public static Vector3i ReadVector3i(this INetworkReader reader)
        {
            var outValue = Vector3i.zero;

            outValue.x = reader.ReadVarInt32();
            outValue.y = reader.ReadVarInt32();
            outValue.z = reader.ReadVarInt32();

            return outValue;
        }

        public static Vector4 ReadVector4(this INetworkReader reader)
        {
            var outValue = Vector4.zero;

            outValue.x = reader.ReadFloat();
            outValue.y = reader.ReadFloat();
            outValue.z = reader.ReadFloat();
            outValue.w = reader.ReadFloat();

            return outValue;
        }

        //From https://github.com/LukeStampfli/DarkriftSerializationExtensions/blob/master/DarkriftSerializationExtensions/DarkriftSerializationExtensions/SerializationExtensions.cs
        public static Quaternion ReadQuaternionCompressed(this INetworkReader reader)
        {
            byte maxIndex = reader.ReadByte();

            float a = reader.ReadVarInt16() / 32767f;
            float b = reader.ReadVarInt16() / 32767f;
            float c = reader.ReadVarInt16() / 32767f;
            float d = Mathf.Sqrt(1f - (a * a + b * b + c * c));

            switch (maxIndex)
            {
                case 0:
                    return new Quaternion(d, a, b, c);
                case 1:
                    return new Quaternion(a, d, b, c);
                case 2:
                    return new Quaternion(a, b, d, c);
                default:
                    return new Quaternion(a, b, c, d);
            }
        }
        #endregion

        #region Writer
        public static void WriteMessage(this INetworkWriter writer, INetworkMessage message)
        {
            message.Serialize(writer);
        }

        public static void WriteColor(this INetworkWriter writer, Color value)
        {
            writer.WriteColor32(value);
        }

        public static void WriteColor32(this INetworkWriter writer, Color32 value)
        {
            writer.WriteByte(value.r);
            writer.WriteByte(value.g);
            writer.WriteByte(value.b);
            writer.WriteByte(value.a);
        }

        public static void WriteQuaternion(this INetworkWriter writer, Quaternion value)
        {
            writer.WriteFloat(value.x);
            writer.WriteFloat(value.y);
            writer.WriteFloat(value.z);
        }

        public static void WriteVarInt16(this INetworkWriter writer, short value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVarInt32(this INetworkWriter writer, int value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVarInt64(this INetworkWriter writer, long value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVarUInt16(this INetworkWriter writer, ushort value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVarUInt32(this INetworkWriter writer, uint value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVarUInt64(this INetworkWriter writer, ulong value)
        {
            var bytes = VarintBitConverter.GetVarintBytes(value);

            writer.WriteByte((byte)bytes.Length);
            writer.WriteBytesAndSize(bytes, bytes.Length);
        }

        public static void WriteVector2(this INetworkWriter writer, Vector2 value)
        {
            writer.WriteFloat(value.x);
            writer.WriteFloat(value.y);
        }

        public static void WriteVector2i(this INetworkWriter writer, Vector2i value)
        {
            writer.WriteVarInt32(value.x);
            writer.WriteVarInt32(value.y);
        }

        public static void WriteVector3(this INetworkWriter writer, Vector3 value)
        {
            writer.WriteFloat(value.x);
            writer.WriteFloat(value.y);
            writer.WriteFloat(value.z);
        }

        public static void WriteVector3i(this INetworkWriter writer, Vector3i value)
        {
            writer.WriteVarInt32(value.x);
            writer.WriteVarInt32(value.y);
            writer.WriteVarInt32(value.z);
        }

        public static void WriteVector4(this INetworkWriter writer, Vector4 value)
        {
            writer.WriteFloat(value.x);
            writer.WriteFloat(value.y);
            writer.WriteFloat(value.z);
            writer.WriteFloat(value.w);
        }

        //From https://github.com/LukeStampfli/DarkriftSerializationExtensions/blob/master/DarkriftSerializationExtensions/DarkriftSerializationExtensions/SerializationExtensions.cs
        public static void WriteQuaternionCompressed(this INetworkWriter writer, Quaternion quaternion)
        {
            byte maxIndex = 0;
            var maxValue = float.MinValue;
            var sign = 1.0f;

            for (var i = 0; i < 4; i++)
            {
                var f = quaternion[i];
                var abs = Mathf.Abs(f);

                if (abs > maxValue)
                {
                    maxValue = abs;
                    maxIndex = (byte)i;
                    sign = f > 0 ? 1 : -1;
                }
            }

            short a = 0, b = 0, c = 0;

            switch (maxIndex)
            {
                case 0:

                    a = (short)(quaternion.y * sign * 32767f);
                    b = (short)(quaternion.z * sign * 32767f);
                    c = (short)(quaternion.w * sign * 32767f);

                    break;

                case 1:

                    a = (short)(quaternion.x * sign * 32767f);
                    b = (short)(quaternion.z * sign * 32767f);
                    c = (short)(quaternion.w * sign * 32767f);

                    break;

                case 2:

                    a = (short)(quaternion.x * sign * 32767f);
                    b = (short)(quaternion.y * sign * 32767f);
                    c = (short)(quaternion.w * sign * 32767f);

                    break;

                case 3:

                    a = (short)(quaternion.x * sign * 32767f);
                    b = (short)(quaternion.y * sign * 32767f);
                    c = (short)(quaternion.z * sign * 32767f);

                    break;
            }

            writer.WriteByte(maxIndex);
            writer.WriteVarInt16(a);
            writer.WriteVarInt16(b);
            writer.WriteVarInt16(c);
        }
        #endregion
    }
}
