﻿using System;
using System.Text;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class StringExtensions
        {
            public static string ExpandedName(this string self)
            {
                StringBuilder outString = new StringBuilder();

                bool hadUppercase = true;
                int lastLowercaseIndex = 0; 

                for(int i = 0; i < self.Length; i++)
                {
                    if(char.IsUpper(self[i]))
                    {
                        if(!hadUppercase)
                        {
                            outString.Append(self.Substring(lastLowercaseIndex, i - lastLowercaseIndex));

                            if (outString.Length > 0)
                            {
                                outString.Append(" ");
                            }

                            lastLowercaseIndex = i;
                        }

                        outString.Append(self[i]);

                        hadUppercase = true;
                    }
                    else if(char.IsDigit(self[i]))
                    {
                        if (i > 0 && !char.IsDigit(self[i - 1]) && !char.IsWhiteSpace(self[i - 1]))
                        {
                            outString.Append(self.Substring(lastLowercaseIndex, i - lastLowercaseIndex));

                            if (outString.Length > 0)
                            {
                                outString.Append(" ");
                            }
                        }

                        outString.Append(self[i]);

                        hadUppercase = false;
                        lastLowercaseIndex = (i + 1 < self.Length ? i + 1 : i);
                    }
                    else
                    {
                        if(hadUppercase)
                        {
                            hadUppercase = false;
                            lastLowercaseIndex = i;
                        }
                    }

                    if(i == self.Length - 1)
                    {
                        if(hadUppercase)
                        {
                            outString.Append(self.Substring(lastLowercaseIndex, (i + 1) - lastLowercaseIndex));
                        }
                        else
                        {
                            outString.Append(self.Substring(lastLowercaseIndex, (i + 1) - lastLowercaseIndex));
                        }
                    }
                }

                return outString.ToString();
            }
        }
    }
}
