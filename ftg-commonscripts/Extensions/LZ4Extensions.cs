﻿#if USING_LZ4
using K4os.Compression.LZ4;
using K4os.Compression.LZ4.Streams;
using System.IO;

public static class LZ4Extensions
{
    public static byte[] Decode(byte[] data)
    {
        try
        {
            var lz4Bytes = new byte[0];

            using (var memoryStream = new MemoryStream(data))
            {
                using (var lz4MemoryStream = new MemoryStream())
                {
                    using (var lz4Stream = LZ4Stream.Decode(memoryStream))
                    {
                        lz4Stream.CopyTo(lz4MemoryStream);
                    }

                    lz4Bytes = lz4MemoryStream.ToArray();
                }
            }

            return lz4Bytes;
        }
        catch(System.Exception)
        {
            return new byte[0];
        }
    }

    public static byte[] Encode(byte[] data, LZ4Level level = LZ4Level.L12_MAX)
    {
        try
        {
            var lz4Bytes = new byte[0];

            using (var memoryStream = new MemoryStream(data))
            {
                using (var lz4MemoryStream = new MemoryStream())
                {
                    using (var lz4Stream = LZ4Stream.Encode(lz4MemoryStream, level))
                    {
                        memoryStream.CopyTo(lz4Stream);
                    }

                    lz4Bytes = lz4MemoryStream.ToArray();
                }
            }

            return lz4Bytes;
        }
        catch(System.Exception)
        {
            return new byte[0];
        }
    }
}

#endif
