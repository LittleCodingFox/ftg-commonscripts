using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public static class SpriteRendererExtensions
    {
        public static void ScaleToScreenSize(this SpriteRenderer renderer)
        {
            renderer.transform.localScale = new Vector3(1, 1, 1);

            //float width = renderer.sprite.bounds.size.x;
            float height = renderer.sprite.bounds.size.y;

            float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            //float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            float scale = worldScreenHeight / height;

            renderer.transform.localScale = new Vector3(scale, scale, 1);
        }
    }
}
