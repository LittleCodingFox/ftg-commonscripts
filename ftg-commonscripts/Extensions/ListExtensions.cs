using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class ListExtensions
        {
            private static System.Random rng = new System.Random();

            public static int Shuffle<T>(this IList<T> list)
            {
                return list.Shuffle(rng);
            }

            public static int Shuffle<T>(this IList<T> list, System.Random random)
            {
                int n = list.Count;
                var iterations = 0;

                while (n > 1)
                {
                    n--;
                    int k = random.Next(n + 1);
                    T value = list[k];
                    list[k] = list[n];
                    list[n] = value;

                    iterations++;
                }

                return iterations;
            }

            public static void Resize<T>(this List<T> list, int size, T defaultValue)
            {
                int current = list.Count;

                if (size < current)
                {
                    list.RemoveRange(size, current - size);
                }
                else if (size > current)
                {
                    if (size > list.Capacity)
                    {
                        list.Capacity = size;
                    }

                    list.AddRange(Enumerable.Repeat(defaultValue, size - current));
                }
            }

            public static void Resize<T>(this List<T> list, int size) where T : new()
            {
                Resize(list, size, new T());
            }
        }
    }
}
