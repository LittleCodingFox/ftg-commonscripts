﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FlamingTorchGames.CommonScripts
{
    public static class UIExtensions
    {
        public static Vector2 CenteredSpacing(this GridLayoutGroup layout)
        {
            var rectTransform = layout.GetComponent<RectTransform>();

            var usableSpaceHorizontal = rectTransform.rect.width - layout.padding.left - layout.padding.right;
            var usableSpaceVertical = rectTransform.rect.height - layout.padding.top - layout.padding.bottom;

            if(usableSpaceHorizontal <= 0)
            {
                return layout.spacing;
            }

            var countHorizontal = Mathf.Floor(usableSpaceHorizontal / layout.cellSize.x);
            var countVertical = Mathf.Floor(usableSpaceVertical / layout.cellSize.y);
            var remainingSpaceHorizontal = usableSpaceHorizontal - countHorizontal * layout.cellSize.x;
            var remainingSpaceVertical = usableSpaceVertical - countVertical * layout.cellSize.y;
            var spacingX = remainingSpaceHorizontal / (countHorizontal + 1);
            var spacingY = Mathf.Clamp(remainingSpaceVertical / (countVertical + 1), 0, Mathf.Infinity);

            //var counter = 0;

            if(remainingSpaceHorizontal <= 0)
            {
                return layout.spacing;
            }

            /*
            while (spacingX <= 5)
            {
                counter++;

                remainingSpaceHorizontal = usableSpaceHorizontal - (countHorizontal - counter) * layout.cellSize.x;
                spacingX = remainingSpaceHorizontal / (countHorizontal - counter - 1);
            }
            */

            return new Vector2(spacingX, spacingY);
        }

        public static Vector2 ExpectedElementCount(this GridLayoutGroup layout)
        {
            var rectTransform = layout.GetComponent<RectTransform>();

            var usableSpaceHorizontal = Mathf.Clamp(rectTransform.rect.width - layout.padding.left - layout.padding.right, 0, Mathf.Infinity);
            var usableSpaceVertical = Mathf.Clamp(rectTransform.rect.height - layout.padding.top - layout.padding.bottom, 0, Mathf.Infinity);
            var countHorizontal = Mathf.Floor(usableSpaceHorizontal / layout.cellSize.x);
            var countVertical = Mathf.Floor(usableSpaceVertical / layout.cellSize.y);

            if (usableSpaceHorizontal <= 0)
            {
                return Vector2.zero;
            }

            while (rectTransform.rect.width - countHorizontal * layout.cellSize.x - (layout.spacing.x * (countHorizontal - 1)) < 0)
            {
                countHorizontal--;
            }

            if(usableSpaceVertical == 0)
            {
                return new Vector2(countHorizontal, 1);
            }

            while(rectTransform.rect.height - countVertical * layout.cellSize.y - (layout.spacing.y * (countVertical - 1)) < 0)
            {
                countVertical--;
            }

            return new Vector2(countHorizontal, countVertical);
        }

        public static RectOffset CenteredPadding(this GridLayoutGroup layout)
        {
            var rectTransform = layout.GetComponent<RectTransform>();
            var elementCount = layout.ExpectedElementCount();

            var verticalSpaceLeft = rectTransform.rect.height - elementCount.y * layout.cellSize.y - (layout.spacing.y * (elementCount.y - 1));
            var horizontalSpaceLeft = rectTransform.rect.width - elementCount.x * layout.cellSize.x - (layout.spacing.x * (elementCount.x - 1));
            var paddingLeftRight = Mathf.FloorToInt(Mathf.Clamp(horizontalSpaceLeft, 0, Mathf.Infinity) / 2.0f);
            var paddingTopBottom = Mathf.FloorToInt(Mathf.Clamp(verticalSpaceLeft, 0, Mathf.Infinity) / 2.0f);

            if(horizontalSpaceLeft < 0)
            {
                return new RectOffset();
            }

            return new RectOffset(paddingLeftRight, paddingLeftRight, paddingTopBottom, paddingTopBottom);
        }
    }
}
