using System.Linq;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class TransformExtensions
        {
            public static bool HasParent(this Transform target, Transform parent)
            {
                if(target == null)
                {
                    return false;
                }

                if(target == parent)
                {
                    return true;
                }

                return target.parent == parent || HasParent(target.parent, parent);
            }

            public static void SetLayerOnSelfAndChildren(this Transform target, string layerName)
            {
                target.gameObject.layer = LayerMask.NameToLayer(layerName);

                foreach(Transform child in target)
                {
                    child.SetLayerOnSelfAndChildren(layerName);
                }
            }

            public static T[] GetComponentsInSelfAndChildren<T>(this Transform target) where T: Component
            {
                return target.GetComponents<T>().Concat(target.GetComponentsInChildren<T>()).ToArray();
            }

            public static Transform Search(this Transform target, string name)
            {
                if (target == null || target.name == name)
                {
                    return target;
                }

                for (int i = 0; i < target.childCount; ++i)
                {
                    var result = Search(target.GetChild(i), name);

                    if (result != null)
                    {
                        return result;
                    }
                }

                return null;
            }

            public static Transform SearchPartial(this Transform target, string name)
            {
                if (target == null || target.name.Contains(name))
                {
                    return target;
                }

                for (int i = 0; i < target.childCount; ++i)
                {
                    var result = SearchPartial(target.GetChild(i), name);

                    if (result != null)
                    {
                        return result;
                    }
                }

                return null;
            }

            public static ComponentType GetComponentInParentRecursive<ComponentType>(this Transform target) where ComponentType : Component
            {
                var parent = target.parent;

                if(parent == null)
                {
                    return null;
                }

                if(parent.TryGetComponent<ComponentType>(out var component))
                {
                    return component;
                }

                return parent.GetComponentInParentRecursive<ComponentType>();
            }

            public static ComponentType SearchAndGetComponent<ComponentType>(this Transform target, string name) where ComponentType : Component
            {
                Transform t = target.Search(name);

                if (t != null)
                {
                    return t.GetComponent<ComponentType>();
                }

                return null;
            }

            public static ComponentType SearchAndGetComponentPartial<ComponentType>(this Transform target, string name) where ComponentType : Component
            {
                Transform t = target.SearchPartial(name);

                if (t != null)
                {
                    return t.GetComponent<ComponentType>();
                }

                return null;
            }
        }
    }
}
