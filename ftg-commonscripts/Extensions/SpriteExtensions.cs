﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public static class SpriteExtensions
    {
        public static Texture2D AsTexture(this Sprite sprite)
        {
            var outTexture = new Texture2D((int)sprite.textureRect.width, (int)sprite.textureRect.height, TextureFormat.RGBA32, false, false);

            var textureRect = new Rect(sprite.textureRect.x, sprite.textureRect.y,
                sprite.textureRect.width, sprite.textureRect.height);

            var pixels = sprite.texture.GetPixels((int)textureRect.x, (int)textureRect.y, (int)textureRect.width, (int)textureRect.height);

            outTexture.SetPixels(pixels);
            outTexture.Apply();

            return outTexture;
        }
    }
}
