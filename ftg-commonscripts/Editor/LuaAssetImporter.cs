﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.AssetImporters;

namespace FlamingTorchGames.CommonScripts
{
    [ScriptedImporter(1, "lua")]
    public class LuaAssetImporter : ScriptedImporter
    {
        public override void OnImportAsset(AssetImportContext ctx)
        {
            var text = System.IO.File.ReadAllText(ctx.assetPath);

            var textAsset = new TextAsset(text);
            ctx.AddObjectToAsset("Text", textAsset);
            ctx.SetMainObject(textAsset);
        }
    }
}
