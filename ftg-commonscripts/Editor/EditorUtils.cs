﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;

namespace FlamingTorchGames.CommonScripts
{
    public class FileCloneEditorWindow : EditorWindow
    {
        private string sourceFile;
        private string destPattern;
        private int variableCount = 1;
        private int[] variableFroms;
        private int[] variableTos;

        [MenuItem("Flaming Torch Games/Clone Files")]
        public static void ShowCloneFiles()
        {
            var window = GetWindow<FileCloneEditorWindow>();

            window.titleContent = new GUIContent("Clone Files");

            window.Show();
        }

        private void OnGUI()
        {
            sourceFile = EditorGUILayout.TextField("Source File:", sourceFile);
            destPattern = EditorGUILayout.TextField("Destination Pattern:", destPattern);
            variableCount = EditorGUILayout.IntField("Variable Count: ", variableCount);

            if(variableFroms == null || variableFroms.Length != variableCount)
            {
                if(variableFroms == null)
                {
                    variableFroms = new int[variableCount];
                }
                else
                {
                    var list = variableFroms.Take(variableCount).ToList();

                    while(list.Count < variableCount)
                    {
                        list.Add(0);
                    }

                    variableFroms = list.ToArray();
                }
            }

            if (variableTos == null || variableTos.Length != variableCount)
            {
                if (variableTos == null)
                {
                    variableTos = new int[variableCount];
                }
                else
                {
                    var list = variableTos.Take(variableCount).ToList();

                    while (list.Count < variableCount)
                    {
                        list.Add(0);
                    }

                    variableTos = list.ToArray();
                }
            }

            for (var i = 0; i < variableCount; i++)
            {
                variableFroms[i] = EditorGUILayout.IntField($"From {i + 1}", variableFroms[i]);
                variableTos[i] = EditorGUILayout.IntField($"To {i + 1}", variableTos[i]);
            }

            if (GUILayout.Button("Browse source"))
            {
                var path = EditorUtility.OpenFilePanel("Open File", Application.dataPath, "");

                if(path != null && path.Length > 0)
                {
                    sourceFile = path;
                }
            }

            if(GUILayout.Button("Browse Destination & Process"))
            {
                var path = EditorUtility.OpenFolderPanel("Open Folder", Application.dataPath, "");

                if(path != null && path.Length > 0)
                {
                    System.Action<int, int[]> loopAction = null;
                    loopAction = (depth, variables) =>
                    {
                        if(depth == 0)
                        {
                            var fileName = string.Format(destPattern, variables.Select(x => (object)x).ToArray());
                            var filePath = $"{path}/{fileName}";

                            try
                            {
                                System.IO.File.Copy(sourceFile, filePath, true);
                            }
                            catch(System.Exception e)
                            {
                                Debug.LogError(e.ToString());
                            }

                            return;
                        }

                        for(var i = variableFroms[depth - 1]; i <= variableTos[depth - 1]; i++)
                        {
                            loopAction(depth - 1, variables.Concat(new int[] { i }).ToArray());
                        }
                    };

                    loopAction(variableCount, new int[] { });
                }
            }
        }
    }

    public static class EditorUtils
    {
        [MenuItem("Flaming Torch Games/Save Procedural Prefab")]
        public static void CreateProceduralPrefab()
        {
            if(Selection.gameObjects.Length == 0)
            {
                EditorUtility.DisplayDialog("Error", "Requires an active selection", "OK");

                return;
            }

            var path = EditorUtility.SaveFolderPanel("Prefab store folder", "", "Prefabs").Replace(Application.dataPath, "Assets");

            foreach(var gameObject in Selection.gameObjects)
            {
                var meshRenderers = gameObject.transform.GetComponentsInSelfAndChildren<MeshRenderer>();

                foreach(var meshRenderer in meshRenderers)
                {
                    var meshFilter = meshRenderer.GetComponent<MeshFilter>();

                    if(meshFilter.sharedMesh.name.Length == 0)
                    {
                        if(!AssetDatabase.IsValidFolder($"{path}/Meshes"))
                        {
                            AssetDatabase.CreateFolder(path, "Meshes");
                        }

                        var meshPath = AssetDatabase.GenerateUniqueAssetPath($"{path}/Meshes/Mesh.asset");

                        AssetDatabase.CreateAsset(meshFilter.sharedMesh, meshPath);
                    }

                    foreach(var material in meshRenderer.sharedMaterials)
                    {
                        if(material != null && material.name.EndsWith("(Clone)"))
                        {
                            if(!AssetDatabase.IsValidFolder($"{path}/Materials"))
                            {
                                AssetDatabase.CreateFolder(path, "Materials");
                            }

                            if(material.mainTexture != null && (material.mainTexture.name.Length == 0 || material.mainTexture.name.EndsWith("(Clone)")))
                            {
                                if (!AssetDatabase.IsValidFolder($"{path}/Textures"))
                                {
                                    AssetDatabase.CreateFolder(path, "Textures");
                                }

                                var texturePath = AssetDatabase.GenerateUniqueAssetPath($"{path}/Textures/Texture.png");

                                AssetDatabase.CreateAsset(material.mainTexture, texturePath);
                            }

                            var materialPath = AssetDatabase.GenerateUniqueAssetPath($"{path}/Materials/Material.mat");

                            AssetDatabase.CreateAsset(material, materialPath);
                        }
                    }
                }

                if (!AssetDatabase.IsValidFolder($"{path}/GameObjects"))
                {
                    AssetDatabase.CreateFolder(path, "GameObjects");
                }

                var objectPath = AssetDatabase.GenerateUniqueAssetPath($"{path}/GameObjects/{gameObject.name}.prefab");

                PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, objectPath, InteractionMode.UserAction);
            }
        }

        [MenuItem("Flaming Torch Games/Platform Utils/Report PC Platform")]
        public static void SetPCSimulatedPlatform()
        {
            EditorUserSettings.SetConfigValue("FTG_SIMULATED_PLATFORM", PlatformType.PC.ToString());
        }

        [MenuItem("Flaming Torch Games/Platform Utils/Report Phone Platform")]
        public static void SetPhoneSimulatedPlatform()
        {
            EditorUserSettings.SetConfigValue("FTG_SIMULATED_PLATFORM", PlatformType.Phone.ToString());
        }

        [MenuItem("Flaming Torch Games/Platform Utils/Report Tablet Platform")]
        public static void SetTabletSimulatedPlatform()
        {
            EditorUserSettings.SetConfigValue("FTG_SIMULATED_PLATFORM", PlatformType.Tablet.ToString());
        }

        [MenuItem("Flaming Torch Games/Platform Utils/Report WebGL Platform")]
        public static void SetWebGLSimulatedPlatform()
        {
            EditorUserSettings.SetConfigValue("FTG_SIMULATED_PLATFORM", PlatformType.WebGL.ToString());
        }

        [MenuItem("Flaming Torch Games/Platform Utils/Clear simulatede platform")]
        public static void ClearSimulatedPlatform()
        {
            EditorUserSettings.SetConfigValue("FTG_SIMULATED_PLATFORM", "");
        }

        [MenuItem("Flaming Torch Games/Take Screenshot/Standard")]
        private static void TakeScreenshot()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                ScreenCapture.CaptureScreenshot(path);
            }
        }

        [MenuItem("Flaming Torch Games/Take Screenshot/x2")]
        private static void TakeScreenshotx2()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                ScreenCapture.CaptureScreenshot(path, 2);
            }
        }

        [MenuItem("Flaming Torch Games/Take Screenshot/x3")]
        private static void TakeScreenshotx3()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                ScreenCapture.CaptureScreenshot(path, 3);
            }
        }

        [MenuItem("Flaming Torch Games/Take Screenshot/x4")]
        private static void TakeScreenshotx4()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                ScreenCapture.CaptureScreenshot(path, 4);
            }
        }

        [MenuItem("Flaming Torch Games/Take Screenshot/x5")]
        private static void TakeScreenshotx5()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                ScreenCapture.CaptureScreenshot(path, 5);
            }
        }

        [MenuItem("Flaming Torch Games/Take Transparent Screenshot/Standard")]
        private static void TakeTransparentScreenshot()
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                var camera = Camera.main;
                var renderTexture = new RenderTexture((int)camera.pixelWidth, (int)camera.pixelHeight, 24, RenderTextureFormat.ARGB32);
                renderTexture.antiAliasing = 1;
                renderTexture.autoGenerateMips = false;
                renderTexture.useMipMap = false;
                renderTexture.filterMode = FilterMode.Point;
                camera.targetTexture = renderTexture;
                var screenshotTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

                RenderTexture.active = renderTexture;

                camera.Render();

                screenshotTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
                screenshotTexture.Apply();

                camera.targetTexture = null;
                RenderTexture.active = null;
                renderTexture.Release();

                byte[] data = screenshotTexture.EncodeToPNG();

                System.IO.File.WriteAllBytes(path, data);
            }
        }

        [MenuItem("Flaming Torch Games/Take Transparent Screenshot/300 DPI")]
        private static void TakeTransparentScreenshot300DPI()
        {
            TakeTransparentScreenshotCustom(300);
        }

        private static void TakeTransparentScreenshotCustom(int dpi)
        {
            var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

            if (path != null && path.Length > 0)
            {
                var camera = Camera.main;
                var renderTexture = new RenderTexture((int)camera.pixelWidth, (int)camera.pixelHeight, 24, RenderTextureFormat.ARGB32);
                renderTexture.antiAliasing = 1;
                renderTexture.autoGenerateMips = false;
                renderTexture.useMipMap = false;
                renderTexture.filterMode = FilterMode.Point;
                camera.targetTexture = renderTexture;
                var screenshotTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

                RenderTexture.active = renderTexture;

                camera.Render();

                screenshotTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
                screenshotTexture.Apply();

                camera.targetTexture = null;
                RenderTexture.active = null;
                renderTexture.Release();

                byte[] data = screenshotTexture.EncodeToPNG();
                data = B83.Image.PNGTools.ChangePPI(data, dpi, dpi);

                System.IO.File.WriteAllBytes(path, data);
            }
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/2 Players")]
        private static void PerformWin64Build2()
        {
            PerformWin64Build(2);
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/3 Players")]
        private static void PerformWin64Build3()
        {
            PerformWin64Build(3);
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/4 Players")]
        private static void PerformWin64Build4()
        {
            PerformWin64Build(4);
        }

        private static void PerformWin64Build(int playerCount)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);

            for (int i = 1; i <= playerCount; i++)
            {
                BuildPipeline.BuildPlayer(GetScenePaths(), string.Format("bin/Win64/{0}/{0}.exe", GetProjectName() + i.ToString()), BuildTarget.StandaloneWindows64, BuildOptions.AutoRunPlayer);
            }
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/2 Players")]
        private static void PerformOSXBuild2()
        {
            PerformOSXBuild(2);
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/3 Players")]
        private static void PerformOSXBuild3()
        {
            PerformOSXBuild(3);
        }

        [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/4 Players")]
        private static void PerformOSXBuild4()
        {
            PerformOSXBuild(4);
        }

        private static void PerformOSXBuild(int playerCount)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);

            for (int i = 1; i <= playerCount; i++)
            {
                BuildPipeline.BuildPlayer(GetScenePaths(), string.Format("bin/OSX/{0}/{0}.app", GetProjectName() + i.ToString()), BuildTarget.StandaloneOSX, BuildOptions.AutoRunPlayer);
            }
        }

        [MenuItem("Flaming Torch Games/Build AssetBundles")]
        static void BuildAllAssetBundles()
        {
            string assetBundleDirectory = "Assets/StreamingAssets/AssetBundles";

            if (!System.IO.Directory.Exists(assetBundleDirectory))
            {
                System.IO.Directory.CreateDirectory(assetBundleDirectory);
            }

            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
        }

        [MenuItem("Flaming Torch Games/Unload all AssetBundles")]
        static void UnloadAllAssetBundles()
        {
            AssetBundle.UnloadAllAssetBundles(true);
        }

        private static string GetProjectName()
        {
            string[] s = Application.dataPath.Split('/');

            return s[s.Length - 2];
        }

        private static string[] GetScenePaths()
        {
            return EditorBuildSettings.scenes.Select(x => x.path).ToArray();
        }
    }
}
