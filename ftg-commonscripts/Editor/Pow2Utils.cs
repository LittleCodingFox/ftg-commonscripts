﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace FlamingTorchGames.CommonScripts
{
    public class Pow2Utils : EditorWindow
    {
        private readonly string[] textureSizeStrings = new string[]
        {
            "16",
            "32",
            "64",
            "128",
            "256",
            "512",
            "1024",
            "2048",
            "4096",
            "8192"
        };

        private int[] textureSizes = new int[]
        {
            16,
            32,
            64,
            128,
            256,
            512,
            1024,
            2048,
            4096,
            8192
        };

        private int targetTextureSizeIndex = 6;
        private bool useMipmaps = false;
        private bool useBilinear = false;
        private bool preserveAspect = true;
        private int anisoLevel = 1;
        private bool createSprite = false;
        private bool useCrunchCompression = false;
        private int crunchCompressionFactor = 50;
        private float mipmapBias = -2;
        private FilterMode textureFiltering = FilterMode.Point;
        private DefaultAsset sourceFolder;
        private DefaultAsset targetFolder;
        private int activeTab = 0;

        [MenuItem("Flaming Torch Games/Power-of-2 Sprite Resizer")]
        public static void ShowWindow()
        {
            var window = GetWindow<Pow2Utils>();
            window.titleContent = new GUIContent("Power of 2 Utils");
        }

        public static int NextPowerOfTwo(int value)
        {
            var power = 1;

            while(power < value)
            {
                power *= 2;
            }

            return power;
        }

        private void OnGUI()
        {
            if (sourceFolder == null)
            {
                EditorGUILayout.HelpBox("Missing source folder!", MessageType.Warning, true);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Source Folder:");
            sourceFolder = (DefaultAsset)EditorGUILayout.ObjectField(sourceFolder, typeof(DefaultAsset), false);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            activeTab = GUILayout.Toolbar(activeTab, new string[] { "Resize", "Adjust" });

            switch(activeTab)
            {
                case 0:

                    if (targetFolder == null)
                    {
                        EditorGUILayout.HelpBox("Missing target folder!", MessageType.Warning, true);
                    }

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel("Target Folder:");
                    targetFolder = (DefaultAsset)EditorGUILayout.ObjectField(targetFolder, typeof(DefaultAsset), false);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel("Target Texture Size:");
                    targetTextureSizeIndex = EditorGUILayout.Popup(targetTextureSizeIndex, textureSizeStrings);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.Space();

                    EditorGUILayout.LabelField("Mipmaps", EditorStyles.boldLabel);
                    useMipmaps = EditorGUILayout.Toggle("Use Mipmaps", useMipmaps);
                    mipmapBias = EditorGUILayout.FloatField("Mipmap Bias", mipmapBias);

                    EditorGUILayout.LabelField("Filtering", EditorStyles.boldLabel);
                    anisoLevel = EditorGUILayout.IntField("Aniso Level", anisoLevel);
                    textureFiltering = (FilterMode)EditorGUILayout.EnumPopup("Texture Filtering", textureFiltering);
                    useBilinear = EditorGUILayout.Popup("Scaling Filtering", useBilinear ? 1 : 0, new string[] { "Point", "Bilinear" }) == 1;

                    EditorGUILayout.LabelField("Compression", EditorStyles.boldLabel);
                    useCrunchCompression = EditorGUILayout.Toggle("Use Crunch Compression", useCrunchCompression);
                    crunchCompressionFactor = (int)EditorGUILayout.Slider("Crunch Compression Quality", crunchCompressionFactor, 0, 100);

                    EditorGUILayout.LabelField("Misc", EditorStyles.boldLabel);
                    createSprite = EditorGUILayout.Toggle("Create Sprite", createSprite);
                    preserveAspect = EditorGUILayout.Toggle("Preserve Aspect", preserveAspect);

                    EditorGUILayout.Space();

                    EditorGUI.BeginDisabledGroup(targetFolder == null || sourceFolder == null);

                    if (GUILayout.Button("Resize"))
                    {
                        var basePath = AssetDatabase.GetAssetPath(sourceFolder);
                        var spriteAssets = AssetDatabase.FindAssets("", new string[] { basePath });
                        var textureSize = textureSizes[targetTextureSizeIndex];
                        var count = 0;

                        foreach (var sourceGUID in spriteAssets)
                        {
                            var sourcePath = AssetDatabase.GUIDToAssetPath(sourceGUID);
                            var targetBasePath = AssetDatabase.GetAssetPath(targetFolder);
                            var asset = AssetDatabase.LoadAssetAtPath<Sprite>(sourcePath);

                            if (asset == null)
                            {
                                count++;

                                continue;
                            }

                            EditorUtility.DisplayProgressBar("Processing Textures", Path.GetFileName(sourcePath), count / (float)spriteAssets.Length);

                            var targetPath = $"{targetBasePath}/{sourcePath.Replace(basePath, "")}";

                            try
                            {
                                var fileData = File.ReadAllBytes(Path.Combine(Application.dataPath, sourcePath.Substring("Assets\\".Length)));

                                var texture = new Texture2D(1, 1);

                                if (!texture.LoadImage(fileData))
                                {
                                    count++;

                                    continue;
                                }

                                var targetWidth = NextPowerOfTwo(texture.width);

                                if(preserveAspect)
                                {
                                    var scale = textureSize / (float)texture.height;

                                    targetWidth = (int)(texture.width * scale);
                                }

                                if (useBilinear)
                                {
                                    TextureScale.Bilinear(texture, targetWidth, textureSize);
                                }
                                else
                                {
                                    TextureScale.Point(texture, targetWidth, textureSize);
                                }

                                var newTexture = new Texture2D(NextPowerOfTwo(texture.width), texture.height, asset.texture.alphaIsTransparency ? TextureFormat.RGBA32 : TextureFormat.RGB24, true);
                                newTexture.name = "Texture";

                                var sourcePixels = texture.GetPixels32();
                                var targetPixels = newTexture.GetPixels32();

                                for (int y = 0, sourceIndex = 0, targetIndex = 0; y < texture.height; y++,
                                    sourceIndex += texture.width,
                                    targetIndex += newTexture.width)
                                {
                                    for (var x = 0; x < texture.width; x++)
                                    {
                                        targetPixels[targetIndex + x] = sourcePixels[sourceIndex + x];
                                    }

                                    for (var x = texture.width; x < newTexture.width; x++)
                                    {
                                        targetPixels[targetIndex + x] = Color.black;
                                    }
                                }

                                newTexture.SetPixels32(targetPixels);
                                newTexture.Apply();

                                var bytes = newTexture.EncodeToPNG();

                                var localTargetPath = Path.Combine(Application.dataPath, targetPath.Substring("Assets\\".Length));

                                try
                                {
                                    Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(Application.dataPath, targetPath.Substring("Assets\\".Length))));
                                }
                                catch (System.Exception)
                                {
                                }

                                File.WriteAllBytes(localTargetPath, bytes);

                                AssetDatabase.ImportAsset(targetPath);

                                var importer = AssetImporter.GetAtPath(targetPath) as TextureImporter;

                                if (importer == null)
                                {
                                    File.Delete(localTargetPath);

                                    count++;

                                    continue;
                                }

                                if (createSprite)
                                {
                                    var spriteRect = new Rect(0, 0, targetWidth, textureSize);

                                    importer.textureType = TextureImporterType.Sprite;
                                    importer.spriteImportMode = SpriteImportMode.Multiple;

                                    importer.spritesheet = new SpriteMetaData[]
                                    {
                                        new SpriteMetaData()
                                        {
                                            name = "Sprite",
                                            rect = spriteRect,
                                        }
                                    };
                                }

                                importer.mipmapEnabled = useMipmaps;
                                importer.mipMapBias = mipmapBias;
                                importer.anisoLevel = anisoLevel;
                                importer.filterMode = textureFiltering;
                                importer.crunchedCompression = useCrunchCompression;
                                importer.compressionQuality = crunchCompressionFactor;

                                importer.SaveAndReimport();

                                count++;

                                DestroyImmediate(texture);
                                DestroyImmediate(newTexture);
                            }
                            catch (System.Exception e)
                            {
                                count++;
                                Debug.LogError(e);
                            }
                        }

                        EditorUtility.ClearProgressBar();
                    }

                    EditorGUI.EndDisabledGroup();

                    break;

                case 1:

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel("Target Texture Size:");
                    targetTextureSizeIndex = EditorGUILayout.Popup(targetTextureSizeIndex, textureSizeStrings);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.Space();

                    EditorGUILayout.LabelField("Mipmaps", EditorStyles.boldLabel);
                    useMipmaps = EditorGUILayout.Toggle("Use Mipmaps", useMipmaps);
                    EditorGUILayout.HelpBox("A positive bias makes a texture appear extra blurry, while a negative bias sharpens the texture. Note that using large negative bias" +
                        " can reduce performance, so it's not recommended to use more than -0.5 negative bias.)", MessageType.Info, true);

                    mipmapBias = EditorGUILayout.FloatField("Mipmap Bias", mipmapBias);

                    EditorGUILayout.LabelField("Filtering", EditorStyles.boldLabel);
                    textureFiltering = (FilterMode)EditorGUILayout.EnumPopup("Texture Filtering", textureFiltering);
                    anisoLevel = EditorGUILayout.IntField("Aniso Level", anisoLevel);
                    
                    EditorGUILayout.LabelField("Compression", EditorStyles.boldLabel);
                    useCrunchCompression = EditorGUILayout.Toggle("Use Crunch Compression", useCrunchCompression);
                    crunchCompressionFactor = EditorGUILayout.IntSlider("Crunch Compression Quality", crunchCompressionFactor, 0, 100);

                    EditorGUI.BeginDisabledGroup(sourceFolder == null);

                    if (GUILayout.Button("Adjust"))
                    {
                        var basePath = AssetDatabase.GetAssetPath(sourceFolder);
                        var assets = AssetDatabase.FindAssets("", new string[] { basePath });
                        var count = 0;

                        foreach (var sourceGUID in assets)
                        {
                            var sourcePath = AssetDatabase.GUIDToAssetPath(sourceGUID);
                            var asset = AssetDatabase.LoadAssetAtPath<Sprite>(sourcePath);

                            if (asset == null)
                            {
                                count++;

                                continue;
                            }

                            EditorUtility.DisplayProgressBar("Processing Textures", Path.GetFileName(sourcePath), count / (float)assets.Length);

                            try
                            {
                                var importer = AssetImporter.GetAtPath(sourcePath) as TextureImporter;

                                if (importer == null)
                                {
                                    count++;

                                    continue;
                                }

                                importer.mipmapEnabled = useMipmaps;
                                importer.mipMapBias = mipmapBias;
                                importer.anisoLevel = anisoLevel;
                                importer.filterMode = textureFiltering;
                                importer.crunchedCompression = useCrunchCompression;
                                importer.compressionQuality = crunchCompressionFactor;
                                importer.maxTextureSize = textureSizes[targetTextureSizeIndex];

                                importer.SaveAndReimport();

                                count++;
                            }
                            catch (System.Exception e)
                            {
                                count++;
                                Debug.LogError(e);
                            }
                        }

                        EditorUtility.ClearProgressBar();

                        EditorGUI.EndDisabledGroup();
                    }

                    break;
            }
        }
    }
}
