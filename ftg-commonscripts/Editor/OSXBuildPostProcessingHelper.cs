﻿using System;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace FlamingTorchGames.CommonScripts
{
    public class OSXBuildPostProcessingHelper : MonoBehaviour
    {
        [PostProcessBuild(1)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            if (target != BuildTarget.StandaloneOSX)
            {
                return;
            }
            Debug.Log("OSX Standalone Script. Executing post process build phase to fix dependencies.");
            Debug.Log("Path to built project: " + pathToBuiltProject);

            // Build paths
            var frameworksPath = Path.Combine(pathToBuiltProject, "Contents/Frameworks/MonoEmbedRuntime/osx");
            var modulesPath = Path.Combine(Application.dataPath, "Plugins/x86_64/");

            // Create MonoEmbedRuntime folder inside Frameworks folder of standalone
            Directory.CreateDirectory(frameworksPath);

            // Copy all *.bundle files from Plugins/x86_64/Firebase to MonoEmbedRuntime/lib*.bundle
            string[] bundleFiles = Directory.GetFiles(modulesPath, "*.bundle");
            foreach (string bundleFile in bundleFiles)
            {
                string bundleFileName = Path.GetFileName(bundleFile);
                FileUtil.CopyFileOrDirectory(Path.Combine(modulesPath, bundleFileName), Path.Combine(frameworksPath, "lib" + bundleFileName));
            }
            // Copy all *.bundle files from Plugins/x86_64/Firebase to MonoEmbedRuntime/lib*.bundle
            string[] dylibFiles = Directory.GetFiles(modulesPath, "*.dylib");
            foreach (string dylibFile in dylibFiles)
            {
                string dylibFileName = Path.GetFileName(dylibFile);
                FileUtil.CopyFileOrDirectory(Path.Combine(modulesPath, dylibFile), Path.Combine(frameworksPath, "lib" + dylibFileName));
            }

            Debug.Log("OSX Standalone Script. Finished executing post process build phase to fix dependencies.");
        }
    }
}
