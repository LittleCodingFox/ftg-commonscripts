﻿#if USING_DARKRIFT

using NUnit.Framework;
using UnityEngine.TestTools;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Tests
{
    public class DarkRiftNetworkingTests
    {
        class CounterMessage : INetworkMessage
        {
            public int counter = 0;
            public bool isOK = false;

            public void Deserialize(INetworkReader reader)
            {
                counter = reader.ReadInt32();
                isOK = reader.ReadBool();
            }

            public void Serialize(INetworkWriter writer)
            {
                writer.WriteInt32(counter);
                writer.WriteBool(isOK);
            }
        }

        class BufferMessage : INetworkMessage
        {
            public byte[] data = new byte[0];

            public void Deserialize(INetworkReader reader)
            {
                data = reader.ReadBytes();
            }

            public void Serialize(INetworkWriter writer)
            {
                writer.WriteBytes(data);
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        [Timeout(500000)]
        public IEnumerator TestOverlappingPacketIDs()
        {
            var clientReceivedCounter = 0;
            var serverReceivedCounter = 0;
            var messagesToSend = 100;

            var server = new DarkRiftGameServer();

            server.OnClientConnected += (playerID) =>
            {
                server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                {
                    counter = 1,
                    isOK = true,
                });
            };

            server.OnClientMessageReceived += (playerID, messageID, reader) =>
            {
                if (messageID != 123)
                {
                    Assert.Fail("Expected a message with ID 123");
                }

                serverReceivedCounter++;

                if (serverReceivedCounter < messagesToSend)
                {
                    server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                    {
                        counter = serverReceivedCounter + 1,
                        isOK = true,
                    });
                }
            };

            server.Create(2048, 10, true);

            var client = new DarkRiftGameClient();

            client.OnConnected += () =>
            {
                client.SendMessage(123, new CounterMessage()
                {
                    counter = 1,
                    isOK = true,
                });
            };

            client.OnMessageReceived += (messageID, reader) =>
            {
                if(messageID != 123)
                {
                    Assert.Fail("Expected a message with ID 123");
                }

                clientReceivedCounter++;

                var message = reader.ReadMessage<CounterMessage>();

                Assert.NotNull(message);

                Assert.AreEqual(clientReceivedCounter, message.counter);
                Assert.AreEqual(true, message.isOK);

                if (clientReceivedCounter < messagesToSend)
                {
                    client.SendMessage(123, new CounterMessage()
                    {
                        counter = clientReceivedCounter,
                        isOK = true,
                    });
                }
            };

            client.Connect("localhost", 2048);

            var framesToWait = 30;

            for (; ; )
            {
                for (var i = 0; i < framesToWait; i++)
                {
                    client.Update();
                    server.Update();

                    yield return null;
                }

                if (clientReceivedCounter == messagesToSend && serverReceivedCounter == messagesToSend)
                {
                    break;
                }
                else
                {
                    Debug.Log($"ClientReceived: {clientReceivedCounter}; ServerReceived: {serverReceivedCounter}");
                }
            }

            Assert.AreEqual(messagesToSend, clientReceivedCounter);
            Assert.AreEqual(messagesToSend, serverReceivedCounter);

            var done = false;

            if (server != null)
            {
                server.OnServerDisposed += () =>
                {
                    done = true;
                };

                server.Shutdown();
                client.Shutdown();

                while (!done)
                {
                    server.Update();
                    client.Update();

                    yield return null;
                }
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        [Timeout(500000)]
        public IEnumerator TestOverlappingPacketIDsNoBatching()
        {
            var clientReceivedCounter = 0;
            var serverReceivedCounter = 0;
            var messagesToSend = 100;

            var server = new DarkRiftGameServer();

            server.OnClientConnected += (playerID) =>
            {
                server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                {
                    counter = 1,
                    isOK = true,
                });
            };

            server.OnClientMessageReceived += (playerID, messageID, reader) =>
            {
                if (messageID != 123)
                {
                    Assert.Fail("Expected a message with ID 123");
                }

                serverReceivedCounter++;

                if (serverReceivedCounter < messagesToSend)
                {
                    server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                    {
                        counter = serverReceivedCounter + 1,
                        isOK = true,
                    });
                }
            };

            server.Create(2048, 10, false);

            var client = new DarkRiftGameClient();

            client.OnConnected += () =>
            {
                client.SendMessage(123, new CounterMessage()
                {
                    counter = 0,
                    isOK = true,
                });
            };

            client.OnMessageReceived += (messageID, reader) =>
            {
                if (messageID != 123)
                {
                    Assert.Fail("Expected a message with ID 123");
                }

                clientReceivedCounter++;

                var message = reader.ReadMessage<CounterMessage>();

                Assert.NotNull(message);

                Assert.AreEqual(clientReceivedCounter, message.counter);
                Assert.AreEqual(true, message.isOK);

                if (clientReceivedCounter < messagesToSend)
                {
                    client.SendMessage(123, new CounterMessage()
                    {
                        counter = clientReceivedCounter,
                        isOK = true,
                    });
                }
            };

            client.Connect("localhost", 2048);

            var framesToWait = 30;

            for (; ; )
            {
                for (var i = 0; i < framesToWait; i++)
                {
                    client.Update();
                    server.Update();

                    yield return null;
                }

                if (clientReceivedCounter == messagesToSend && serverReceivedCounter == messagesToSend)
                {
                    break;
                }
                else
                {
                    Debug.Log($"ClientReceived: {clientReceivedCounter}; ServerReceived: {serverReceivedCounter}");
                }
            }

            Assert.AreEqual(messagesToSend, clientReceivedCounter);
            Assert.AreEqual(messagesToSend, serverReceivedCounter);

            var done = false;

            if (server != null)
            {
                server.OnServerDisposed += () =>
                {
                    done = true;
                };

                server.Shutdown();
                client.Shutdown();

                while (!done)
                {
                    server.Update();
                    client.Update();

                    yield return null;
                }
            }
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        [Timeout(500000)]
        public IEnumerator TestPacketData()
        {
            var server = new DarkRiftGameServer();
            var gotFirst = false;
            var gotSecond = false;

            server.OnClientConnected += (playerID) =>
            {
                server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                {
                    counter = 0,
                    isOK = true,
                });
            };

            server.OnClientMessageReceived += (playerID, messageID, reader) =>
            {
                server.SendMessageToPlayer(playerID, 123, new CounterMessage()
                {
                    counter = 123,
                    isOK = true,
                });
            };

            server.Create(2048, 10, true);

            var client = new DarkRiftGameClient();

            client.OnConnected += () =>
            {
                client.SendMessage(123, new CounterMessage()
                {
                    counter = 0,
                    isOK = true,
                });
            };

            client.OnMessageReceived += (messageID, reader) =>
            {
                var message = reader.ReadMessage<CounterMessage>();

                if(messageID != 123)
                {
                    Assert.Fail("Expected message with ID 123");
                }

                if(message.counter == 0 && !gotFirst)
                {
                    gotFirst = true;
                }
                else if(message.counter == 123 && !gotSecond)
                {
                    gotSecond = true;
                }
                else
                {
                    Assert.Fail("Expected packets in the right order");
                }
            };

            client.Connect("localhost", 2048);

            while(!gotSecond)
            {
                var framesToWait = 30;

                for (var i = 0; i < framesToWait; i++)
                {
                    client.Update();
                    server.Update();

                    yield return null;
                }
            }

            var done = false;

            if (server != null)
            {
                server.OnServerDisposed += () =>
                {
                    done = true;
                };

                server.Shutdown();
                client.Shutdown();

                while (!done)
                {
                    server.Update();
                    client.Update();

                    yield return null;
                }
            }
        }

        [UnityTest]
        [Timeout(1000)]
        public IEnumerator TestBigPacketData()
        {
            var server = new DarkRiftGameServer();
            var gotMessage = false;

            server.OnClientConnected += (playerID) =>
            {
                server.SendMessageToPlayer(playerID, 123, new BufferMessage()
                {
                    data = Enumerable.Repeat((byte)0, 10240).Select((x, xIndex) => (byte)xIndex).ToArray(),
                });
            };

            server.OnClientMessageReceived += (playerID, messageID, reader) =>
            {
                server.SendMessageToPlayer(playerID, 123, new BufferMessage()
                {
                    data = Enumerable.Repeat((byte)0, 10240).Select((x, xIndex) => (byte)xIndex).ToArray(),
                });
            };

            server.Create(2048, 10, true);

            var client = new DarkRiftGameClient();

            client.OnConnected += () =>
            {
                client.SendMessage(123, new BufferMessage()
                {
                    data = Enumerable.Repeat((byte)0, 10240).Select((x, xIndex) => (byte)xIndex).ToArray(),
                });
            };

            client.OnMessageReceived += (messageID, reader) =>
            {
                var message = reader.ReadMessage<BufferMessage>();

                if(message.data.Length == 10240 && messageID == 123 && message.data.SequenceEqual(Enumerable.Repeat((byte)0, 10240).Select((x, xIndex) => (byte)xIndex)))
                {
                    gotMessage = true;
                }
                else
                {
                    Assert.Fail("Expected packet with data field sized 10240");
                }
            };

            client.Connect("localhost", 2048);

            while (!gotMessage)
            {
                var framesToWait = 30;

                for (var i = 0; i < framesToWait; i++)
                {
                    client.Update();
                    server.Update();

                    yield return null;
                }
            }

            var done = false;

            if (server != null)
            {
                server.OnServerDisposed += () =>
                {
                    done = true;
                };

                server.Shutdown();
                client.Shutdown();

                while (!done)
                {
                    server.Update();
                    client.Update();

                    yield return null;
                }
            }
        }

        [UnityTest]
        [Timeout(1000)]
        public IEnumerator TestBatchedMessages()
        {
            var server = new DarkRiftGameServer();
            var counter = 0;
            var messageCount = 1000;

            server.OnClientConnected += (playerID) =>
            {
                for(var i = 0; i < messageCount; i++)
                {
                    server.SendMessageToPlayer(playerID, 123, new BufferMessage()
                    {
                        data = new byte[10],
                    });
                }
            };

            server.Create(2048, 10, true);

            var client = new DarkRiftGameClient();

            client.OnMessageReceived += (messageID, reader) =>
            {
                var message = reader.ReadMessage<BufferMessage>();

                if (message.data.Length == 10 && messageID == 123)
                {
                    counter++;
                }
                else
                {
                    Assert.Fail("Expected packet with data field sized 10240");
                }
            };

            client.Connect("localhost", 2048);

            while (counter < messageCount)
            {
                var previousCounter = counter;

                var framesToWait = 30;

                for (var i = 0; i < framesToWait; i++)
                {
                    client.Update();
                    server.Update();

                    yield return null;
                }

                if(counter == previousCounter && counter != messageCount)
                {
                    Assert.Fail("Missing batched messages");
                }
            }

            Assert.AreEqual(messageCount, counter);

            var done = false;

            if (server != null)
            {
                server.OnServerDisposed += () =>
                {
                    done = true;
                };

                server.Shutdown();
                client.Shutdown();

                while (!done)
                {
                    server.Update();
                    client.Update();

                    yield return null;
                }
            }
        }
    }
}
#endif
