﻿#if USING_DARKRIFT

using NUnit.Framework;
using UnityEngine.TestTools;
using System.Collections;
using UnityEngine;
using System.Linq;

namespace FlamingTorchGames.CommonScripts.Networking.Tests
{
    public class NetworkBatcherTests
    {
        ushort messageID = 123;
        byte[] data = new byte[] { 3, 2, 1 };

        [Test]
        public void TestEncodeDecode()
        {
            var result = NetworkBatcher.Encode(messageID, data);

            int position = 0;

            Assert.AreEqual(NetworkBatcher.DecodeStatus.Success, NetworkBatcher.Decode(result, ref position, out var outMessageID, out var outData));
            Assert.AreEqual(messageID, outMessageID);
            Assert.AreEqual(data, outData);
        }

        [Test]
        public void TestEncoding()
        {
            var result = NetworkBatcher.Encode(messageID, data);
            var empty = NetworkBatcher.Encode(messageID, new byte[0]);

            var sizeVarint = VarintBitConverter.GetVarintBytes(data.Length);

            var position = 0;

            Assert.AreEqual(sizeof(ushort) + sizeof(byte) + sizeVarint.Length + data.Length, result.Length);
            Assert.AreEqual(NetworkBatcher.DecodeStatus.Success, NetworkBatcher.Decode(result, ref position, out var outID, out var outData));
            Assert.AreEqual(result.Length, position);

            var clone = new byte[0].Concat(result)
                .Concat(result)
                .Concat(empty)
                .ToArray();

            Assert.AreEqual(NetworkBatcher.DecodeStatus.Success, NetworkBatcher.Decode(clone, ref position, out outID, out outData));
            Assert.AreEqual(result.Length * 2, position);

            Assert.AreEqual(NetworkBatcher.DecodeStatus.Success, NetworkBatcher.Decode(clone, ref position, out outID, out outData));
            Assert.AreEqual(result.Length * 2 + empty.Length, position);
        }

        [Test]
        public void TestDecoding()
        {
            var result = NetworkBatcher.Encode(messageID, data);

            var sizeVarint = VarintBitConverter.GetVarintBytes(data.Length);

            Assert.AreEqual(sizeof(ushort) + sizeof(byte) + sizeVarint.Length + data.Length, result.Length);
            Assert.AreEqual((byte)messageID, result[0]);
            Assert.AreEqual((byte)(messageID >> 8), result[1]);
            Assert.AreEqual((byte)sizeVarint.Length, result[2]);

            for (var i = 0; i < sizeVarint.Length; i++)
            {
                Assert.AreEqual(sizeVarint[i], result[i + 3]);
            }

            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], result[i + 3 + sizeVarint.Length]);
            }

            Assert.AreEqual(data.Length + 3 + sizeVarint.Length, result.Length);
        }

        [Test]
        public void TestBatching()
        {
            var encoded = NetworkBatcher.Encode(messageID, data);
            var batcher = new NetworkBatcher();

            for(var i = 0; i < 10; i++)
            {
                Assert.IsTrue(batcher.Batch(messageID, data));

                Assert.AreEqual(batcher.Length, (i + 1) * encoded.Length);
            }
        }
    }
}
#endif
