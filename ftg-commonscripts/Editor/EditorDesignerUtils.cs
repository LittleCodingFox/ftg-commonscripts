﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using RoboRyanTron.SearchableEnum.Editor;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class EditorDesignerUtils
        {
            private static GUIStyle linkStyle;
            public static GUIStyle LinkStyle
            {
                get
                {
                    if(linkStyle == null)
                    {
                        linkStyle = new GUIStyle(GUI.skin.label);
                        linkStyle.wordWrap = false;
                        // Match selection color which works nicely for both light and dark skins
                        linkStyle.normal.textColor = new Color(0x00 / 255f, 0x78 / 255f, 0xDA / 255f, 1f);
                        linkStyle.stretchWidth = false;
                    }

                    return linkStyle;
                }
            }

            public static bool LinkLabelLayout(GUIContent label, int size, params GUILayoutOption[] options)
            {
                var fontSize = LinkStyle.fontSize;
                LinkStyle.fontSize = size;

                var position = GUILayoutUtility.GetRect(label, LinkStyle, options);

                Handles.BeginGUI();
                Handles.color = LinkStyle.normal.textColor;
                Handles.DrawLine(new Vector3(position.xMin, position.yMax), new Vector3(position.xMax, position.yMax));
                Handles.color = Color.white;
                Handles.EndGUI();

                EditorGUIUtility.AddCursorRect(position, MouseCursor.Link);

                var result = GUI.Button(position, label, LinkStyle);

                LinkStyle.fontSize = fontSize;

                return result;
            }

            public static bool LinkLabelLayout(GUIContent label, params GUILayoutOption[] options)
            {
                return LinkLabelLayout(label, LinkStyle.fontSize, options);
            }

            public static bool LinkLabel(ref Rect rect, GUIContent label, int size)
            {
                var fontSize = LinkStyle.fontSize;
                LinkStyle.fontSize = size;

                var tempRect = new Rect(rect.x, rect.y, LinkStyle.CalcSize(label).x, size);

                Handles.BeginGUI();
                Handles.color = LinkStyle.normal.textColor;
                Handles.DrawLine(new Vector3(tempRect.xMin, tempRect.yMax), new Vector3(tempRect.xMax, tempRect.yMax));
                Handles.color = Color.white;
                Handles.EndGUI();

                EditorGUIUtility.AddCursorRect(tempRect, MouseCursor.Link);

                var result = GUI.Button(tempRect, label, LinkStyle);

                LinkStyle.fontSize = fontSize;

                rect.x += tempRect.x;

                return result;
            }

            public static bool LinkLabel(ref Rect rect, GUIContent label)
            {
                return LinkLabel(ref rect, label, LinkStyle.fontSize);
            }

            public static void Label(ref Rect rect, string content, float width = 0.0f)
            {
                if (width == 0.0f)
                {
                    width = GUI.skin.label.CalcSize(new GUIContent(content)).x;
                }

                EditorGUI.LabelField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), content);

                rect.x += width;
            }

            public static void Label(ref Rect rect, string content, int fontSize, float width = 0.0f)
            {
                var style = new GUIStyle(GUI.skin.label)
                {
                    fontSize = fontSize
                };

                var size = style.CalcSize(new GUIContent(content));

                if (width == 0.0f)
                {
                    width = size.x;
                }

                EditorGUI.LabelField(new Rect(rect.x, rect.y, width, size.y), content, style);

                rect.x += width;
            }

            public static float LabelWidth(string content)
            {
                return GUI.skin.label.CalcSize(new GUIContent(content)).x;
            }

            public static void PrefixLabel(ref Rect rect, string content)
            {
                float width = GUI.skin.label.CalcSize(new GUIContent(content)).x;

                EditorGUI.PrefixLabel(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), new GUIContent(content));

                rect.x += width;
            }

            public static int Popup(ref Rect rect, List<string> content, int current)
            {
                string currentString = current >= 0 && current < content.Count ? content[current] : "";
                float width = GUI.skin.label.CalcSize(new GUIContent(currentString)).x + 16;

                int outResult = EditorGUI.Popup(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), current, content.ToArray());

                rect.x += width;

                return outResult;
            }

            public static int PopupLayout(List<string> content, int current, string label = "")
            {
                return EditorGUILayout.Popup(label, current, content.ToArray());
            }

            public static void SearchablePopupLayout(List<string> content, int current, System.Action<int> onSelected)
            {
                GUIContent buttonText =
                    new GUIContent(current >= 0 && current < content.Count ? content[current] : "");

                var lastRect = GUILayoutUtility.GetRect(GUI.skin.label.CalcSize(buttonText).x + 16, 16, GUIStyle.none, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));

                int id = GUIUtility.GetControlID(FocusType.Keyboard);

                if(SearchableDropdownButton(id, lastRect, buttonText))
                {
                    SearchablePopup.Show(lastRect, content.ToArray(), current, onSelected);
                }
            }

            public static void SearchablePopupRect(ref Rect rect, List<string> content, int current, System.Action<int> onSelected)
            {
                GUIContent buttonText =
                    new GUIContent(current >= 0 && current < content.Count ? content[current] : "");

                float width = GUI.skin.label.CalcSize(buttonText).x + 16;

                var oldRect = rect;
                oldRect.width = width;

                int id = GUIUtility.GetControlID(FocusType.Keyboard);
                
                if (SearchableDropdownButton(id, oldRect, buttonText))
                {
                    SearchablePopup.Show(oldRect, content.ToArray(), current, onSelected);
                }

                rect.x += width;
            }

            private static bool SearchableDropdownButton(int id, Rect position, GUIContent content)
            {
                Event current = Event.current;
                switch (current.type)
                {
                    case EventType.MouseDown:
                        if (position.Contains(current.mousePosition) && current.button == 0)
                        {
                            Event.current.Use();
                            return true;
                        }
                        break;
                    case EventType.KeyDown:
                        if (GUIUtility.keyboardControl == id && current.character == '\n')
                        {
                            Event.current.Use();
                            return true;
                        }
                        break;
                    case EventType.Repaint:
                        EditorStyles.popup.Draw(position, content, id, false);
                        break;
                }
                return false;
            }

            public static EnumType EnumPopup<EnumType>(ref Rect rect, EnumType current) where EnumType : struct, System.IConvertible
            {
                List<EnumType> values = new List<EnumType>((EnumType[])System.Enum.GetValues(typeof(EnumType)));
                List<string> names = new List<string>(System.Enum.GetNames(typeof(EnumType)));
                List<string> displayNames = names.Select(x => x.ExpandedName()).ToList();

                int outResult = Popup(ref rect, displayNames, values.IndexOf(current));

                if(outResult >= 0 && outResult < values.Count)
                {
                    return values[outResult];
                }

                return current;
            }

            public static EnumType EnumPopupLayout<EnumType>(EnumType current, string label = "") where EnumType : struct, System.IConvertible
            {
                List<EnumType> values = new List<EnumType>((EnumType[])System.Enum.GetValues(typeof(EnumType)));
                List<string> names = new List<string>(System.Enum.GetNames(typeof(EnumType)));
                List<string> displayNames = names.Select(x => x.ExpandedName()).ToList();

                int outResult = PopupLayout(displayNames, values.IndexOf(current), label);

                if (outResult >= 0 && outResult < values.Count)
                {
                    return values[outResult];
                }

                return current;
            }

            public static float FloatField(ref Rect rect, float value, float width)
            {
                float outResult = EditorGUI.FloatField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static int IntField(ref Rect rect, int value, float width)
            {
                int outResult = EditorGUI.IntField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static bool Toggle(ref Rect rect, bool value, float width)
            {
                bool outResult = EditorGUI.Toggle(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static Color ColorField(ref Rect rect, Color value, float width)
            {
                Color outResult = EditorGUI.ColorField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static Vector2 Vector2Field(ref Rect rect, Vector2 value, float width)
            {
                Vector2 outResult = EditorGUI.Vector2Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static Vector3 Vector3Field(ref Rect rect, Vector3 value, float width)
            {
                Vector3 outResult = EditorGUI.Vector3Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static Vector4 Vector4Field(ref Rect rect, Vector4 value, float width)
            {
                Vector4 outResult = EditorGUI.Vector4Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static string TextField(ref Rect rect, string value, float width)
            {
                string outResult = EditorGUI.TextField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static string TextArea(ref Rect rect, string value, float width)
            {
                string outResult = EditorGUI.TextArea(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static ObjectType ObjectField<ObjectType>(ref Rect rect, ObjectType value) where ObjectType : Object
            {
                float width = 120.0f;

                ObjectType outResult = (ObjectType)EditorGUI.ObjectField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value, typeof(ObjectType), false);
                rect.x += width;

                return outResult;
            }

            public static ObjectType ObjectFieldLayout<ObjectType>(ObjectType value) where ObjectType : Object
            {
                return (ObjectType)EditorGUILayout.ObjectField(value, typeof(ObjectType), false);
            }
			
            public static void DrawSprite(Rect rect, Sprite sprite)
            {
                if(sprite != null)
                {
                    Texture texture = sprite.texture;
                    Rect textureRect = sprite.textureRect;
                    Rect texCoords = new Rect(textureRect.x / texture.width, textureRect.y / texture.height, textureRect.width / texture.width, textureRect.height / texture.height);

                    GUI.DrawTextureWithTexCoords(new Rect(rect.x, rect.y, textureRect.width, textureRect.height), texture, texCoords);
                }
            }

            public static void DrawSpriteLayout(Sprite sprite)
            {
                if (sprite != null)
                {
                    Texture texture = sprite.texture;
                    Rect textureRect = sprite.textureRect;
                    Rect texCoords = new Rect(textureRect.x / texture.width, textureRect.y / texture.height, textureRect.width / texture.width, textureRect.height / texture.height);

                    Rect rect = GUILayoutUtility.GetRect(textureRect.width, textureRect.height);

                    GUI.DrawTextureWithTexCoords(new Rect(rect.x, rect.y, textureRect.width, textureRect.height), texture, texCoords);
                }
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, string value)
            {
                if(property.stringValue == value)
                {
                    return false;
                }

                property.stringValue = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, int value)
            {
                if (property.intValue == value)
                {
                    return false;
                }

                property.intValue = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, Vector2 value)
            {
                if (property.vector2Value == value)
                {
                    return false;
                }

                property.vector2Value = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, Vector3 value)
            {
                if (property.vector3Value == value)
                {
                    return false;
                }

                property.vector3Value = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, Vector4 value)
            {
                if (property.vector4Value == value)
                {
                    return false;
                }

                property.vector4Value = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, bool value)
            {
                if (property.boolValue == value)
                {
                    return false;
                }

                property.boolValue = value;

                return true;
            }

            public static bool SetEditorPropertyValue(this SerializedProperty property, float value)
            {
                if (property.floatValue == value)
                {
                    return false;
                }

                property.floatValue = value;

                return true;
            }
        }
    }
}
