﻿#if USING_NANINOVEL

using Naninovel;
using UniRx.Async;

namespace FlamingTorchGames.CommonScripts
{
    public class NaninovelUtils
    {
        public static void PlayScript(string name, string label)
        {
            var inputManager = Engine.GetService<IInputManager>();
            inputManager.ProcessInput = true;

            var scriptPlayer = Engine.GetService<IScriptPlayer>();

            if(label.Length > 0)
            {
                scriptPlayer.PreloadAndPlayAsync(name, label: label).Forget();
            }
            else
            {
                scriptPlayer.PreloadAndPlayAsync(name).Forget();
            }
        }
    }
}
#endif
