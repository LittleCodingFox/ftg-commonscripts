﻿#if USING_NANINOVEL

using Naninovel;
using Naninovel.Commands;
using UniRx.Async;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class NaninovelSetup : MonoBehaviour
    {
        private static NaninovelSetup instance;

        private async void Start()
        {
            if(instance != null)
            {
                Destroy(gameObject);

                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);

            // 1. Initialize Naninovel.
            await RuntimeInitializer.InitializeAsync();

            // 2. Disable Naninovel input.
            var inputManager = Engine.GetService<IInputManager>();
            inputManager.ProcessInput = false;

            // 3. Stop script player.
            var scriptPlayer = Engine.GetService<IScriptPlayer>();
            scriptPlayer.Stop();

            // 4. Hide text printer.
            var hidePrinter = new HidePrinter();
            hidePrinter.ExecuteAsync().Forget();

            // 5. Reset state.
            var stateManager = Engine.GetService<IStateManager>();
            await stateManager.ResetStateAsync();

            // 6. Disable Nani camera.
            var naniCamera = Engine.GetService<ICameraManager>().Camera;
            naniCamera.enabled = false;
        }
    }
}

#endif
