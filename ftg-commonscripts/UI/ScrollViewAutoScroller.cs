﻿using UnityEngine;
using UnityEngine.UI;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        [RequireComponent(typeof(ScrollRect))]
        public class ScrollViewAutoScroller : MonoBehaviour
        {
            private ScrollRect scrollRect;

            private void Start()
            {
                scrollRect = GetComponent<ScrollRect>();
            }

            private void FixedUpdate()
            {
                if (scrollRect != null)
                {
                    scrollRect.verticalNormalizedPosition = 0.0f;
                }
            }
        }
    }
}
