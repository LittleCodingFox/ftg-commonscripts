﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class CheapGridLayout : MonoBehaviour
    {
        public Vector2 cellSize;
        public RectOffset padding;
        public Vector2 spacing;

        public RectTransform rectTransform
        {
            get;
            private set;
        }

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        public void UpdateLayout()
        {
            var position = new Vector2(padding.left + cellSize.x / 2, padding.top - cellSize.y / 2);

            foreach(RectTransform child in transform)
            {
                child.sizeDelta = cellSize;

                var targetPosition = new Vector2(position.x, position.y);

                if(targetPosition.x + cellSize.x / 2 + padding.right > rectTransform.rect.width)
                {
                    targetPosition.x = padding.left + cellSize.x / 2;
                    targetPosition.y -= cellSize.y + spacing.y;
                }

                child.anchoredPosition = targetPosition;

                targetPosition.x += cellSize.x + spacing.x;

                position = targetPosition;
            }
        }
    }
}
