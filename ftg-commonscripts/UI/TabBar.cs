﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace FlamingTorchGames.CommonScripts
{
    public class TabBar : MonoBehaviour
    {
        [System.Serializable]
        public class TabBarInfo
        {
            public string name;
            public GameObject tabButton;
            public GameObject container;
            public List<GameObject> enabledObjects = new List<GameObject>();
            public List<GameObject> disabledObjects = new List<GameObject>();
        }

        public List<TabBarInfo> tabs = new List<TabBarInfo>();
        public ScrollRect scrollRect;

        public delegate void TabChangedDelegate(TabBarInfo currentTab);

        public TabChangedDelegate onTabChanged;

        [System.NonSerialized]
        private TabBarInfo activeTab;
        public TabBarInfo ActiveTab
        {
            get
            {
                return activeTab ?? tabs.FirstOrDefault();
            }
        }

        public GameObject ContainerForTab(string name)
        {
            return tabs.FirstOrDefault(x => x.name == name)?.container;
        }

        public void SetActiveTab(string name)
        {
            if(activeTab?.name == name)
            {
                return;
            }

            activeTab = null;

            foreach(var tab in tabs)
            {
                tab.container.SetActive(tab.name == name);

                if(tab.container.activeSelf)
                {
                    activeTab = tab;

                    foreach(var gameObject in tab.enabledObjects)
                    {
                        gameObject.SetActive(true);
                    }

                    foreach(var gameObject in tab.disabledObjects)
                    {
                        gameObject.SetActive(false);
                    }
                }
            }

            scrollRect.verticalNormalizedPosition = 1;

            if (activeTab != null)
            {
                onTabChanged?.Invoke(activeTab);
            }
        }
    }
}
