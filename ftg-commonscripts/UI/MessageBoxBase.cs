﻿using FlamingTorchGames.CommonScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace FlamingTorchGames.CommonScripts
{
    public class MessageBoxBase : MonoBehaviour
    {
        public enum MessageBoxButtonStyle
        {
            None,
            OK,
            YesNo,
            Checkbox,
            TextField,
            TextFieldYesNo,
        }

        public float padding = 40;
        public float titleYPadding = 50;
        public float messageYPadding = 20;

        public float animationTime = 0.5f;

        public System.Action onConfirm;
        public System.Action onPositive;
        public System.Action onNegative;
        public System.Action<bool> onConfirmCheckbox;
        public System.Action<string> onConfirmTextField;

        private TMP_Text titleText;
        private TMP_Text messageText;
        private RectTransform rectTransform;
        private Button confirmationButton;
        private Button positiveButton;
        private Button negativeButton;
        private TMP_Text confirmationButtonText;
        private TMP_Text positiveButtonText;
        private TMP_Text negativeButtonText;
        private Toggle checkbox;
        private TMP_Text checkboxTitle;
        private TMP_InputField textField;
        private RectTransform confirmationButtonRectTransform;
        private bool checkboxValue = false;
        private string textFieldValue = "";

        public static bool Showing
        {
            get
            {
                return FindObjectOfType<MessageBoxBase>() != null;
            }
        }

        public string Title
        {
            get
            {
                return titleText != null ? titleText.text : "";
            }

            set
            {
                if (titleText != null && titleText.text != value)
                {
                    titleText.text = value;
                }
            }
        }

        public string Message
        {
            get
            {
                return messageText != null ? messageText.text : "";
            }

            set
            {
                if (messageText != null && messageText.text != value)
                {
                    messageText.text = value;
                }

                UpdateSize();
            }
        }

        public string ConfirmationTitle
        {
            get
            {
                return confirmationButtonText != null ? confirmationButtonText.text : "";
            }

            set
            {
                if (confirmationButtonText != null && confirmationButtonText.text != value)
                {
                    confirmationButtonText.text = value;
                }
            }
        }

        public string PositiveTitle
        {
            get
            {
                return positiveButtonText != null ? positiveButtonText.text : "";
            }

            set
            {
                if (positiveButtonText != null && positiveButtonText.text != value)
                {
                    positiveButtonText.text = value;
                }
            }
        }

        public string NegativeTitle
        {
            get
            {
                return negativeButtonText != null ? negativeButtonText.text : "";
            }

            set
            {
                if (negativeButtonText != null && negativeButtonText.text != value)
                {
                    negativeButtonText.text = value;
                }
            }
        }

        public string CheckboxTitle
        {
            get
            {
                return checkboxTitle != null ? checkboxTitle.text : "";
            }

            set
            {
                if(checkboxTitle != null && checkboxTitle.text != value)
                {
                    checkboxTitle.text = value;
                }
            }
        }

        public string TextFieldPlaceholder
        {
            get
            {
                var placeholder = textField?.transform.SearchAndGetComponent<TextMeshProUGUI>("Placeholder");

                if(placeholder != null)
                {
                    return placeholder.text;
                }

                return "";
            }

            set
            {
                var placeholder = textField?.transform.SearchAndGetComponent<TextMeshProUGUI>("Placeholder");

                if (placeholder != null)
                {
                    placeholder.text = value;
                }
            }
        }

        private MessageBoxButtonStyle privButtonStyle;
        public MessageBoxButtonStyle ButtonStyle
        {
            get
            {
                return privButtonStyle;
            }

            set
            {
                privButtonStyle = value;

                switch (privButtonStyle)
                {
                    case MessageBoxButtonStyle.None:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(false);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        if (checkbox != null)
                        {
                            checkbox.gameObject.SetActive(false);
                        }

                        if (checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(false);
                        }

                        if(textField != null)
                        {
                            textField.gameObject.SetActive(false);
                        }

                        break;

                    case MessageBoxButtonStyle.OK:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(true);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        if (checkbox != null)
                        {
                            checkbox.gameObject.SetActive(false);
                        }

                        if (checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(false);
                        }

                        if (textField != null)
                        {
                            textField.gameObject.SetActive(false);
                        }

                        break;

                    case MessageBoxButtonStyle.YesNo:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(false);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(true);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(true);
                        }

                        if (checkbox != null)
                        {
                            checkbox.gameObject.SetActive(false);
                        }

                        if (checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(false);
                        }

                        if (textField != null)
                        {
                            textField.gameObject.SetActive(false);
                        }

                        break;

                    case MessageBoxButtonStyle.Checkbox:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(true);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        if(checkbox != null)
                        {
                            checkbox.gameObject.SetActive(true);
                        }

                        if(checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(true);
                        }

                        if (textField != null)
                        {
                            textField.gameObject.SetActive(false);
                        }

                        break;

                    case MessageBoxButtonStyle.TextField:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(true);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        if (checkbox != null)
                        {
                            checkbox.gameObject.SetActive(false);
                        }

                        if (checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(false);
                        }

                        if (textField != null)
                        {
                            textField.gameObject.SetActive(true);
                        }

                        break;

                    case MessageBoxButtonStyle.TextFieldYesNo:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(false);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(true);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(true);
                        }

                        if (checkbox != null)
                        {
                            checkbox.gameObject.SetActive(false);
                        }

                        if (checkboxTitle != null)
                        {
                            checkboxTitle.gameObject.SetActive(false);
                        }

                        if (textField != null)
                        {
                            textField.gameObject.SetActive(true);
                        }

                        break;
                }
            }
        }

        private static bool applicationRunning = true;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void SetupStubs()
        {
            Application.quitting += () =>
            {
                applicationRunning = false;
            };
        }

        private void Awake()
        {
            titleText = transform.SearchAndGetComponent<TMP_Text>("Title");
            messageText = transform.SearchAndGetComponent<TMP_Text>("Message");
            rectTransform = transform.SearchAndGetComponent<RectTransform>("Content");
            confirmationButton = transform.SearchAndGetComponent<Button>("ConfirmButton");

            if (confirmationButton != null)
            {
                confirmationButtonRectTransform = confirmationButton.GetComponent<RectTransform>();
                confirmationButtonText = confirmationButton.transform.GetComponentInChildren<TMP_Text>();

                confirmationButton.onClick.AddListener(() =>
                {
                    if(ButtonStyle == MessageBoxButtonStyle.Checkbox)
                    {
                        onConfirmCheckbox?.Invoke(checkboxValue);
                    }
                    else if(ButtonStyle == MessageBoxButtonStyle.TextField)
                    {
                        onConfirmTextField?.Invoke(textFieldValue);
                    }
                    else
                    {
                        onConfirm?.Invoke();
                    }

                    Destroy(gameObject);
                });
            }

            positiveButton = transform.SearchAndGetComponent<Button>("PositiveButton");

            if (positiveButton != null)
            {
                positiveButtonText = positiveButton.transform.GetComponentInChildren<TMP_Text>();

                positiveButton.onClick.AddListener(() =>
                {
                    if (ButtonStyle == MessageBoxButtonStyle.TextFieldYesNo)
                    {
                        onConfirmTextField?.Invoke(textFieldValue);
                    }
                    else
                    {
                        onPositive?.Invoke();
                    }

                    Destroy(gameObject);
                });
            }

            negativeButton = transform.SearchAndGetComponent<Button>("NegativeButton");

            if (negativeButton != null)
            {
                negativeButtonText = negativeButton.transform.GetComponentInChildren<TMP_Text>();

                negativeButton.onClick.AddListener(() =>
                {
                    onNegative?.Invoke();

                    Destroy(gameObject);
                });
            }

            checkbox = transform.SearchAndGetComponent<Toggle>("Checkbox");

            if(checkbox != null)
            {
                checkboxValue = checkbox.isOn;

                checkbox.onValueChanged.AddListener((value) =>
                {
                    checkboxValue = value;
                });
            }

            checkboxTitle = transform.SearchAndGetComponent<TextMeshProUGUI>("CheckboxTitle");

            textField = transform.SearchAndGetComponent<TMP_InputField>("TextField");

            if(textField != null)
            {
                textField.text = "";

                textField.onValueChanged.AddListener((value) =>
                {
                    textFieldValue = value;
                });
            }
        }

        /// <summary>
        /// Updates the size of this message box
        /// </summary>
        private void UpdateSize()
        {
            Canvas.ForceUpdateCanvases();

            var height = titleText.rectTransform.sizeDelta.y;
            height += messageText.rectTransform.sizeDelta.y + titleYPadding;

            if (confirmationButtonRectTransform != null)
            {
                height += confirmationButtonRectTransform.sizeDelta.y + messageYPadding;
            }

            if(ButtonStyle == MessageBoxButtonStyle.Checkbox && checkboxTitle != null)
            {
                height += checkboxTitle.rectTransform.sizeDelta.y + messageYPadding;
            }
            else if ((ButtonStyle == MessageBoxButtonStyle.TextField || ButtonStyle == MessageBoxButtonStyle.TextFieldYesNo) && textField != null)
            {
                height += textField.GetComponent<RectTransform>().sizeDelta.y + messageYPadding;
            }

            height += padding;

            titleText.rectTransform.anchoredPosition = new Vector2(titleText.rectTransform.anchoredPosition.x, -(titleText.rectTransform.sizeDelta.y / 2 + titleYPadding));
            messageText.rectTransform.anchoredPosition = new Vector2(messageText.rectTransform.anchoredPosition.x, -(messageText.rectTransform.sizeDelta.y / 2 + messageYPadding +
                titleText.rectTransform.sizeDelta.y + titleYPadding));

            if (ButtonStyle == MessageBoxButtonStyle.Checkbox && checkboxTitle != null)
            {
                checkboxTitle.rectTransform.anchoredPosition = new Vector2(checkboxTitle.rectTransform.anchoredPosition.x, -(checkboxTitle.rectTransform.sizeDelta.y + messageYPadding +
                    messageText.rectTransform.sizeDelta.y + messageYPadding +
                    titleText.rectTransform.sizeDelta.y + titleYPadding));

                var checkboxRectTransform = checkbox.GetComponent<RectTransform>();

                checkboxRectTransform.anchoredPosition = new Vector2(checkboxRectTransform.anchoredPosition.x, -(checkboxTitle.rectTransform.sizeDelta.y + messageYPadding + 
                    messageText.rectTransform.sizeDelta.y + messageYPadding +
                    titleText.rectTransform.sizeDelta.y + titleYPadding));
            }
            else if((ButtonStyle == MessageBoxButtonStyle.TextField || ButtonStyle == MessageBoxButtonStyle.TextFieldYesNo) && textField != null)
            {
                var rectTransform = textField.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, -(rectTransform.sizeDelta.y / 2 + messageYPadding +
                    messageText.rectTransform.sizeDelta.y + messageYPadding +
                    titleText.rectTransform.sizeDelta.y + titleYPadding));
            }

            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, height);
        }

        /// <summary>
        /// Clears all active message boxes
        /// </summary>
        public static void Clear()
        {
            var instances = FindObjectsOfType<MessageBoxBase>();

            foreach(var instance in instances)
            {
                Destroy(instance.gameObject);
            }
        }

        /// <summary>
        /// Creates an instance of a message box with no actions
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase Create(string title, string message)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.None;
            }

            return outValue;
        }

        /// <summary>
        /// Creates an instance of a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase Create(string title, string message, string okButtonTitle, System.Action onOK = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.OK;
                outValue.ConfirmationTitle = okButtonTitle;
                outValue.onConfirm = onOK;
            }

            return outValue;
        }

        /// <summary>
        /// Creates an instance of a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase CreateCheckbox(string title, string message, string okButtonTitle, string checkboxTitle, System.Action<bool> onOK = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBoxCheckbox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.ButtonStyle = MessageBoxButtonStyle.Checkbox;
                outValue.ConfirmationTitle = okButtonTitle;
                outValue.CheckboxTitle = checkboxTitle;
                outValue.onConfirmCheckbox = onOK;
                outValue.Message = message;
            }

            return outValue;
        }

        /// <summary>
        /// Creates an instance of a message box wtih a text field with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="maxLength">The maximum amount of characters in the text field</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase CreateTextField(string title, string message, int? maxLength, string okButtonTitle, System.Action<string> onOK = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBoxTextField");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.ButtonStyle = MessageBoxButtonStyle.TextField;
                outValue.ConfirmationTitle = okButtonTitle;
                outValue.onConfirmTextField = onOK;
                outValue.Message = message;

                if(maxLength.HasValue && outValue.textField != null)
                {
                    outValue.textField.characterLimit = maxLength.Value;
                }
            }

            return outValue;
        }

        /// <summary>
        /// Creates an instance of a message box wtih a text field with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="maxLength">The maximum amount of characters in the text field</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase CreateTextField(string title, string message, int? maxLength, string yesButtonTitle, string noButtonTitle, System.Action<string> onYes = null, System.Action onNo = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBoxTextField");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.ButtonStyle = MessageBoxButtonStyle.TextFieldYesNo;
                outValue.PositiveTitle = yesButtonTitle;
                outValue.NegativeTitle = noButtonTitle;
                outValue.onConfirmTextField = onYes;
                outValue.onNegative = onNo;
                outValue.Message = message;

                if (maxLength.HasValue && outValue.textField != null)
                {
                    outValue.textField.characterLimit = maxLength.Value;
                }
            }

            return outValue;
        }

        /// <summary>
        /// Creates a message box with a positive and negative action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="yesButtonTitle">The title of the positive action</param>
        /// <param name="noButtonTitle">The title of the negative action</param>
        /// <param name="onYes">The action to perform when selecting the positive action</param>
        /// <param name="onNo">The action to perform when selecting the negative action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>The created message box, if successful</returns>
        public static MessageBoxBase Create(string title, string message, string yesButtonTitle, string noButtonTitle,
            System.Action onYes = null, System.Action onNo = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if(instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.YesNo;
                outValue.PositiveTitle = yesButtonTitle;
                outValue.NegativeTitle = noButtonTitle;
                outValue.onPositive = onYes;
                outValue.onNegative = onNo;
            }

            return outValue;
        }

#if USING_ITWEEN
        /// <summary>
        /// Shows and animates this message box
        /// </summary>
        /// <param name="easeType">The type of easing to use</param>
        public void Show(iTween.EaseType easeType = iTween.EaseType.easeOutQuart, bool instant = false)
        {
            if(!applicationRunning)
            {
                return;
            }

            if(instant)
            {
                rectTransform.localScale = Vector3.one;
            }
            else
            {
                rectTransform.localScale = Vector3.zero;

                iTween.ScaleTo(rectTransform.gameObject, iTween.Hash("scale", Vector3.one, "time", animationTime, "islocal", true,
                    "easetype", easeType));
            }
        }

        /// <summary>
        /// Shows a message box with no actions
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>The message box instance, if successfully created</returns>
        public static MessageBoxBase Show(string title, string message, iTween.EaseType easeType = iTween.EaseType.easeOutQuart, bool instant = false)
        {
            if (!applicationRunning)
            {
                return null;
            }

            var instance = Create(title, message);

            if (instance != null)
            {
                instance.Show(easeType, instant);
            }

            return instance;
        }

        /// <summary>
        /// Shows a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool Show(string title, string message, string okButtonTitle, System.Action onOK = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            if (!applicationRunning)
            {
                return false;
            }

            var instance = Create(title, message, okButtonTitle, onOK);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }

        /// <summary>
        /// Shows a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool ShowCheckbox(string title, string message, string okButtonTitle, string checkboxTitle, System.Action<bool> onOK = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            if (!applicationRunning)
            {
                return false;
            }

            var instance = CreateCheckbox(title, message, okButtonTitle, checkboxTitle, onOK);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }

        /// <summary>
        /// Shows a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="maxLength">The maximum amount of characters in the text field</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool ShowTextField(string title, string message, string okButtonTitle,
            int? maxLength = null, System.Action<string> onOK = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            if (!applicationRunning)
            {
                return false;
            }

            var instance = CreateTextField(title, message, maxLength, okButtonTitle, onOK);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }

        /// <summary>
        /// Shows a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="maxLength">The maximum amount of characters in the text field</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool ShowTextField(string title, string message, string yesTitle, string noTitle,
            int? maxLength = null, System.Action<string> onYes = null, System.Action onNo = null,
            iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            if (!applicationRunning)
            {
                return false;
            }

            var instance = CreateTextField(title, message, maxLength, yesTitle, noTitle, onYes, onNo);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }

        /// <summary>
        /// Shows a message box with a positive and negative action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="yesButtonTitle">The title of the positive action</param>
        /// <param name="noButtonTitle">The title of the negative action</param>
        /// <param name="onYes">The action to perform when selecting the positive action</param>
        /// <param name="onNo">The action to perform when selecting the negative action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool Show(string title, string message, string yesButtonTitle, string noButtonTitle,
            System.Action onYes = null, System.Action onNo = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            if (!applicationRunning)
            {
                return false;
            }

            var instance = Create(title, message, yesButtonTitle, noButtonTitle, onYes, onNo);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }
#endif
    }
}
