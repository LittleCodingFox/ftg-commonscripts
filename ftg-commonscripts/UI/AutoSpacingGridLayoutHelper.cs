﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FlamingTorchGames.CommonScripts
{
    [ExecuteInEditMode]
    public class AutoSpacingGridLayoutHelper : MonoBehaviour
    {
        [HideInInspector]
        public GridLayoutGroup layout;
        public bool performAtEditTime = true;
        private bool didOnce = false;

        private void Update()
        {
            if(layout == null)
            {
                layout = GetComponent<GridLayoutGroup>();
            }

            if(layout == null)
            {
                return;
            }

            if(Application.isPlaying || !performAtEditTime)
            {
                if(didOnce)
                {
                    return;
                }

                didOnce = true;
            }

            layout.spacing = layout.CenteredSpacing();
        }
    }
}
