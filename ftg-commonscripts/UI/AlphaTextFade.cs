﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class AlphaTextFade : MonoBehaviour
        {
            private class StyleChangeInfo
            {
                public Color color;
                public bool bold = false;
                public bool italic = false;

                public int characterIndex = 0;

                public bool changedColor = false;
                public bool changedBold = false;
                public bool changedItalic = false;
            }

            public float timeBetweenCharacterFadeIns = 0.1f;
            public float timePerCharacterFadeIn = 1.0f;
            public bool finishAnimatingOnClick = false;

            public Action onAdvanceAction;
            public Text textElement;

            private EventTrigger eventTrigger;

            private List<float> fadeInTimers = new List<float>();
            private float timer = 0;
            private List<StyleChangeInfo> styleChanges = new List<StyleChangeInfo>();

            private readonly Dictionary<string, Color> colorCache = new Dictionary<string, Color>()
            {
                { "black", Color.black },
                { "blue", Color.blue },
                { "clear", Color.clear },
                { "cyan", Color.cyan },
                { "gray", Color.gray },
                { "green", Color.green },
                { "grey", Color.grey },
                { "magenta", Color.magenta },
                { "red", Color.red },
                { "white", Color.white },
                { "yellow", Color.yellow },
            };

            private string insertedText;
            public string text
            {
                get
                {
                    return insertedText;
                }

                set
                {
                    insertedText = value;

                    RefreshText();
                }
            }

            public string strippedText
            {
                get
                {
                    string text = insertedText;

                    Regex tagRegex = new Regex(@"<(.*?)>(.*?)<\/(.*?)>");

                    for (;;)
                    {
                        Match match = tagRegex.Match(text);

                        if (match.Groups.Count == 1)
                        {
                            break;
                        }

                        text = text.Replace(match.Groups[0].Value, match.Groups[2].Value);
                    }

                    return text;
                }
            }

            public string processedText
            {
                get
                {
                    var stringBuilder = new StringBuilder();
                    var currentColor = textElement.color;
                    var text = strippedText;

                    for (int i = 0; i < text.Length; i++)
                    {
                        var character = text[i];
                        var time = fadeInTimers.Count > i ? fadeInTimers[i] : 0;
                        var alpha = Mathf.Clamp01((timer - time) / timePerCharacterFadeIn);
                        var bold = false;
                        var italic = false;

                        StyleChangeInfo styleChange = null;

                        foreach (var styleChangeInfo in styleChanges)
                        {
                            if (styleChangeInfo.characterIndex <= i)
                            {
                                styleChange = styleChangeInfo;

                                if (styleChange.changedBold)
                                {
                                    bold = styleChange.bold;
                                }

                                if (styleChange.changedColor)
                                {
                                    currentColor = styleChange.color;
                                }

                                if (styleChange.changedItalic)
                                {
                                    italic = styleChange.italic;
                                }
                            }
                        }

                        var adjustedColor = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);

                        stringBuilder.Append((bold ? "<b>" : "") + (italic ? "<i>" : "") + "<color=#" + ColorUtility.ToHtmlStringRGBA(adjustedColor) + ">" + character + "</color>" +
                            (italic ? "</i>" : "") + (bold ? "</b>" : ""));
                    }

                    return stringBuilder.ToString();
                }
            }

            public bool finishedAnimating
            {
                get
                {
                    float endTime = timeBetweenCharacterFadeIns * fadeInTimers.Count + timePerCharacterFadeIn;

                    return timer > endTime;
                }
            }

            public void Advance()
            {
                if (onAdvanceAction != null)
                {
                    onAdvanceAction();
                }
            }

            private void Start()
            {
                textElement = GetComponent<Text>();
                eventTrigger = GetComponent<EventTrigger>();

                if(eventTrigger == null)
                {
                    eventTrigger = gameObject.AddComponent<EventTrigger>();
                }

                EventTrigger.Entry entry = new EventTrigger.Entry()
                {
                    eventID = EventTriggerType.PointerClick,
                };

                entry.callback.AddListener((eventData) =>
                {
                    float endTime = timeBetweenCharacterFadeIns * fadeInTimers.Count + timePerCharacterFadeIn;

                    if(timer < endTime)
                    {
                        if(finishAnimatingOnClick)
                        {
                            FinishAnimating();
                        }
                    }
                    else
                    {
                        Advance();
                    }
                });

                eventTrigger.triggers.Add(entry);
            }

            private void FixedUpdate()
            {
                float endTime = timeBetweenCharacterFadeIns * fadeInTimers.Count + timePerCharacterFadeIn;

                if (timer > endTime)
                {
                    return;
                }

                textElement.text = processedText;

                timer += Time.fixedDeltaTime;
            }

            private void RefreshText()
            {
                textElement.text = strippedText;
                timer = 0;
                styleChanges = new List<StyleChangeInfo>();

                fadeInTimers = new List<float>();

                float currentTime = 0;

                foreach (char character in strippedText)
                {
                    fadeInTimers.Add(currentTime);

                    currentTime += timeBetweenCharacterFadeIns;
                }

                string text = insertedText;
                List<Color> colorStack = new List<Color>();
                int currentNormalCharacterIndex = 0;

                for (int i = 0; i < text.Length; i++)
                {
                    if (text[i] == '<' && i + 1 < text.Length)
                    {
                        if (text[i + 1] == '/')
                        {
                            if (text[i + 2] == 'b')
                            {
                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    bold = false,
                                    changedBold = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                            else if (text[i + 2] == 'i')
                            {
                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    italic = false,
                                    changedItalic = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                            else if (text.Substring(i + 2).StartsWith("color"))
                            {
                                colorStack.RemoveAt(colorStack.Count - 1);

                                Color color = textElement.color;

                                if (colorStack.Count > 0)
                                {
                                    color = colorStack.Last();
                                }

                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    color = color,
                                    changedColor = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                        }
                        else
                        {
                            if (text[i + 1] == 'b')
                            {
                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    bold = true,
                                    changedBold = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                            else if (text[i + 1] == 'i')
                            {
                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    italic = true,
                                    changedItalic = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                            else if (text.Substring(i + 1).StartsWith("color="))
                            {
                                int startIndex = i + "color=".Length + 1;
                                int endIndex = -1;
                                Color color = textElement.color;

                                for (int j = startIndex; j < text.Length; j++)
                                {
                                    if (text[j] == '>')
                                    {
                                        endIndex = j;

                                        break;
                                    }
                                }

                                string colorString = text.Substring(startIndex, endIndex - startIndex).ToLowerInvariant();

                                if (colorCache.ContainsKey(colorString))
                                {
                                    color = colorCache[colorString];
                                }
                                else if (colorString.StartsWith("#"))
                                {
                                    ColorUtility.TryParseHtmlString(colorString.Substring(1), out color);
                                }

                                colorStack.Add(color);

                                styleChanges.Add(new StyleChangeInfo()
                                {
                                    color = color,
                                    changedColor = true,
                                    characterIndex = currentNormalCharacterIndex,
                                });
                            }
                        }

                        i = text.IndexOf('>', i + 1);
                    }
                    else
                    {
                        currentNormalCharacterIndex++;
                    }
                }

                textElement.text = processedText;
            }

            public void FinishAnimating()
            {
                timer = timeBetweenCharacterFadeIns * fadeInTimers.Count + timePerCharacterFadeIn + 1;

                textElement.text = processedText;
            }
        }
    }
}
