﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    [ExecuteInEditMode]
    public class TextMeshProAutosizingInputField : MonoBehaviour
    {
        private TMP_InputField text;
        private RectTransform rectTransform;
        private readonly float extra = 10.0f;

        private void Awake()
        {
            text = GetComponent<TMP_InputField>();
            rectTransform = GetComponent<RectTransform>();
        }

#if UNITY_EDITOR
        private void Update()
        {
            Handle();
        }
#else
        private void FixedUpdate()
        {
            Handle();
        }
#endif

        private void Handle()
        {
            var size = text.textComponent.GetPreferredValues();

            if (size.y + extra != rectTransform.sizeDelta.y)
            {
                rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, size.y + extra);
            }
        }
    }
}
