﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System.Collections;

namespace FlamingTorchGames.CommonScripts
{
    public class TMProTextTyper : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        public TextMeshProUGUI text;
        public float timeBetweenCharacters = 0.025f;
        public bool allowSkipping = true;

        private bool typing = false;
        private string textString;
        private Coroutine typeCoroutine;
        private System.Action onAdvance;
        private System.Action onFinishedTyping;

        public void TypeText(string text, System.Action onFinishedTyping, System.Action onAdvance)
        {
            if(typing)
            {
                StopCoroutine(typeCoroutine);
            }

            textString = text;
            this.onAdvance = onAdvance;
            this.onFinishedTyping = onFinishedTyping;

            typing = true;
            typeCoroutine = StartCoroutine(TypeCoroutine());
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(typing)
            {
                if(allowSkipping)
                {
                    typing = false;

                    StopCoroutine(typeCoroutine);

                    text.maxVisibleCharacters = text.textInfo.characterCount;

                    var finishAction = onFinishedTyping;
                    onFinishedTyping = null;

                    finishAction?.Invoke();
                }
            }
            else
            {
                var finishAction = onAdvance;
                onAdvance = null;

                finishAction?.Invoke();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        public void OnPointerUp(PointerEventData eventData)
        {
        }

        private IEnumerator TypeCoroutine()
        {
            text.maxVisibleCharacters = 0;
            text.text = textString;

            for(; ; )
            {
                yield return new WaitForSeconds(timeBetweenCharacters);

                text.maxVisibleCharacters++;

                if(text.textInfo.characterCount < text.maxVisibleCharacters)
                {
                    break;
                }
            }

            yield return new WaitForSeconds(timeBetweenCharacters);

            typing = false;

            var finishAction = onFinishedTyping;
            onFinishedTyping = null;

            finishAction?.Invoke();
        }
    }
}
