﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class FPSCounter : MonoBehaviour
        {
            public string textFormat = "{0} FPS";
            private Text text;
            private int frameCounter = 0;
            private float timer = 0;

            private void Start()
            {
                text = GetComponent<Text>();
            }

            private void Update()
            {
                if (timer >= 1)
                {
                    text.text = string.Format(textFormat, frameCounter);

                    frameCounter = 0;
                    timer = 0;
                }

                frameCounter++;
                timer += Time.deltaTime;
            }
        }
    }
}
