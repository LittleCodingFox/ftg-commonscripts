﻿using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Translated Text behaviour for Text Mesh Pro UGUI Elements
        /// </summary>
        public class TranslatedTextTMProUGUI : MonoBehaviour
        {
            private TMPro.TextMeshProUGUI textInstance;
            public string stringToTranslate;
            public bool useGlobalTranslationCollection = true;

            [System.NonSerialized]
            public TranslationCollection translationCollection;

            private void Start()
            {
                textInstance = GetComponent<TMPro.TextMeshProUGUI>();

                if(useGlobalTranslationCollection)
                {
                    translationCollection = TranslationUtils.GlobalTranslations;
                }

                RefreshTranslation();
            }

            /// <summary>
            /// Updates the current translation of this TranslatedText
            /// </summary>
            public void RefreshTranslation()
            {
                if(translationCollection != null && textInstance != null)
                {
                    textInstance.text = translationCollection.GetString(stringToTranslate);
                }
            }
        }
    }
}
