﻿using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Translated Text behaviour for Text Mesh Pro objects
        /// </summary>
        public class TranslatedTextTMPro : MonoBehaviour
        {
            private TMPro.TextMeshPro textInstance;
            public string stringToTranslate;
            public bool useGlobalTranslationCollection = true;

            [System.NonSerialized]
            public TranslationCollection translationCollection;

            private void Start()
            {
                textInstance = GetComponent<TMPro.TextMeshPro>();

                if (useGlobalTranslationCollection)
                {
                    translationCollection = TranslationUtils.GlobalTranslations;
                }

                RefreshTranslation();
            }

            /// <summary>
            /// Updates the current translation of this TranslatedText
            /// </summary>
            public void RefreshTranslation()
            {
                if (translationCollection != null && textInstance != null)
                {
                    textInstance.text = translationCollection.GetString(stringToTranslate);
                }
            }
        }
    }
}
