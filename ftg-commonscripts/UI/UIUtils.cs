﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
#endif
using System.Collections;
using UnityEngine.Events;
using System.Linq;
using System.Collections.Generic;

namespace FlamingTorchGames.CommonScripts
{
    public static class UIUtils
    {
        /// <summary>
        /// Gets the expected UI Scale for a screen size
        /// </summary>
        /// <returns>The expected UI Scale</returns>
        /// <param name="screenSize">The current screen size</param>
        /// <param name="referenceSize">The base screen size</param>
        /// <param name="matchWidthOrHeight">How to match the width and height</param>
        /// <remarks>matchWidthOrHeight should be 0 = width, 1 = height, 0.5 = both, any other value: something in-between</remarks>
        public static float UIScale(Vector2 screenSize, Vector2 referenceSize, float matchWidthOrHeight)
        {
            const float kLogBase = 2;

            float logWidth = Mathf.Log(screenSize.x / referenceSize.x, kLogBase);
            float logHeight = Mathf.Log(screenSize.y / referenceSize.y, kLogBase);
            float logWeightedAverage = Mathf.Lerp(logWidth, logHeight, matchWidthOrHeight);

            return Mathf.Pow(kLogBase, logWeightedAverage);
        }

        /// <summary>
        /// Measures the size that a Text component needs
        /// </summary>
        /// <param name="textToMeasure">The Text to measure</param>
        /// <returns>The required size (width and height)</returns>
        public static Vector2 MeasureText(Text textToMeasure)
        {
            Canvas.ForceUpdateCanvases();

            return new Vector2(LayoutUtility.GetPreferredWidth(textToMeasure.rectTransform), LayoutUtility.GetPreferredHeight(textToMeasure.rectTransform));
        }

        /// <summary>
        /// Quicker way to add an event to an event trigger that also makes sure if it's a click event it will only click when it's the hovered object
        /// </summary>
        /// <param name="eventTrigger">The Event Trigger to add the event to</param>
        /// <param name="type">The event type</param>
        /// <param name="sourceObject">The Source GameObject that contains the event trigger</param>
        /// <param name="onTriggerAction">The action to perform when triggered</param>
        public static void AddEventToEventTrigger(EventTrigger eventTrigger, EventTriggerType type, GameObject sourceObject, UnityAction<BaseEventData> onTriggerAction)
        {
            EventTrigger.Entry entry = null;

            foreach (EventTrigger.Entry triggerEntry in eventTrigger.triggers)
            {
                if (triggerEntry.eventID == type)
                {
                    entry = triggerEntry;

                    break;
                }
            }

            if (entry == null)
            {
                entry = new EventTrigger.Entry();
            }

            entry.eventID = type;
            entry.callback.AddListener((EventData) =>
            {
                if (type != EventTriggerType.PointerClick || (EventSystem.current.currentSelectedGameObject == null || EventSystem.current.currentSelectedGameObject == sourceObject))
                {
                    onTriggerAction(EventData);
                }
            });

            eventTrigger.triggers.Add(entry);
        }

        /// <summary>
        /// Gets the current pointer over object
        /// </summary>
        /// <returns>The object under the pointer, or null</returns>
        public static GameObject PointerOverObject()
        {
            var pointerData = new PointerEventData(EventSystem.current);

            if (Application.isMobilePlatform)
            {
#if ENABLE_INPUT_SYSTEM
                pointerData.position = UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.FirstOrDefault(x => x.finger.index == 0).screenPosition;
#else
                pointerData.position = Input.touches.FirstOrDefault(x => x.fingerId == 0).position;
#endif
            }
            else
            {
#if ENABLE_INPUT_SYSTEM
                pointerData.position = Mouse.current.position.ReadValue();
#else
                pointerData.position = Input.mousePosition;
#endif
            }

            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            return results.Count > 0 ? results.OrderBy(x => x.distance).First().gameObject : null;
        }
    }
}
