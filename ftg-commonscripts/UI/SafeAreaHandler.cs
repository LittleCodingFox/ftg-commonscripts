﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SafeAreaHandler : MonoBehaviour
    {
        public enum SimulatorDevice
        {
            None,
            iPhoneX
        }

        public static SimulatorDevice simulatedDevice = SimulatorDevice.None;

        private readonly Rect[] iPhoneXSafeArea = {

            new Rect (0f, 102f / 2436f, 1f, 2202f / 2436f),  // Portrait
            new Rect (132f / 2436f, 63f / 1125f, 2172f / 2436f, 1062f / 1125f)  // Landscape
        };

        private RectTransform panel;
        private Rect lastSafeArea = new Rect(0, 0, 0, 0);

        private void Awake()
        {
            panel = GetComponent<RectTransform>();
            Refresh();
        }

        private void FixedUpdate()
        {
            Refresh();
        }

        private void Refresh()
        {
            Rect safeArea = GetSafeArea();

            if (safeArea != lastSafeArea)
                ApplySafeArea(safeArea);
        }

        private Rect GetSafeArea()
        {
#if UNITY_EDITOR
            var safeArea = Screen.safeArea;

            if (Application.isEditor && simulatedDevice != SimulatorDevice.None)
            {
                var newSafeArea = new Rect(0, 0, Screen.width, Screen.height);

                switch(simulatedDevice)
                {
                    case SimulatorDevice.iPhoneX:

                        newSafeArea = iPhoneXSafeArea[Screen.height > Screen.width ? 0 : 1];

                        break;
                }

                safeArea = new Rect(Screen.width * newSafeArea.x, Screen.height * newSafeArea.y, Screen.width * newSafeArea.width, Screen.height * newSafeArea.height);
            }

            return safeArea;
#else
            return Screen.safeArea;
#endif
        }

        private void ApplySafeArea(Rect r)
        {
            lastSafeArea = r;

            // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
            Vector2 anchorMin = r.position;
            Vector2 anchorMax = r.position + r.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            panel.anchorMin = anchorMin;
            panel.anchorMax = anchorMax;

#if UNITY_EDITOR
            Debug.LogFormat("New safe area applied to {0}: x={1}, y={2}, w={3}, h={4} on full extents w={5}, h={6}",
                name, r.x, r.y, r.width, r.height, Screen.width, Screen.height);
#endif
        }
    }
}
