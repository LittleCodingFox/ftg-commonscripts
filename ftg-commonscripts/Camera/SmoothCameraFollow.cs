﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SmoothCameraFollow : MonoBehaviour
    {
        public GameObject target;
        public Vector3 offset;
        public float smoothSpeed = 30.0f;
        public bool followEnabled = true;
        public bool preserveZ = false;

        public void Reset()
        {
            if(target == null)
            {
                return;
            }

            var targetPosition = target.transform.position + offset;

            if (preserveZ)
            {
                targetPosition.z = transform.position.z;
            }

            transform.position = targetPosition;
        }

        private void LateUpdate()
        {
            if(target == null)
            {
                return;
            }

            if(followEnabled)
            {
                var targetPosition = target.transform.position + offset;

                if(preserveZ)
                {
                    targetPosition.z = transform.position.z;
                }

                var smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothSpeed * Time.deltaTime);

                transform.position = smoothPosition;
            }
        }
    }
}
