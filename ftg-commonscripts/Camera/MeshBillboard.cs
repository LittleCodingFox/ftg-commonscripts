﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class MeshBillboard : MonoBehaviour
    {
        private Camera mainCamera;
        public bool faceY = false;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        private void Update()
        {
            if(mainCamera != null && Application.isPlaying)
            {
                var y = faceY ? mainCamera.transform.forward.y : 0;
                var viewDirection = new Vector3(mainCamera.transform.forward.x, y, mainCamera.transform.forward.z);
                transform.LookAt(transform.position + viewDirection);
            }
        }
    }
}
