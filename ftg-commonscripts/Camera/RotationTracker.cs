﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class RotationTracker : MonoBehaviour
    {
        public Transform target;
        public bool onlyY = false;

        private void LateUpdate()
        {
            if(onlyY)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, target.rotation.eulerAngles.y, 0));
            }
            else
            {
                transform.rotation = target.rotation;
            }
        }
    }
}
