﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SmoothMouseOrbit : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset;
        public float distance = 5.0f;
        public float minDistance = 1;
        public float sensitivity = 2;
        public float speed = 10.0f;
        public float wheelSpeed = 50.0f;
        public Vector2 yLimits = new Vector2(-20, 80);
        public bool clampYAxis = false;
        public bool invertY = true;
        public bool invertX = false;
        public bool lockCursor = true;
        public bool mouseLookEnabled = true;
        public bool enableMouseLookOnRightClick = false;
        public bool handleCameraCollisions = false;

        public Vector2 distanceLimits = new Vector2(0.5f, 15.0f);

        private float x = 0.0f;
        private float y = 0.0f;

        private void Awake()
        {
            Vector3 angles = transform.eulerAngles;
            x = angles.y;
            y = angles.x;
        }

        private void LateUpdate()
        {
            if (enableMouseLookOnRightClick)
            {
                mouseLookEnabled = Input.GetMouseButton(1);
            }

            if (!mouseLookEnabled)
            {
                Cursor.lockState = CursorLockMode.None;
            }

            if (lockCursor && mouseLookEnabled)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            if (target)
            {
                var xAxis = Input.GetAxisRaw("Mouse X");
                var yAxis = Input.GetAxisRaw("Mouse Y");
                var wheelAxis = Input.GetAxis("Mouse ScrollWheel");

                if (mouseLookEnabled)
                {
                    x += xAxis * sensitivity * distance * speed * Time.deltaTime * (invertX ? -1 : 1);
                    y += yAxis * sensitivity * distance * speed * Time.deltaTime * (invertY ? -1 : 1);
                }

                if (y < -360.0f)
                {
                    y += 360.0f;
                }

                if (y > 360.0f)
                {
                    y -= 360.0f;
                }

                if (clampYAxis)
                {
                    y = Mathf.Clamp(y, yLimits.x, yLimits.y);
                }

                var rotation = Quaternion.Euler(y, x, 0);

                distance = Mathf.Clamp(distance - wheelAxis * sensitivity * wheelSpeed * Time.deltaTime, distanceLimits.x, distanceLimits.y);

                if (distance < minDistance)
                {
                    distance = minDistance;
                }

                if (handleCameraCollisions)
                {
                    RaycastHit hit;

                    if (Physics.Linecast(target.position, transform.position, out hit))
                    {
                        distance -= hit.distance;
                    }
                }

                var negativeDistance = new Vector3(0.0f, 0.0f, -distance);
                var position = rotation * negativeDistance + target.position + offset;

                transform.rotation = rotation;
                transform.position = position;
            }
        }
    }
}
