﻿using UnityEngine;
using System.Collections;

namespace FlamingTorchGames.CommonScripts
{
    public class CameraShake : MonoBehaviour
    {
        public float shakeAmount = 0.7f;
        public float decreaseFactor = 1.0f;

        public void Shake(float time)
        {
            StartCoroutine(ShakeCoroutine(time));
        }

        private IEnumerator ShakeCoroutine(float time)
        {
            var originalPosition = transform.localPosition;

            for(; ; )
            {
                if (time > 0)
                {
                    transform.localPosition = originalPosition + Random.insideUnitSphere * shakeAmount;

                    time -= Time.deltaTime * decreaseFactor;
                }
                else
                {
                    transform.localPosition = originalPosition;

                    yield break;
                }

                yield return null;
            }
        }
    }
}