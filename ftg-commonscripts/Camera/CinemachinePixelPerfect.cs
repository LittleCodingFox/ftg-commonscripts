﻿#if USING_CINEMACHINE
using UnityEngine;
using Cinemachine;

public class CinemachinePixelPerfect : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    private CinemachineVirtualCamera virtualCameraScript;

    // Use this for initialization
    void Start()
    {
        virtualCameraScript = GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        virtualCameraScript.m_Lens.OrthographicSize = mainCamera.orthographicSize;
    }

}
#endif
