using UnityEngine;

#if USING_PROCAMERA2D
using Com.LuisPedroFonseca.ProCamera2D;

namespace FlamingTorchGames.CommonScripts
{
	public class ProCamera2DCameraGrid : BasePC2D, IPositionOverrider
	{
		public static string ExtensionName = "Camera Grid (FTG)";

		public float PixelsPerUnit = 32;

        override protected void Awake()
		{
			base.Awake();

			if (!ProCamera2D.GameCamera.orthographic)
			{
				enabled = false;
				return;
			}

			ProCamera2D.AddPositionOverrider(this);
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			ProCamera2D.RemovePositionOverrider(this);
		}

        #region IPositionOverrider implementation

		public Vector3 OverridePosition(float deltaTime, Vector3 originalPosition)
		{
			if (!enabled)
				return originalPosition;

			// We do this so we can have the camera movement not snapping to the grid, while the sprites are
			var cameraPixelStep = 1f / PixelsPerUnit;

			return VectorHVD(Utils.AlignToGrid(Vector3H(originalPosition), cameraPixelStep), Utils.AlignToGrid(Vector3V(originalPosition), cameraPixelStep), 0);
		}

        public int POOrder { get { return _poOrder; } set { _poOrder = value; } }
        int _poOrder = 2000;

        #endregion
    }
}

#endif
