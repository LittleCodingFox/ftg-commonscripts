﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SmoothMouseLook : MonoBehaviour
    {
        public bool lockCursor = true;
        public bool mouseLookEnabled = true;
        public bool enableMouseLookOnRightClick = false;
        public Vector2 sensitivity = new Vector2(2, 2);
        public Vector2 smoothing = new Vector2(3, 3);
        public Vector2 clampInDegrees = new Vector2(360, 180);
        private Vector2 targetDirection;
        private Vector2 smoothMouse;
        private Vector2 mouseAbsolute;

        private void Start()
        {
            targetDirection = transform.localRotation.eulerAngles;
        }

        private void Update()
        {
            if(enableMouseLookOnRightClick)
            {
                mouseLookEnabled = Input.GetMouseButton(1);
            }

            if (!mouseLookEnabled)
            {
                Cursor.lockState = CursorLockMode.None;

                return;
            }

            if(lockCursor)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            var targetOrietation = Quaternion.Euler(targetDirection);
            var mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity.x * smoothing.x, sensitivity.y * smoothing.y));

            smoothMouse.x = Mathf.Lerp(smoothMouse.x, mouseDelta.x, 1.0f / smoothing.x);
            smoothMouse.y = Mathf.Lerp(smoothMouse.y, mouseDelta.y, 1.0f / smoothing.y);

            mouseAbsolute += smoothMouse;

            if(clampInDegrees.x < 360)
            {
                mouseAbsolute.x = Mathf.Clamp(mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);
            }

            if (clampInDegrees.y < 360)
            {
                mouseAbsolute.y = Mathf.Clamp(mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);
            }

            transform.localRotation = Quaternion.AngleAxis(-mouseAbsolute.y, targetOrietation * Vector3.right) * targetOrietation;

            var yRotation = Quaternion.AngleAxis(mouseAbsolute.x, transform.InverseTransformDirection(Vector3.up));
            transform.localRotation *= yRotation;
        }
    }
}
