﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class CameraFollow : MonoBehaviour
    {
        public GameObject target;
        public bool preserveZ = false;

        private void LateUpdate()
        {
            if(target != null)
            {
                var position = target.transform.position;

                if(preserveZ)
                {
                    position.z = transform.position.z;
                }

                transform.position = position;
            }
        }
    }
}
