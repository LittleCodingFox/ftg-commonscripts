﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Entities
{
    public class NetworkMovementInterpolator : MonoBehaviour
    {
        public NetworkMovementUpdateData currentUpdateData;
        public NetworkMovementUpdateData previousUpdateData;

        private float lastInputTime;

        private void Update()
        {
            var timeSinceLastInput = Time.time - lastInputTime;
            var t = timeSinceLastInput / Time.fixedDeltaTime;
            transform.position = Vector3.LerpUnclamped(previousUpdateData.position, currentUpdateData.position, t);
            transform.rotation = Quaternion.SlerpUnclamped(previousUpdateData.lookDirection, currentUpdateData.lookDirection, t);
        }

        public void SetUpdateDataFramePosition(NetworkMovementUpdateData data)
        {
            RefreshUpdateDataToPosition(data, currentUpdateData);
        }

        public void RefreshUpdateDataToPosition(NetworkMovementUpdateData data, NetworkMovementUpdateData previous)
        {
            previousUpdateData = previous;
            currentUpdateData = data;
            lastInputTime = Time.fixedTime;
        }
    }
}
