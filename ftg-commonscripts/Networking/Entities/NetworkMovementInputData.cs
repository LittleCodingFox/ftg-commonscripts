﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Entities
{
    public class NetworkMovementInputData : INetworkMessage
    {
        public bool[] inputs;
        public Quaternion lookDirection;
        public uint time;

        public void Deserialize(INetworkReader reader)
        {
            var inputCount = reader.ReadByte();

            if(inputCount > 32)
            {
                throw new System.Exception($"[{GetType().Name}] Excessive input size read: {inputCount}. Should limit to 32 inputs");
            }

            inputs = new bool[inputCount];

            for(var i = 0; i < inputCount; i++)
            {
                inputs[i] = reader.ReadBool();
            }

            lookDirection = reader.ReadQuaternionCompressed();

            time = reader.ReadVarUInt32();
        }

        public void Serialize(INetworkWriter writer)
        {
            if (inputs.Length > 32)
            {
                throw new System.Exception($"[{GetType().Name}] Excessive input size sent: {inputs.Length}. Should limit to 32 inputs.");
            }

            writer.WriteByte((byte)inputs.Length);

            for(var i = 0; i < inputs.Length; i++)
            {
                writer.WriteBool(inputs[i]);
            }

            writer.WriteQuaternionCompressed(lookDirection);
            writer.WriteVarUInt32(time);
        }
    }
}
