﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Entities
{
    public class NetworkEntitySpawnData : INetworkMessage
    {
        public uint entityID;
        public Vector3 position;

        public void Deserialize(INetworkReader reader)
        {
            entityID = reader.ReadVarUInt32();
            position = reader.ReadVector3();
        }

        public void Serialize(INetworkWriter writer)
        {
            writer.WriteVarUInt32(entityID);
            writer.WriteVector3(position);
        }
    }

    public class NetworkEntityDespawnData : INetworkMessage
    {
        public uint entityID;

        public void Deserialize(INetworkReader reader)
        {
            entityID = reader.ReadVarUInt32();
        }

        public void Serialize(INetworkWriter writer)
        {
            writer.WriteVarUInt32(entityID);
        }
    }

    public class NetworkEntityUpdateData : INetworkMessage
    {
        public uint frame;
        public NetworkEntitySpawnData[] spawns;
        public NetworkEntityDespawnData[] despawns;
        public NetworkMovementUpdateData[] movementData;

        public void Deserialize(INetworkReader reader)
        {
            frame = reader.ReadVarUInt32();

            var count = reader.ReadByte();

            spawns = new NetworkEntitySpawnData[count];

            for(var i = 0; i < count; i++)
            {
                spawns[i] = new NetworkEntitySpawnData();
                spawns[i].Deserialize(reader);
            }

            count = reader.ReadByte();

            despawns = new NetworkEntityDespawnData[count];

            for (var i = 0; i < count; i++)
            {
                despawns[i] = new NetworkEntityDespawnData();
                despawns[i].Deserialize(reader);
            }

            count = reader.ReadByte();

            movementData = new NetworkMovementUpdateData[count];

            for (var i = 0; i < count; i++)
            {
                movementData[i] = new NetworkMovementUpdateData();
                movementData[i].Deserialize(reader);
            }
        }

        public void Serialize(INetworkWriter writer)
        {
            writer.WriteVarUInt32(frame);

            writer.WriteByte((byte)spawns.Length);

            foreach(var spawn in spawns)
            {
                spawn.Serialize(writer);
            }

            writer.WriteByte((byte)despawns.Length);

            foreach (var despawn in despawns)
            {
                despawn.Serialize(writer);
            }

            writer.WriteByte((byte)movementData.Length);

            foreach (var movementData in movementData)
            {
                movementData.Serialize(writer);
            }
        }
    }

    public class NetworkEntityStartData : INetworkMessage
    {
        public uint serverTick;
        public NetworkEntitySpawnData[] entities;

        public void Deserialize(INetworkReader reader)
        {
            serverTick = reader.ReadVarUInt32();

            var count = reader.ReadByte();

            entities = new NetworkEntitySpawnData[count];

            for(var i = 0; i < count; i++)
            {
                entities[i] = new NetworkEntitySpawnData();
                entities[i].Deserialize(reader);
            }
        }

        public void Serialize(INetworkWriter writer)
        {
            writer.WriteVarUInt32(serverTick);

            writer.WriteByte((byte)entities.Length);

            foreach(var entity in entities)
            {
                entity.Serialize(writer);
            }
        }
    }
}
