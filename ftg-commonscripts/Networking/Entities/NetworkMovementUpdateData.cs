﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Entities
{
    public class NetworkMovementUpdateData : INetworkMessage
    {
        public uint entityID;
        public Vector3 position;
        public float gravity;
        public Quaternion lookDirection;

        public void Deserialize(INetworkReader reader)
        {
            entityID = reader.ReadVarUInt32();
            position = reader.ReadVector3();
            gravity = reader.ReadFloat();
            lookDirection = reader.ReadQuaternionCompressed();
        }

        public void Serialize(INetworkWriter writer)
        {
            writer.WriteVarUInt32(entityID);
            writer.WriteVector3(position);
            writer.WriteFloat(gravity);
            writer.WriteQuaternionCompressed(lookDirection);
        }
    }
}
