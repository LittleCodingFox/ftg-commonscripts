﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking.Entities
{
    [RequireComponent(typeof(NetworkMovementInterpolator))]
    public class NetworkEntity : MonoBehaviour
    {
        public uint entityID;
        public bool isPlayer = false;

        public NetworkMovementInterpolator interpolation;

        protected byte forwardMovementInputIndex = 0;
        protected byte backwardMovementInputIndex = 1;
        protected byte leftMovementInputIndex = 2;
        protected byte rightMovementInputIndex = 3;
        protected byte jumpMovementInputIndex = 4;

        protected virtual void Awake()
        {
            interpolation = GetComponent<NetworkMovementInterpolator>();
        }

        protected virtual void FixedUpdate()
        {
            if(isPlayer)
            {
                var inputData = new NetworkMovementInputData()
                {
                    inputs = CollectInputs(),
                    lookDirection = transform.localRotation,
                    time = 0/*here we later write the last recieved tick*/
                };

                transform.position = interpolation.currentUpdateData.position;

                var updateData = GetNextFrameData(inputData, interpolation.currentUpdateData);

                interpolation.SetUpdateDataFramePosition(updateData);
            }
        }

        public virtual void Initialize(uint entityID)
        {
            interpolation.currentUpdateData = new NetworkMovementUpdateData()
            {
                entityID = entityID,
                gravity = 0,
                lookDirection = Quaternion.identity,
                position = Vector3.zero,
            };
        }

        public virtual void OnServerDataUpdate(NetworkMovementUpdateData data)
        {
            if(isPlayer)
            {

            }
            else
            {
                interpolation.SetUpdateDataFramePosition(data);
            }
        }

        protected virtual bool[] CollectInputs()
        {
            var result = new bool[Mathf.Max(forwardMovementInputIndex, backwardMovementInputIndex, leftMovementInputIndex, rightMovementInputIndex) + 1];

            result[forwardMovementInputIndex] = Input.GetKey(KeyCode.W);
            result[backwardMovementInputIndex] = Input.GetKey(KeyCode.S);
            result[leftMovementInputIndex] = Input.GetKey(KeyCode.A);
            result[rightMovementInputIndex] = Input.GetKey(KeyCode.D);
            result[jumpMovementInputIndex] = Input.GetKey(KeyCode.Space);

            return result;
        }

        public virtual void Move(Vector3 movement)
        {
        }

        public virtual void ApplyMovementSpeed(ref Vector3 movement)
        {
        }

        public virtual void ApplyGravity(ref Vector3 gravity, bool jumpPressed)
        {
            gravity.y -= GravityConstant();
        }

        public virtual float GravityConstant()
        {
            return -9.81f;
        }

        public virtual NetworkMovementUpdateData GetNextFrameData(NetworkMovementInputData input, NetworkMovementUpdateData current)
        {
            var forward = input.inputs[forwardMovementInputIndex];
            var backward = input.inputs[backwardMovementInputIndex];
            var left = input.inputs[leftMovementInputIndex];
            var right = input.inputs[rightMovementInputIndex];
            var jump = input.inputs[jumpMovementInputIndex];

            var rotation = input.lookDirection.eulerAngles;

            var gravity = new Vector3(0, current.gravity, 0);

            var movement = Vector3.zero;

            if(forward)
            {
                movement += Vector3.forward;
            }

            if(backward)
            {
                movement += Vector3.back;
            }

            if(left)
            {
                movement += Vector3.left;
            }

            if(right)
            {
                movement += Vector3.right;
            }

            movement = (Quaternion.Euler(0, rotation.y, 0) * movement).normalized;

            ApplyMovementSpeed(ref movement);

            movement *= Time.fixedDeltaTime;
            movement += gravity * Time.fixedDeltaTime;

            ApplyGravity(ref gravity, jump);

            Move(movement);

            return new NetworkMovementUpdateData()
            {
                entityID = current.entityID,
                gravity = gravity.y,
                lookDirection = input.lookDirection,
                position = transform.localPosition
            };
        }
    }
}
