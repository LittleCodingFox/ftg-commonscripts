﻿#if USING_UNET
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class UNetNetworkReader : INetworkReader
    {
        private NetworkReader networkReader;

        public int Length
        {
            get
            {
                return networkReader?.Length ?? 0;
            }
        }

        public int Position
        {
            get
            {
                return (int)(networkReader?.Position ?? 0);
            }
        }

        public UNetNetworkReader(NetworkReader reader)
        {
            networkReader = reader;
        }

        public bool ReadBool()
        {
            return networkReader.ReadBoolean();
        }

        public byte ReadByte()
        {
            return networkReader.ReadByte();
        }

        public byte[] ReadBytes()
        {
            return networkReader.ReadBytesAndSize();
        }

        public byte[] ReadBytesAndSize(int length)
        {
            return networkReader.ReadBytes(length);
        }

        public char ReadChar()
        {
            return networkReader.ReadChar();
        }

        public double ReadDouble()
        {
            return networkReader.ReadDouble();
        }

        public float ReadFloat()
        {
            return networkReader.ReadSingle();
        }

        public short ReadInt16()
        {
            return networkReader.ReadInt16();
        }

        public int ReadInt32()
        {
            return networkReader.ReadInt32();
        }

        public long ReadInt64()
        {
            return networkReader.ReadInt64();
        }

        public sbyte ReadSByte()
        {
            return networkReader.ReadSByte();
        }

        public string ReadString()
        {
            return networkReader.ReadString();
        }

        public ushort ReadUInt16()
        {
            return networkReader.ReadUInt16();
        }

        public uint ReadUInt32()
        {
            return networkReader.ReadUInt32();
        }

        public ulong ReadUInt64()
        {
            return networkReader.ReadUInt64();
        }
    }
}
#endif
