﻿#if USING_UNET
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class UNetGameClient : IGameClient
    {
        public GameClientState state
        {
            get;
            private set;
        } = GameClientState.None;

        public long playerID
        {
            get;
            private set;
        } = 0;

        /// <summary>
        /// Low level network client for communicating with the server
        /// </summary>
        private NetworkClient client;

        public event GameClientConnectionDelegate OnConnected;
        public event GameClientConnectionDelegate OnDisconnected;
        public event GameClientMessageDelegate OnMessageReceived;

        public void Connect(string host, int port)
        {
            Create();

            var Addresses = Dns.GetHostAddresses(host);

            if (Addresses.Length != 0)
            {
                state = GameClientState.Connecting;
                client.Connect(Addresses[0].ToString(), port);
            }
            else
            {
                Disconnect();
                state = GameClientState.Error;
            }
        }

        public void Create()
        {
            client = new NetworkClient();

            for (var i = 0; i < 65545; i++)
            {
                client.RegisterHandler((short)i, OnMessage);
            }
        }

        public void Disconnect()
        {
            if (!client.isConnected)
            {
                return;
            }

            state = GameClientState.Disconnected;

            client.Disconnect();
        }

        public void SendMessage(short messageID, INetworkMessage message)
        {
        }

        public void Update()
        {
        }

        private void OnMessage(NetworkMessage message)
        {
            if(message.msgType < 100)
            {
                if(message.msgType == MsgType.Connect)
                {
                    state = GameClientState.Active;
                    OnConnected();
                }
                else if(message.msgType == MsgType.Disconnect)
                {
                    if (state == GameClientState.Disconnected)
                    {
                        return;
                    }

                    state = GameClientState.Disconnected;
                    OnDisconnected();
                }
            }
            else
            {
                var nativeMessage = message.ReadMessage<GenericUNetNetworkMessage>();

                OnMessageReceived(message.msgType, new UNetNetworkReader(new NetworkReader(nativeMessage.data)));
            }
        }
    }
}
#endif
