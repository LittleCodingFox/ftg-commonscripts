﻿#if USING_UNET
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class UNetNetworkWriter : INetworkWriter
    {
        private NetworkWriter networkWriter;

        public int Length
        {
            get
            {
                return networkWriter?.AsArray().Length ?? 0;
            }
        }

        public int Position
        {
            get
            {
                return (int)(networkWriter?.Position ?? 0);
            }
        }

        public UNetNetworkWriter(NetworkWriter writer)
        {
            networkWriter = writer;
        }

        public void WriteBool(bool value)
        {
            networkWriter.Write(value);
        }

        public void WriteByte(byte value)
        {
            networkWriter.Write(value);
        }

        public void WriteBytes(byte[] value)
        {
            networkWriter.WriteBytesFull(value);
        }

        public void WriteBytesAndSize(byte[] value, int length)
        {
            networkWriter.WriteBytesAndSize(value, length);
        }

        public void WriteChar(char value)
        {
            networkWriter.Write(value);
        }

        public void WriteDouble(double value)
        {
            networkWriter.Write(value);
        }

        public void WriteFloat(float value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt16(short value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt32(int value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt64(long value)
        {
            networkWriter.Write(value);
        }

        public void WriteSByte(sbyte value)
        {
            networkWriter.Write(value);
        }

        public void WriteString(string value)
        {
            networkWriter.Write(value);
        }

        public void WriteUInt16(ushort value)
        {
            networkWriter.Write(value);
        }

        public void WriteUInt32(uint value)
        {
            networkWriter.Write(value);
        }

        public void WriteUInt64(ulong value)
        {
            networkWriter.Write(value);
        }

        public byte[] ToArray()
        {
            return networkWriter.AsArray();
        }
    }
}
#endif
