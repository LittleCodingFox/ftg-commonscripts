﻿#if USING_UNET
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.Networking
{
    class GenericUNetNetworkMessage : MessageBase
    {
        public byte[] data;
    }

    class UNetGameServer : IGameServer
    {
        /// <summary>
        /// Dictionary of connection ID to Player ID
        /// </summary>
        private Dictionary<int, long> playerIDs = new Dictionary<int, long>();

        /// <summary>
        /// Dictionary of Player ID to connection ID
        /// </summary>
        private Dictionary<long, int> reversePlayerIDs = new Dictionary<long, int>();

        /// <summary>
        /// Internal counter for player IDs
        /// </summary>
        private long playerCounter = 0;

        /// <summary>
        /// Network instance for the server
        /// </summary>
        private NetworkServerSimple server;

        public event GameServerClientConnectiondDelegate OnClientConnected;
        public event GameServerClientConnectiondDelegate OnClientDisconnected;
        public event GameServerClientMessageReceivedDelegate OnClientMessageReceived;

        public void Create(int port, int maxConnections)
        {
            server = new NetworkServerSimple();

            server.Configure(new ConnectionConfig(), maxConnections);

            server.Listen(port);

            for(var i = 0; i < 65545; i++)
            {
                server.RegisterHandler((short)i, OnMessage);
            }
        }

        public void DisconnectPlayer(long playerID)
        {
            if (reversePlayerIDs.ContainsKey(playerID))
            {
                var connectionID = reversePlayerIDs[playerID];

                server.Disconnect(connectionID);
            }
        }

        public void SendMessageToPlayer(long playerID, short messageID, INetworkMessage message)
        {
            var connection = ConnectionForPlayer(playerID);
            var networkWriter = new NetworkWriter();
            var abstractNetworkWriter = new UNetNetworkWriter(networkWriter);

            message.Serialize(abstractNetworkWriter);

            connection.Send(messageID, new GenericUNetNetworkMessage()
            {
                data = networkWriter.AsArray(),
            });
        }

        public void Shutdown()
        {
            server.DisconnectAllConnections();
            server.Stop();
        }

        public void Update()
        {
            server.Update();
            server.UpdateConnections();
        }

        private NetworkConnection ConnectionForPlayer(long playerID)
        {
            if (reversePlayerIDs.ContainsKey(playerID))
            {
                return server.connections[reversePlayerIDs[playerID]];
            }

            return null;
        }

        private void OnMessage(NetworkMessage message)
        {
            if (message.msgType < 100)
            {
                if (message.msgType == MsgType.Connect)
                {
                    var playerID = ++playerCounter;
                    playerIDs[message.conn.connectionId] = playerID;
                    reversePlayerIDs[playerCounter] = message.conn.connectionId;

                    OnClientConnected(playerID);
                }
                else if (message.msgType == MsgType.Disconnect)
                {
                    if (playerIDs.ContainsKey(message.conn.connectionId))
                    {
                        var playerID = playerIDs[message.conn.connectionId];

                        OnClientDisconnected(playerID);
                    }
                }
            }
            else
            {
                if (playerIDs.ContainsKey(message.conn.connectionId))
                {
                    var playerID = playerIDs[message.conn.connectionId];
                    var nativeMessage = message.ReadMessage<GenericUNetNetworkMessage>();

                    OnClientMessageReceived(playerID, message.msgType, new UNetNetworkReader(new NetworkReader(nativeMessage.data)));
                }
            }
        }
    }
}
#endif
