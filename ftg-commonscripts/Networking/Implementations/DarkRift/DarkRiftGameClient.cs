﻿#if USING_DARKRIFT
using DarkRift;
using DarkRift.Client;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class DarkRiftGameClient : IGameClient
    {
        private class PacketInfo
        {
            public short packetIndex;
            public ushort tag;
            public DarkRiftNetworkReader reader;
        }

        public GameClientState state
        {
            get;
            protected set;
        } = GameClientState.None;

        public long playerID
        {
            get;
            protected set;
        } = 0;

        private DarkRiftClient client;
        private DarkRift.Dispatching.Dispatcher dispatcher = new DarkRift.Dispatching.Dispatcher(true);
        private short sentCounter = 0;
        private short receivedCounter = 0;
        private List<PacketInfo> receivedPackets = new List<PacketInfo>();

        public event GameClientConnectionDelegate OnConnected;
        public event GameClientConnectionDelegate OnDisconnected;
        public event GameClientMessageDelegate OnMessageReceived;

        public bool debuggingNetworkMessages = false;

        public void Connect(string host, int port)
        {
            Create();

            try
            {
                var Addresses = Dns.GetHostAddresses(host);

                if (Addresses.Length != 0)
                {
                    var IPV4Address = Addresses.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);

                    state = GameClientState.Connecting;

                    client.ConnectInBackground(IPV4Address, port, true, (exception) =>
                    {
                        if (exception != null)
                        {
                            dispatcher.InvokeAsync(() =>
                            {
                                OnDisconnected?.Invoke();

                                Disconnect();
                                state = GameClientState.Error;
                            });
                        }
                        else
                        {
                            dispatcher.InvokeAsync(() =>
                            {
                                state = GameClientState.Active;
                                sentCounter = 0;
                                receivedCounter = 0;
                                receivedPackets = new List<PacketInfo>();

                                OnConnected?.Invoke();
                            });
                        }
                    });
                }
                else
                {
                    Disconnect();
                    state = GameClientState.Error;
                    OnDisconnected?.Invoke();
                }
            }
            catch(System.Exception)
            {
                Disconnect();
                state = GameClientState.Error;
                OnDisconnected?.Invoke();
            }
        }

        public void Create()
        {
            client = new DarkRiftClient();

            client.MessageReceived += (sender, args) =>
            {
                var tag = args.Tag;

                var message = args.GetMessage();

                var reader = new DarkRiftNetworkReader(message.GetReader());

                if (debuggingNetworkMessages)
                {
                    Debug.Log($"[DarkRiftGameClient] Received raw message with tag {tag} and size {reader.Length}");
                }

                dispatcher.InvokeAsync(() =>
                {
                    receivedPackets.Add(new PacketInfo()
                    {
                        packetIndex = reader.ReadInt16(),
                        reader = reader,
                        tag = tag,
                    });

                    message.Dispose();
                });
            };

            client.Disconnected += (sender, args) =>
            {
                if(state == GameClientState.Disconnected)
                {
                    return;
                }

                dispatcher.InvokeAsync(() =>
                {
                    state = GameClientState.Disconnected;

                    OnDisconnected?.Invoke();
                });
            };
        }

        public void Disconnect()
        {
            if(state != GameClientState.Active)
            {
                return;
            }

            client.Disconnect();
        }

        public void SendMessage(ushort messageID, INetworkMessage message)
        {
            try
            {
                using (DarkRiftWriter writer = DarkRiftWriter.Create())
                {
                    var abstractWriter = new DarkRiftNetworkWriter(writer);

                    if(sentCounter < 0)
                    {
                        sentCounter = 0;
                    }

                    abstractWriter.WriteInt16(sentCounter);

                    sentCounter++;

                    message.Serialize(abstractWriter);

                    if(debuggingNetworkMessages)
                    {
                        Debug.Log($"[DarkRiftGameClient] Sending a message {messageID} with size {abstractWriter.Length}");
                    }

                    using (Message outMessage = Message.Create(messageID, writer))
                    {
                        client.SendMessage(outMessage, SendMode.Reliable);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError($"[DarkRiftGameClient]: Failed to serialize a message: {e}");
            }
        }

        public void Update()
        {
            dispatcher.ExecuteDispatcherTasks();

            for(var i = 0; i < receivedPackets.Count; i++)
            {
                var packet = receivedPackets[i];

                if(receivedCounter < 0)
                {
                    receivedCounter = 0;
                }

                if (packet.packetIndex == receivedCounter)
                {
                    if(packet.tag == (ushort)InternalMessages.BatchMessage)
                    {
                        var data = packet.reader.ReadBytes();

                        var messages = new List<NetworkBatcher.Message>(new NetworkBatcher.BatchWalker(data));

                        if(debuggingNetworkMessages)
                        {
                            Debug.Log($"[DarkRiftGameClient] Received batch message with {data.Length} bytes and {messages.Count} messages");
                        }

                        foreach(var message in messages)
                        {
                            using (var darkRiftReader = DarkRiftReader.CreateFromArray(message.data, 0, message.data.Length))
                            {
                                using (var reader = new DarkRiftNetworkReader(darkRiftReader))
                                {
                                    try
                                    {
                                        OnMessageReceived?.Invoke(message.ID, reader);
                                    }
                                    catch(System.Exception e)
                                    {
                                        Debug.LogError($"[DarkRiftGameClient] Failed to handle message {message.ID} with {message.data.Length} bytes: {e}");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            OnMessageReceived?.Invoke(packet.tag, packet.reader);
                        }
                        catch (System.Exception e)
                        {
                            Debug.LogError($"[DarkRiftGameClient] Failed to handle message {packet.tag} with {packet.reader.Length} bytes: {e}");
                        }
                    }

                    receivedPackets.RemoveAt(i);

                    receivedCounter++;

                    break;
                }
            }
        }

        public void Shutdown()
        {
            if(client != null)
            {
                try
                {
                    client.Dispose();
                }
                catch(System.Exception e)
                {
                    Debug.LogError($"[{GetType().Name}] Failed to dispose the client: {e}");
                }

                client = null;
            }
        }
    }
}

#endif
