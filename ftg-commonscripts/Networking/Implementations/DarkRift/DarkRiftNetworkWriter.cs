﻿#if USING_DARKRIFT
using DarkRift;
using System;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class DarkRiftNetworkWriter : INetworkWriter, IDisposable
    {
        DarkRiftWriter networkWriter;

        public int Length
        {
            get
            {
                return networkWriter.Length;
            }
        }

        public int Position
        {
            get
            {
                return networkWriter.Position;
            }
        }

        public DarkRiftNetworkWriter(DarkRiftWriter writer)
        {
            networkWriter = writer;
        }

        public byte[] ToArray()
        {
            return networkWriter.ToArray();
        }

        public void WriteBool(bool value)
        {
            networkWriter.Write(value);
        }

        public void WriteByte(byte value)
        {
            networkWriter.Write(value);
        }

        public void WriteBytes(byte[] value)
        {
            networkWriter.Write(value);
        }

        public void WriteBytesAndSize(byte[] value, int length)
        {
            networkWriter.WriteRaw(value, 0, length);
        }

        public void WriteChar(char value)
        {
            networkWriter.Write(value);
        }

        public void WriteDouble(double value)
        {
            networkWriter.Write(value);
        }

        public void WriteFloat(float value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt16(short value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt32(int value)
        {
            networkWriter.Write(value);
        }

        public void WriteInt64(long value)
        {
            networkWriter.Write(value);
        }

        public void WriteSByte(sbyte value)
        {
            networkWriter.Write(value);
        }

        public void WriteString(string value)
        {
            if(value == null)
            {
                networkWriter.Write("");

                return;
            }

            networkWriter.Write(value);
        }

        public void WriteUInt16(ushort value)
        {
            networkWriter.Write(value);
        }

        public void WriteUInt32(uint value)
        {
            networkWriter.Write(value);
        }

        public void WriteUInt64(ulong value)
        {
            networkWriter.Write(value);
        }

        public void Dispose()
        {
            networkWriter = null;
        }
    }
}
#endif
