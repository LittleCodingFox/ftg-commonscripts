﻿#if USING_DARKRIFT
using DarkRift;
using DarkRift.Server;
using UnityEngine;
using System.Collections.Generic;
using System;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public sealed class DarkRiftUnityConsoleWriter : LogWriter
    {
        public override Version Version
        {
            get
            {
                return new Version(1, 0, 0);
            }
        }

        public DarkRiftUnityConsoleWriter(LogWriterLoadData pluginLoadData) : base(pluginLoadData)
        {
        }

        public override void WriteEvent(WriteEventArgs args)
        {
            switch (args.LogType)
            {
                case DarkRift.LogType.Trace:
                case DarkRift.LogType.Info:
                    Debug.Log(args.FormattedMessage);
                    break;
                case DarkRift.LogType.Warning:
                    Debug.LogWarning(args.FormattedMessage);
                    break;
                case DarkRift.LogType.Error:
                case DarkRift.LogType.Fatal:
                    Debug.LogError(args.FormattedMessage);
                    break;
            }
        }
    }
}
#endif
