﻿#if USING_DARKRIFT
using DarkRift;
using DarkRift.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class DarkRiftGameServer : IGameServer
    {
        private class PacketInfo
        {
            public short packetIndex;
            public ushort tag;
            public DarkRiftNetworkReader reader;
        }

        private class ClientInfo
        {
            public NetworkBatcher networkBatcher = new NetworkBatcher();
            public List<PacketInfo> receivedPackets = new List<PacketInfo>();
            public short sentPacketCounters = 0;
            public short receivedPacketCounters = 0;
            public long playerID;
            public IClient client;
        }

        private const int MaxBufferSize = 4 * 1024 * 1024;
        private const double TimeBetweenBatches = 60 / 1000.0;

        private List<ClientInfo> clients = new List<ClientInfo>();

        /// <summary>
        /// Internal counter for player IDs
        /// </summary>
        private long playerCounter = 0;

        private DarkRiftServer server;

        /// <summary>
        /// Whether the server has been disposed
        /// </summary>
        private bool disposed = false;

        public event GameServerClientConnectionDelegate OnClientConnected;
        public event GameServerClientConnectionDelegate OnClientDisconnected;
        public event GameServerClientMessageReceivedDelegate OnClientMessageReceived;
        public event GameServerDisposedDelegate OnServerDisposed;

        public bool debuggingNetworkMessages = false;

        private object lockObject = new object();
        private DateTime networkBatcherTimer = DateTime.Now;
        private bool useNetworkBatching = false;

        public void Create(int port, int maxConnections, bool useNetworkBatching)
        {
            this.useNetworkBatching = useNetworkBatching;

            var serverSpawnData = new ServerSpawnData();
            var listenerSettings = new ServerSpawnData.ListenersSettings.NetworkListenerSettings();

            listenerSettings.Type = listenerSettings.Name = "CompatibilityBichannelListener";
            listenerSettings.Address = System.Net.IPAddress.Any;
            listenerSettings.Port = (ushort)port;

            listenerSettings.Settings.Add("maxTcpBodyLength", $"{MaxBufferSize}");
            serverSpawnData.Listeners.NetworkListeners.Add(listenerSettings);

            server = new DarkRiftServer(serverSpawnData);
 
            server.ClientManager.ClientConnected += (sender, args) =>
            {
                ClientInfo clientInfo;

                lock (lockObject)
                {
                    var playerID = ++playerCounter;

                    clientInfo = new ClientInfo()
                    {
                        playerID = playerID,
                        client = args.Client,
                    };

                    clients.Add(clientInfo);
                }

                args.Client.MessageReceived += (sender2, args2) =>
                {
                    var client = args2.Client;
                    var tag = args2.Tag;

                    var message = args2.GetMessage();

                    if (debuggingNetworkMessages)
                    {
                        Debug.Log($"[DarkRiftGameServer] Received raw message with tag {tag} and size {message.DataLength}");
                    }

                    lock(lockObject)
                    {
                        clientInfo = clients.FirstOrDefault(x => x.client == client);
                    }

                    server.Dispatcher.InvokeAsync(() =>
                    {
                        var reader = new DarkRiftNetworkReader(message.GetReader());
                        var packetIndex = reader.ReadInt16();

                        lock(lockObject)
                        {
                            var playerID = clientInfo.playerID;

                            if (clientInfo.receivedPackets.Count >= 10)
                            {
                                DisconnectPlayer(playerID);

                                return;
                            }

                            clientInfo.receivedPackets.Add(new PacketInfo()
                            {
                                packetIndex = packetIndex,
                                reader = reader,
                                tag = tag,
                            });
                        }

                        message.Dispose();
                    });
                };

                server.Dispatcher.InvokeAsync(() =>
                {
                    lock (lockObject)
                    {
                        clientInfo = clients.FirstOrDefault(x => x.client == args.Client);

                        if(clientInfo != null)
                        {
                            OnClientConnected(clientInfo.playerID);
                        }
                    }
                });
            };

            server.ClientManager.ClientDisconnected += (sender, args) =>
            {
                lock(lockObject)
                {
                    var clientInfo = clients.FirstOrDefault(x => x.client == args.Client);

                    if(clientInfo != null)
                    {
                        server.Dispatcher.InvokeAsync(() =>
                        {
                            lock (lockObject)
                            {
                                var playerID = clientInfo.playerID;

                                OnClientDisconnected(playerID);

                                if(args.Client.ConnectionState != ConnectionState.Connected)
                                {
                                    clients.Remove(clientInfo);
                                }
                            }
                        });
                    }
                }
            };

            server.StartServer();
        }

        public void DisconnectPlayer(long playerID)
        {
            lock(lockObject)
            {
                var clientInfo = clients.FirstOrDefault(x => x.playerID == playerID);

                if(clientInfo != null)
                {
                    clientInfo.client.Disconnect();

                    clients.Remove(clientInfo);
                }
            }
        }

        public void SendMessageToPlayer(long playerID, ushort messageID, INetworkMessage message)
        {
            lock (lockObject)
            {
                var clientInfo = clients.FirstOrDefault(x => x.playerID == playerID);

                if (clientInfo != null)
                {
                    if (useNetworkBatching)
                    {
                        byte[] messageData = new byte[0];

                        using (DarkRiftWriter writer = DarkRiftWriter.Create())
                        {
                            var abstractWriter = new DarkRiftNetworkWriter(writer);

                            message.Serialize(abstractWriter);

                            messageData = abstractWriter.ToArray();
                        }

                        var previousSize = clientInfo.networkBatcher.Length;

                        if (!clientInfo.networkBatcher.Batch(messageID, messageData))
                        {
                            using (DarkRiftWriter writer = DarkRiftWriter.Create())
                            {
                                var abstractWriter = new DarkRiftNetworkWriter(writer);
                                var counter = clientInfo.sentPacketCounters;

                                if (counter < 0)
                                {
                                    counter = 0;
                                }

                                abstractWriter.WriteInt16(counter);

                                counter++;

                                clientInfo.sentPacketCounters = counter;

                                var outData = clientInfo.networkBatcher.Buffer.Take(clientInfo.networkBatcher.Length)
                                    .Concat(NetworkBatcher.Encode(messageID, messageData))
                                    .ToArray();

                                abstractWriter.WriteBytes(outData);

                                clientInfo.networkBatcher.Clear();

                                using (Message outMessage = Message.Create((ushort)InternalMessages.BatchMessage, writer))
                                {
                                    if (debuggingNetworkMessages)
                                    {
                                        using (var reader = outMessage.GetReader())
                                        {
                                            Debug.Log($"[DarkRiftGameServer] Sending batch payload message to player {playerID} with size {reader.Length}");

                                            var walker = new NetworkBatcher.BatchWalker(outData);

                                            Debug.Log($"[DarkRiftGameServer] Messages:");

                                            foreach (var piece in walker)
                                            {
                                                Debug.Log($"[DarkRiftGameServer]    {piece.ID} ({piece.data.Length})");
                                            }

                                            Debug.Log($"[DarkRiftGameServer] End");
                                        }
                                    }

                                    clientInfo.client.SendMessage(outMessage, SendMode.Reliable);
                                }
                            }
                        }
                        else
                        {
                            if (debuggingNetworkMessages)
                            {
                                Debug.Log($"[DarkRiftGameServer] Batching message {messageID} to player {playerID} with size {clientInfo.networkBatcher.Length - previousSize}." +
                                    $" Batch size: {clientInfo.networkBatcher.Length}");
                            }
                        }
                    }
                    else
                    {
                        using (DarkRiftWriter writer = DarkRiftWriter.Create())
                        {
                            var abstractWriter = new DarkRiftNetworkWriter(writer);
                            var counter = clientInfo.sentPacketCounters;

                            if (counter < 0)
                            {
                                counter = 0;
                            }

                            abstractWriter.WriteInt16(counter);

                            counter++;

                            clientInfo.sentPacketCounters = counter;

                            message.Serialize(abstractWriter);

                            using (Message outMessage = Message.Create(messageID, writer))
                            {
                                if (debuggingNetworkMessages)
                                {
                                    using (var reader = outMessage.GetReader())
                                    {
                                        Debug.Log($"[DarkRiftGameServer] Sending message {messageID} to player {playerID} with size {reader.Length}");
                                    }
                                }

                                clientInfo.client.SendMessage(outMessage, SendMode.Reliable);
                            }
                        }
                    }
                }
            }
        }

        public void Shutdown()
        {
            try
            {
                server?.Dispose();

                disposed = true;
            }
            catch(System.Exception e)
            {
                Debug.LogError($"[{GetType().Name}] Failed to dispose the server: {e}");
            }
        }

        public void SendBatchMessages()
        {
            lock(lockObject)
            {
                foreach (var clientInfo in clients)
                {
                    if (clientInfo.networkBatcher.Length > 0)
                    {
                        using (DarkRiftWriter writer = DarkRiftWriter.Create())
                        {
                            var abstractWriter = new DarkRiftNetworkWriter(writer);
                            var counter = clientInfo.sentPacketCounters;

                            if (counter < 0)
                            {
                                counter = 0;
                            }

                            abstractWriter.WriteInt16(counter);

                            counter++;

                            clientInfo.sentPacketCounters = counter;

                            var outData = clientInfo.networkBatcher.Buffer.Take(clientInfo.networkBatcher.Length).ToArray();

                            abstractWriter.WriteBytes(outData);

                            clientInfo.networkBatcher.Clear();

                            using (Message outMessage = Message.Create((ushort)InternalMessages.BatchMessage, writer))
                            {
                                if (debuggingNetworkMessages)
                                {
                                    using (var reader = outMessage.GetReader())
                                    {
                                        Debug.Log($"[DarkRiftGameServer] Sending batch payload messages to player {clientInfo.playerID} with size {reader.Length}");

                                        var walker = new NetworkBatcher.BatchWalker(outData);

                                        Debug.Log($"[DarkRiftGameServer] Messages:");

                                        foreach (var message in walker)
                                        {
                                            Debug.Log($"[DarkRiftGameServer]    {message.ID} ({message.data.Length})");
                                        }

                                        Debug.Log($"[DarkRiftGameServer] End");
                                    }
                                }

                                clientInfo.client.SendMessage(outMessage, SendMode.Reliable);
                            }
                        }
                    }
                }
            }
        }

        public void Update()
        {
            if(server == null)
            {
                return;
            }

            if(disposed && server.Disposed)
            {
                server = null;

                playerCounter = 0;

                clients = new List<ClientInfo>();

                OnServerDisposed?.Invoke();

                return;
            }

            server.ExecuteDispatcherTasks();

            if(useNetworkBatching)
            {
                lock (lockObject)
                {
                    if ((DateTime.Now - networkBatcherTimer).TotalSeconds >= TimeBetweenBatches)
                    {
                        networkBatcherTimer = DateTime.Now;

                        SendBatchMessages();
                    }
                }
            }

            lock (lockObject)
            {
                foreach(var clientInfo in clients)
                {
                    var counter = clientInfo.receivedPacketCounters;

                    if (counter < 0)
                    {
                        counter = 0;
                    }

                    for (var i = 0; i < clientInfo.receivedPackets.Count; i++)
                    {
                        var packet = clientInfo.receivedPackets[i];

                        if (packet.packetIndex == counter)
                        {
                            OnClientMessageReceived?.Invoke(clientInfo.playerID, packet.tag, packet.reader);

                            clientInfo.receivedPackets.RemoveAt(i);

                            counter++;

                            clientInfo.receivedPacketCounters = counter;

                            break;
                        }
                    }
                }
            }
        }
    }
}

#endif
