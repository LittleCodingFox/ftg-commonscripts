﻿#if USING_DARKRIFT
using DarkRift;
using System;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class DarkRiftNetworkReader : INetworkReader, IDisposable
    {
        private DarkRiftReader networkReader;

        public int Length
        {
            get
            {
                return networkReader.Length;
            }
        }

        public int Position
        {
            get
            {
                return networkReader.Position;
            }
        }

        public DarkRiftNetworkReader(DarkRiftReader reader)
        {
            networkReader = reader;
        }

        public bool ReadBool()
        {
            return networkReader.ReadBoolean();
        }

        public byte ReadByte()
        {
            return networkReader.ReadByte();
        }

        public byte[] ReadBytes()
        {
            return networkReader.ReadBytes();
        }

        public byte[] ReadBytesAndSize(int length)
        {
            return networkReader.ReadRaw(length);
        }

        public char ReadChar()
        {
            return networkReader.ReadChar();
        }

        public double ReadDouble()
        {
            return networkReader.ReadDouble();
        }

        public float ReadFloat()
        {
            return networkReader.ReadSingle();
        }

        public short ReadInt16()
        {
            return networkReader.ReadInt16();
        }

        public int ReadInt32()
        {
            return networkReader.ReadInt32();
        }

        public long ReadInt64()
        {
            return networkReader.ReadInt64();
        }

        public sbyte ReadSByte()
        {
            return networkReader.ReadSByte();
        }

        public string ReadString()
        {
            return networkReader.ReadString();
        }

        public ushort ReadUInt16()
        {
            return networkReader.ReadUInt16();
        }

        public uint ReadUInt32()
        {
            return networkReader.ReadUInt32();
        }

        public ulong ReadUInt64()
        {
            return networkReader.ReadUInt64();
        }

        public void Dispose()
        {
            networkReader = null;
        }
    }
}
#endif
