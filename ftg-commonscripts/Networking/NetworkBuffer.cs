﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NetworkBuffer<T>
{
    public int Size => elements.Count;

    private int counter;

    private readonly Queue<T> elements;
    private readonly int bufferSize;
    private readonly int correctionTollerance;

    public NetworkBuffer(int bufferSize, int correctionTollerance)
    {
        this.bufferSize = bufferSize;
        this.correctionTollerance = correctionTollerance;

        elements = new Queue<T>();
    }

    public void Add(T element)
    {
        elements.Enqueue(element);
    }

    public int Get(ref T[] buffer)
    {
        if (buffer.Length != bufferSize)
        {
            Debug.LogWarning($"[{GetType().Name}] Attempted to Get with an incorrect sized buffer");

            return 0;
        }

        int size = elements.Count - 1;

        if (size == bufferSize)
        {
            counter = 0;
        }

        if (size > bufferSize)
        {
            if (counter < 0)
            {
                counter = 0;
            }

            counter++;

            if (counter > correctionTollerance)
            {
                int amount = elements.Count - bufferSize;

                for (int i = 0; i < amount; i++)
                {
                    buffer[i] = elements.Dequeue();
                }

                return amount;
            }
        }

        if (size < bufferSize)
        {
            if (counter > 0)
            {
                counter = 0;
            }

            counter--;

            if (-counter > correctionTollerance)
            {
                return 0;
            }
        }

        if (elements.Any())
        {
            buffer[0] = elements.Dequeue();

            return 1;
        }

        return 0;
    }
}
