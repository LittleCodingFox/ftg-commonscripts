﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public class NetworkBatcher
    {
        public const int MaxSize = (int)GameServerFlags.MaxBatchSize;

        public enum DecodeStatus
        {
            Success,
            Empty,
            Corrupted
        }

        public class Message
        {
            public ushort ID;
            public byte[] data;
        }

        public class BatchWalker : IEnumerable<Message>
        {
            private byte[] buffer;
            private int position = 0;

            public BatchWalker(byte[] buffer)
            {
                this.buffer = buffer;
            }

            public IEnumerator<Message> GetEnumerator()
            {
                for (; ; )
                {
                    if (Decode(buffer, ref position, out var message, out var data) == DecodeStatus.Success)
                    {
                        yield return new Message()
                        {
                            ID = message,
                            data = data,
                        };
                    }
                    else
                    {
                        yield break;
                    }
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                for(; ; )
                {
                    if(Decode(buffer, ref position, out var message, out var data) == DecodeStatus.Success)
                    {
                        yield return new Message()
                        {
                            ID = message,
                            data = data,
                        };
                    }
                    else
                    {
                        yield break;
                    }
                }
            }
        }

        private byte[] buffer = new byte[MaxSize];
        private int position = 0;

        public int Length
        {
            get
            {
                return position;
            }
        }

        public byte[] Buffer
        {
            get
            {
                return buffer;
            }
        }

        public static byte[] Encode(ushort message, byte[] data)
        {
            var sizeVarint = VarintBitConverter.GetVarintBytes(data.Length);

            int position = 0;
            byte[] buffer = new byte[data.Length + sizeof(ushort) + sizeof(byte) + sizeVarint.Length];

            buffer[position++] = (byte)message;
            buffer[position++] = (byte)(message >> 8);

            buffer[position++] = (byte)sizeVarint.Length;

            Array.Copy(sizeVarint, 0, buffer, position, sizeVarint.Length);

            position += sizeVarint.Length;

            Array.Copy(data, 0, buffer, position, data.Length);

            return buffer;
        }

        public static DecodeStatus Decode(byte[] inData, ref int position, out ushort outMessage, out byte[] outData)
        {
            outMessage = 0;
            outData = new byte[0];

            if (position + sizeof(ushort) >= inData.Length)
            {
                return DecodeStatus.Empty;
            }

            outMessage = BitConverter.ToUInt16(inData, position);

            position += sizeof(ushort);

            if (position + sizeof(byte) >= inData.Length)
            {
                return DecodeStatus.Corrupted;
            }

            var dataSize = inData[position++];

            if (position + dataSize > inData.Length)
            {
                return DecodeStatus.Corrupted;
            }

            var dataSizeBytes = inData.Skip(position)
                .Take(dataSize)
                .ToArray();

            var dataLength = VarintBitConverter.ToInt32(dataSizeBytes);

            position += dataSize;

            if (position + dataLength > inData.Length)
            {
                return DecodeStatus.Corrupted;
            }

            outData = inData.Skip(position)
                .Take(dataLength)
                .ToArray();

            position += dataLength;

            return DecodeStatus.Success;
        }

        public void Clear()
        {
            position = 0;
        }

        public bool Batch(ushort message, byte[] data)
        {
            var sizeVarint = VarintBitConverter.GetVarintBytes(data.Length);

            if(position + data.Length + sizeof(ushort) + sizeof(byte) + sizeVarint.Length > MaxSize)
            {
                return false;
            }

            var inData = Encode(message, data);

            Array.Copy(inData, 0, buffer, position, inData.Length);

            position += inData.Length;

            return true;
        }
    }
}
