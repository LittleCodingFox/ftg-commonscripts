﻿using System.Collections;
using System.Collections.Generic;

namespace FlamingTorchGames.CommonScripts.Networking
{
    public interface INetworkMessage
    {
        void Serialize(INetworkWriter writer);
        void Deserialize(INetworkReader reader);
    }
}
