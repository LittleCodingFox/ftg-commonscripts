﻿using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class NetworkRoomManager : MonoBehaviour
    {
        public static NetworkRoomManager instance
        {
            get;
            private set;
        }

        public GameObject roomPrefab;

        private Dictionary<string, NetworkRoom> rooms = new Dictionary<string, NetworkRoom>();

        private void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);

                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public virtual NetworkRoom CreateRoom(string roomName, byte maxSlots)
        {
            if(roomPrefab == null)
            {
                Debug.LogError($"[{GetType().Name}] Missing room prefab");

                return null;
            }

            if(rooms.ContainsKey(roomName))
            {
                Debug.LogWarning($"[{GetType().Name}] Duplicate room name for {roomName}. Not creating a room.");

                return null;
            }

            var instancedRoom = Instantiate(roomPrefab);

            if(instancedRoom == null)
            {
                Debug.LogError($"[{GetType().Name}] Failed to create room {roomName} with {maxSlots} slots");

                return null;
            }

            var networkRoom = instancedRoom.GetComponent<NetworkRoom>();

            if(networkRoom == null)
            {
                Debug.LogError($"[{GetType().Name}] Room prefab is missing NetworkRoom behaviour");

                Destroy(instancedRoom);

                return null;
            }

            networkRoom.Initialize(roomName, maxSlots);
            rooms.Add(roomName, networkRoom);

            return networkRoom;
        }

        public virtual void DestroyRoom(string roomName)
        {
            if(rooms.TryGetValue(roomName, out var networkRoom))
            {
                networkRoom.DeInitialize();

                rooms.Remove(roomName);
            }
        }
    }
}
