﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FlamingTorchGames.CommonScripts
{
    public class NetworkRoom : MonoBehaviour
    {
        public string roomName;
        public byte maxSlots;

        public List<long> players = new List<long>();

        protected Scene scene;
        protected PhysicsScene physicsScene;

        public void Initialize(string roomName, byte maxSlots)
        {
            this.roomName = roomName;
            this.maxSlots = maxSlots;

            var createSceneParameters = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
            scene = SceneManager.CreateScene($"Room ({roomName})", createSceneParameters);
            physicsScene = scene.GetPhysicsScene();

            SceneManager.MoveGameObjectToScene(gameObject, scene);
        }

        public void DeInitialize()
        {
            SceneManager.UnloadSceneAsync(scene);
        }
    }
}
