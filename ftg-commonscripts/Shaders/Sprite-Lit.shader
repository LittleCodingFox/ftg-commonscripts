﻿Shader "Sprites/Lit"
{
    Properties
    {
        _MainCol("Main Tint", Color) = (1,1,1,1)
        _MainTex("Main Texture", 2D) = "white" {}
        _Cutoff("Alpha cutoff", Range(0,1)) = 0.5
    }

    SubShader
    {
        Tags
        {
            "Queue" = "AlphaTest"
            "IgnoreProjector" = "True"
            "RenderType" = "TransparentCutout"
            "PreviewType" = "Plane"
        }

        Cull Off
        LOD 200

        CGPROGRAM
        #pragma surface surf SimpleLambert alphatest:_Cutoff addshadow fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;
        fixed4 _MainCol;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        half4 LightingSimpleLambert(SurfaceOutput s, half3 lightDir, half atten)
        {
            half NdotL = dot(s.Normal, lightDir);
            half4 c;
            c.rgb = s.Albedo * _MainCol.rgb * atten * _LightColor0.rgb;
            c.a = s.Alpha;

            return c;
        }

        struct Input {
            float2 uv_MainTex;
            float2 uv_DetailTex;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _MainCol;

            o.Albedo = fixed4(c.rgb, 1);
            o.Alpha = c.a;
        }
        ENDCG
    }

    Fallback "Transparent/Cutout/VertexLit"
}