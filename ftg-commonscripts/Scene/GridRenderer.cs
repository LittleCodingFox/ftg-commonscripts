﻿using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class GridRenderer : MonoBehaviour
    {
        public Vector3 size;
        public Color color = Color.white;
        public float gridSize = 1.0f;
        public int step = 1;

        public LineRenderer lineRenderer;

        private Material lineMaterial;
        private Vector3 previousSize;
        private Color previousColor;
        private int previousStep;
        private Vector3[] vertices;
        private Vector3 previousPosition;

        private void Start()
        {
            var shader = Shader.Find("Hidden/Internal-Colored");

            lineMaterial = new Material(shader)
            {
                hideFlags = HideFlags.HideAndDontSave
            };

            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            lineMaterial.SetInt("_ZWrite", 0);
        }

        private void FixedUpdate()
        {
            if(lineMaterial != null && (previousSize != size || previousColor != color || previousStep != step || previousPosition != transform.position))
            {
                if(step <= 0)
                {
                    return;
                }

                previousColor = color;
                previousSize = size;
                previousStep = step;
                previousPosition = transform.position;

                var start = new Vector3(
                    Mathf.Round(transform.position.x),
                    Mathf.Round(transform.position.y),
                    Mathf.Round(transform.position.z)
                );

                var newVertices = new List<Vector3>();

                System.Action<int> drawGridFunction = (corner) =>
                {
                    Vector2 size = Vector2.zero;
                    Vector3[] offsets = new Vector3[4];
                    Vector3 positionOffset = Vector3.zero;
                    Vector3 axis = Vector3.forward;

                    switch (corner)
                    {
                        case 0: //Front
                            size = new Vector2(this.size.x, this.size.y);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 1),
                                new Vector3(1, 1),
                                new Vector3(1, 0)
                            };

                            positionOffset = new Vector3(-this.size.x / 2, -this.size.y / 2, -this.size.z / 2);

                            break;

                        case 1: //Back
                            size = new Vector2(this.size.x, this.size.y);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 1),
                                new Vector3(1, 1),
                                new Vector3(1, 0)
                            };

                            positionOffset = new Vector3(-this.size.x / 2, -this.size.y / 2, this.size.z / 2);

                            break;

                        case 2: //Left
                            size = new Vector2(this.size.z, this.size.y);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 0, 1),
                                new Vector3(0, 1, 1),
                                new Vector3(0, 1, 0)
                            };

                            positionOffset = new Vector3(-this.size.x / 2, -this.size.y / 2, -this.size.z / 2);

                            axis = Vector3.left;

                            break;

                        case 3: //Right
                            size = new Vector2(this.size.z, this.size.y);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 0, 1),
                                new Vector3(0, 1, 1),
                                new Vector3(0, 1, 0)
                            };

                            positionOffset = new Vector3(this.size.x / 2, -this.size.y / 2, -this.size.z / 2);

                            axis = Vector3.left;

                            break;

                        case 4: //Up
                            size = new Vector2(this.size.x, this.size.z);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 0, 1),
                                new Vector3(1, 0, 1),
                                new Vector3(1, 0, 0)
                            };

                            positionOffset = new Vector3(-this.size.x / 2, this.size.y / 2, -this.size.z / 2);

                            axis = Vector3.up;

                            break;

                        case 5: //Down
                            size = new Vector2(this.size.x, this.size.z);
                            offsets = new Vector3[]
                            {
                                Vector3.zero,
                                new Vector3(0, 0, 1),
                                new Vector3(1, 0, 1),
                                new Vector3(1, 0, 0)
                            };

                            positionOffset = new Vector3(-this.size.x / 2, -this.size.y / 2, -this.size.z / 2);

                            axis = Vector3.up;

                            break;
                    }

                    for (float y = 0; y < size.y; y+=step)
                    {
                        for (float x = 0; x < size.x; x+=step)
                        {
                            Vector3 position = transform.position + positionOffset * gridSize;

                            if (axis == Vector3.forward)
                            {
                                position = position + new Vector3(x, y) * gridSize;
                            }
                            else if (axis == Vector3.left)
                            {
                                position = position + new Vector3(0, y, x) * gridSize;
                            }
                            else if (axis == Vector3.up)
                            {
                                position = position + new Vector3(x, 0, y) * gridSize;
                            }

                            Vector3[] scaledOffsets = new Vector3[4]
                            {
                                offsets[0] * gridSize * step,
                                offsets[1] * gridSize * step,
                                offsets[2] * gridSize * step,
                                offsets[3] * gridSize * step,
                            };

                            newVertices.Add(position);
                            newVertices.Add(position + scaledOffsets[0]);
                            newVertices.Add(position + scaledOffsets[0]);
                            newVertices.Add(position + scaledOffsets[1]);
                            newVertices.Add(position + scaledOffsets[1]);
                            newVertices.Add(position + scaledOffsets[2]);
                            newVertices.Add(position + scaledOffsets[2]);
                            newVertices.Add(position + scaledOffsets[3]);
                            newVertices.Add(position + scaledOffsets[3]);
                            newVertices.Add(position + scaledOffsets[0]);
                        }
                    }
                };

                for (int i = 0; i < 6; i++)
                {
                    drawGridFunction(i);
                }

                vertices = newVertices.ToArray();

                if(lineRenderer != null)
                {
                    lineRenderer.startColor = color;
                    lineRenderer.endColor = color;
                    lineRenderer.positionCount = vertices.Length;
                    lineRenderer.SetPositions(vertices);
                }
            }
        }

        private void OnRenderObject()
        {
            if(lineRenderer == null)
            {
                lineMaterial.SetPass(0);

                GL.PushMatrix();
                GL.Begin(GL.LINES);
                GL.Color(color);

                if(vertices != null)
                {
                    foreach (var vertex in vertices)
                    {
                        GL.Vertex(vertex);
                    }
                }

                GL.End();
                GL.PopMatrix();
            }
        }
    }
}
