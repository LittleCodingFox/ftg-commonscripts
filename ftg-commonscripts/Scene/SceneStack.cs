﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class SceneStack : MonoBehaviour
        {
            private Stack<string> sceneStack = new Stack<string>();

            private static SceneStack privInstance;
            public static SceneStack instance
            {
                get
                {
                    if (privInstance == null)
                    {
                        GameObject stackObject = GameObject.Find("SceneStack");

                        if (stackObject != null)
                        {
                            privInstance = stackObject.GetComponent<SceneStack>();

                            if (privInstance == null)
                            {
                                DestroyImmediate(stackObject);
                            }
                        }

                        if (privInstance == null)
                        {
                            stackObject = new GameObject("SceneStack");
                            privInstance = stackObject.AddComponent<SceneStack>();

                            DontDestroyOnLoad(stackObject);
                        }
                    }

                    return privInstance;
                }
            }

            public string CurrentSceneName
            {
                get
                {
                    return SceneManager.GetActiveScene().name;
                }
            }

            public void PushScene(string sceneName, string currentSceneName = "", bool additive = false)
            {
                currentSceneName = currentSceneName.Length == 0 ? CurrentSceneName : currentSceneName;

                if (sceneStack.Count > 0 && sceneStack.Peek() == currentSceneName)
                {
                    Debug.LogError("[SceneStack] Attempted to push the same scene twice in a row");

                    return;
                }

                sceneStack.Push(currentSceneName);

                SceneManager.LoadScene(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            }

            public AsyncOperation PushSceneAsync(string sceneName, string currentSceneName = "", bool additive = false)
            {
                currentSceneName = currentSceneName.Length == 0 ? CurrentSceneName : currentSceneName;

                if (sceneStack.Count > 0 && sceneStack.Peek() == currentSceneName)
                {
                    Debug.LogError("[SceneStack] Attempted to push the same scene twice in a row");

                    return null;
                }

                sceneStack.Push(currentSceneName);

                return SceneManager.LoadSceneAsync(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            }

            public void PopScene(bool additive = false)
            {
                if (sceneStack.Count == 0)
                {
                    Debug.LogError("[SceneStack] Attempted to pop a scene from an empty stack");

                    return;
                }

                string sceneName = sceneStack.Pop();

                SceneManager.LoadScene(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            }

            public AsyncOperation PopSceneAsync(bool additive = false)
            {
                if (sceneStack.Count == 0)
                {
                    Debug.LogError("[SceneStack] Attempted to pop a scene from an empty stack");

                    return null;
                }

                string sceneName = sceneStack.Pop();

                return SceneManager.LoadSceneAsync(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            }
        }
    }
}
