﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Stores information on a language, as well as its translated strings
        /// </summary>
        [Serializable]
        public class TranslationLanguageInfo
        {
            /// <summary>
            /// The name of the language
            /// </summary>
            public string name;

            /// <summary>
            /// The ISO code identifier for the language. Used to identify the language to use.
            /// </summary>
            public string isoCode;

            /// <summary>
            /// All translated strings for this particular language
            /// </summary>
            public List<TranslationString> translatedStrings = new List<TranslationString>();

            [System.NonSerialized]
            public Dictionary<string, TranslationString> cachedTranslations = new Dictionary<string, TranslationString>();

            /// <summary>
            /// Rebuilds the translation cache for fast access
            /// </summary>
            public void RebuildCache()
            {
                cachedTranslations.Clear();

                foreach (var translationString in translatedStrings)
                {
                    if (!cachedTranslations.ContainsKey(translationString.key))
                    {
                        cachedTranslations.Add(translationString.key, translationString);
                    }
                }
            }

            /// <summary>
            /// Makes a copy of this language info
            /// </summary>
            /// <returns>The copy of this language info</returns>
            public TranslationLanguageInfo Clone()
            {
                return new TranslationLanguageInfo()
                {
                    name = name,
                    isoCode = isoCode,
                    translatedStrings = translatedStrings.Select(x => x.Clone()).ToList(),
                };
            }

            /// <summary>
            /// Merges this language data with the data from another language
            /// </summary>
            /// <param name="language">The other language to merge with</param>
            public void Merge(TranslationLanguageInfo language)
            {
                foreach(var translation in language.translatedStrings)
                {
                    var localString = translatedStrings.FirstOrDefault(x => x.key == translation.key);

                    if(localString != null)
                    {
                        translatedStrings.Remove(localString);
                    }

                    translatedStrings.Add(translation);
                }
            }

            /// <summary>
            /// Gets a translation string for a key
            /// </summary>
            /// <param name="key">The key to search for</param>
            /// <returns>The translation string, or null</returns>
            public TranslationString GetString(string key)
            {
                if(key == null)
                {
                    Debug.LogError("[TranslationLanguageInfo] null key requested for GetString");

                    return new TranslationString()
                    {
                        key = "null",
                        value = "null",
                    };
                }

                if(cachedTranslations.Count == 0)
                {
                    RebuildCache();
                }

                return cachedTranslations.TryGetValue(key, out var translation) ? translation : null;
            }

            /// <summary>
            /// Exports this language to JSON
            /// </summary>
            /// <returns>The exported JSON</returns>
            public string ToJSON()
            {
                return JsonUtility.ToJson(this, true);
            }

            /// <summary>
            /// Imports this language from JSON
            /// </summary>
            /// <param name="json">The JSON for this language</param>
            public void FromJSON(string json)
            {
                var languageInfo = JsonUtility.FromJson<TranslationLanguageInfo>(json);

                if(languageInfo == null)
                {
                    return;
                }

                name = languageInfo.name;
                isoCode = languageInfo.isoCode;
                translatedStrings = languageInfo.translatedStrings;

                cachedTranslations = new Dictionary<string, TranslationString>();
            }
        }
    }
}
