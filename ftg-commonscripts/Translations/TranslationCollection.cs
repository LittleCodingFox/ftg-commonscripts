﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FlamingTorchGames.CommonScripts
{
    /// <summary>
    /// Contains information on translated strings for a game
    /// </summary>
    [Serializable]
    public class TranslationCollection
    {
        public List<TranslationLanguageInfo> languages = new List<TranslationLanguageInfo>();

        /// <summary>
        /// Gets a language by name
        /// </summary>
        /// <param name="isoCode">The iso code of the language</param>
        /// <returns>The language, or null</returns>
        public TranslationLanguageInfo GetLanguage(string isoCode)
        {
            return languages
                .Where(x => x.isoCode == isoCode)
                .Select(x => x.Clone())
                .FirstOrDefault();
        }

        /// <summary>
        /// Gets a translated string for a specific language with a specific key
        /// </summary>
        /// <param name="isoCode">The ISO Code for the language to fetch</param>
        /// <param name="key">The key of the string to fetch</param>
        /// <returns>The translated string, or key if missing</returns>
        public string GetString(string isoCode, string key)
        {
            var language = GetLanguage(isoCode);

            if (language == null)
            {
                return key;
            }

            var translatedString = language.GetString(key);

            if (translatedString == null)
            {
                return key;
            }

            return translatedString.value;
        }

        /// <summary>
        /// Gets a translated string for a specific key
        /// </summary>
        /// <param name="key">The key of the string to fetch</param>
        /// <returns>The translated string, or key if missing</returns>
        public string GetString(string key)
        {
            return GetString(TranslationUtils.CurrentIsoCode, key);
        }
    }
}
