﻿using UnityEngine;
#if USE_JSONNET
using Newtonsoft.Json;
#endif

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Utilities for managing translations, especially for managing the global translations list
        /// </summary>
        public class TranslationUtils
        {
            public static readonly string PreferencesKey = "GAME_LANGUAGE";

            private static string privCurrentIsoCode = "en-US";

            /// <summary>
            /// Gets the current iso code to use with the translation collections
            /// </summary>
            public static string CurrentIsoCode
            {
                get
                {
                    return privCurrentIsoCode;
                }

                set
                {
                    privCurrentIsoCode = value;

                    PlayerPrefs.SetString(PreferencesKey, value);
                }
            }

            /// <summary>
            /// Loads the current iso code from the unity player preferences
            /// </summary>
            public static void LoadIsoCodeFromPlayerPrefs()
            {
                privCurrentIsoCode = PlayerPrefs.GetString(PreferencesKey, privCurrentIsoCode);
            }

            private static TranslationCollection privGlobalTranslations = new TranslationCollection();

            /// <summary>
            /// Gets the global translation instance
            /// </summary>
            public static TranslationCollection GlobalTranslations
            {
                get
                {
                    return privGlobalTranslations;
                }
            }

            /// <summary>
            /// Loads the translation strings for all languages from a JSON String
            /// </summary>
            /// <param name="value">The formatted JSON string</param>
            /// <param name="collection">The collection to load into</param>
            /// <returns>Whether the collection was successfully loaded</returns>
            public static bool LoadTranslationsFromJSONString(string value, ref TranslationCollection collection)
            {
                var tempCollection = JsonUtility.FromJson<TranslationCollection>(value);

                if(tempCollection == null)
                {
                    return false;
                }

                collection.languages = tempCollection.languages;

                return true;
            }

            /// <summary>
            /// Loads the translation strings for all languages from a JSON file
            /// </summary>
            /// <param name="path">The path of the JSON file to load</param>
            /// <param name="collection">The collection to load into</param>
            /// <returns>Whether the collection was successfully loaded</returns>
            public static bool LoadTranslationsFromJSONFile(string path, ref TranslationCollection collection)
            {
                try
                {
                    var inText = System.IO.File.ReadAllText(path);

                    return LoadTranslationsFromJSONString(inText, ref collection);
                }
                catch(System.Exception)
                {
                }

                return false;
            }

            /// <summary>
            /// Loads the translation strings for all languages for the Global Translations from a JSON file
            /// </summary>
            /// <param name="path">The path of the JSON file to load</param>
            /// <returns>Whether the collection was successfully loaded</returns>
            public static bool LoadGlobalTranslationsFromJSONFile(string path)
            {
                var collection = new TranslationCollection();

                if(!LoadTranslationsFromJSONFile(path, ref collection))
                {
                    return false;
                }

                privGlobalTranslations = collection;

                return collection != null;
            }

            /// <summary>
            /// Loads the translation strings for all languages for the Global Translations from a JSON String
            /// </summary>
            /// <param name="value">The formatted JSON string</param>
            /// <returns>Whether the collection was successfully loaded</returns>
            public static bool LoadGlobalTranslationsFromJSONString(string value)
            {
                var collection = new TranslationCollection();

                if(!LoadTranslationsFromJSONString(value, ref collection))
                {
                    return false;
                }

                privGlobalTranslations = collection;

                return collection != null;
            }
        }
    }
}