﻿using System;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Stores details on a translated string
        /// </summary>
        [Serializable]
        public class TranslationString
        {
            /// <summary>
            /// The name of the translated string
            /// </summary>
            public string key;

            /// <summary>
            /// The value of the translated string
            /// </summary>
            public string value;

            /// <summary>
            /// Makes a copy of this translation string
            /// </summary>
            /// <returns>The copy of the translation string</returns>
            public TranslationString Clone()
            {
                return new TranslationString()
                {
                    key = key,
                    value = value,
                };
            }
        }
    }
}
