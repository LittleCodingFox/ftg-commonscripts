﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

namespace FlamingTorchGames.CommonScripts
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class CharacterMovement : MonoBehaviour
    {
        public int horizontalRays = 4;
        public int verticalRays = 4;

        public enum Direction
        {
            Left,
            Right,
            Top,
            Bottom
        }

        public string MovementAnimationName
        {
            get
            {
                Vector2 directionVector = new Vector2(transform.position.x - lastPosition.x, transform.position.y - lastPosition.y);

                bool moving = directionVector != Vector2.zero;

                return string.Format("{0}{1}", moving ? "Walk" : "Idle", direction.ToString());
            }
        }

        public GameObject targetedObject;
        public CharacterMovement targetedCharacter;
        public LayerMask collisionLayers;
        public LayerMask triggerLayers;
        public float distance = 1.0f;

        public UnityEvent OnBeforeUpdate;
        public UnityEvent OnUpdate;
        public UnityEvent OnAfterUpdate;

        private BoxCollider2D boxCollider;
        private Vector3 lastPosition;
        public Direction direction = Direction.Bottom;

        private void Awake()
        {
            boxCollider = gameObject.GetComponent<BoxCollider2D>();
        }

        void Start()
        {
            lastPosition = transform.position;
        }

        public void Move(Vector2 direction)
        {
            transform.position = RaycastUtils.MoveCharacter(gameObject, direction, boxCollider, collisionLayers);

            if (direction.x < 0)
            {
                this.direction = Direction.Left;
            }
            else if (direction.x > 0)
            {
                this.direction = Direction.Right;
            }
            else if (direction.y > 0)
            {
                this.direction = Direction.Top;
            }
            else if (direction.y < 0)
            {
                this.direction = Direction.Bottom;
            }

            UpdateTargets();
        }

        private void UpdateTargets()
        {
            var directionVector = new Vector2(transform.position.x - lastPosition.x, transform.position.y - lastPosition.y).normalized;

            if (directionVector.x < 0)
            {
                direction = Direction.Left;
            }
            else if (directionVector.x > 0)
            {
                direction = Direction.Right;
            }
            else if (directionVector.y > 0)
            {
                direction = Direction.Top;
            }
            else if (directionVector.y < 0)
            {
                direction = Direction.Bottom;
            }

            float distance = 0;

            switch (direction)
            {
                case Direction.Top:
                case Direction.Bottom:

                    distance = boxCollider.size.y / 2 * (direction == Direction.Top ? this.distance : -this.distance) * boxCollider.transform.lossyScale.y;

                    targetedObject = RaycastUtils.RaycastCharacterVertical(gameObject, transform.position, boxCollider, distance, triggerLayers, verticalRays)
                        .Where(x => x != null)
                        .FirstOrDefault();

#if USING_UNITY_TILEMAPS
                    if (targetedObject != null && targetedObject.GetComponent<Tilemap>())
                    {
                        targetedObject = null;
                    }
#endif

                    if (targetedObject != null)
                    {
                        targetedCharacter = targetedObject.GetComponent<CharacterMovement>();
                    }
                    else
                    {
                        targetedObject = RaycastUtils.RaycastCharacterVertical(gameObject, transform.position, boxCollider, distance, collisionLayers, verticalRays)
                            .Where(x => x != null)
                            .FirstOrDefault();

#if USING_UNITY_TILEMAPS
                        if (targetedObject != null && targetedObject.GetComponent<Tilemap>())
                        {
                            targetedObject = null;
                        }
#endif

                        if (targetedObject != null)
                        {
                            targetedCharacter = targetedObject.GetComponent<CharacterMovement>();
                        }
                    }

                    break;

                case Direction.Left:
                case Direction.Right:

                    distance = boxCollider.size.x / 2 * (direction == Direction.Right ? this.distance : -this.distance) * boxCollider.transform.lossyScale.x;

                    targetedObject = RaycastUtils.RaycastCharacterHorizontal(gameObject, transform.position, boxCollider, distance, triggerLayers, horizontalRays)
                        .Where(x => x != null)
                        .FirstOrDefault();

#if USING_UNITY_TILEMAPS
                    if (targetedObject != null && targetedObject.GetComponent<Tilemap>())
                    {
                        targetedObject = null;
                    }
#endif

                    if (targetedObject != null)
                    {
                        targetedCharacter = targetedObject.GetComponent<CharacterMovement>();
                    }
                    else
                    {
                        targetedObject = RaycastUtils.RaycastCharacterHorizontal(gameObject, transform.position, boxCollider, distance, collisionLayers, horizontalRays)
                            .Where(x => x != null)
                            .FirstOrDefault();

#if USING_UNITY_TILEMAPS
                        if (targetedObject != null && targetedObject.GetComponent<Tilemap>())
                        {
                            targetedObject = null;
                        }
#endif

                        if (targetedObject != null)
                        {
                            targetedCharacter = targetedObject.GetComponent<CharacterMovement>();
                        }
                    }

                    break;
            }
        }

        private void Update()
        {
            OnBeforeUpdate?.Invoke();

            UpdateTargets();

            OnUpdate?.Invoke();

            lastPosition = transform.position;

            OnAfterUpdate?.Invoke();
        }
    }
}
