﻿using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class TouchInputFilter : MonoBehaviour
        {
            private bool isMouse = false;
            private bool mousePressing = false;
            private Vector2 lastTouchPosition = Vector2.zero;
            private Vector2 currentTouchPosition = Vector2.zero;
            private bool movedLastFrame = false;
            private TouchPhase lastTouchPhase = TouchPhase.Canceled;

            [HideInInspector]
            public bool hasTouch = false;

            public TouchPhase phase
            {
                get
                {
                    return lastTouchPhase;
                }
            }

            public Vector2 position
            {
                get
                {
                    return currentTouchPosition;
                }
            }

            public Vector2 relativePosition
            {
                get
                {
                    return currentTouchPosition - lastTouchPosition;
                }
            }

            public Vector2 lastPosition
            {
                get
                {
                    return lastTouchPosition;
                }
            }

            public bool pressing
            {
                get
                {
                    return mousePressing;
                }
            }

            public bool moved
            {
                get
                {
                    return movedLastFrame;
                }
            }

            public static TouchInputFilter instance;

            private void Start()
            {
                if (instance != null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    instance = this;

                    DontDestroyOnLoad(gameObject);
                }

#if UNITY_EDITOR
                isMouse = true;
#endif

                if (!Application.isMobilePlatform)
                {
                    isMouse = true;
                }
            }

            private void Update()
            {
                hasTouch = false;

                lastTouchPosition = currentTouchPosition;

                if (isMouse)
                {
                    bool setTouchPhase = false;
                    Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                    if (mousePressing != Input.GetMouseButton(0))
                    {
                        hasTouch = true;
                        setTouchPhase = true;
                        lastTouchPhase = Input.GetMouseButtonDown(0) ? TouchPhase.Began : TouchPhase.Ended;

                        if (mousePressing)
                        {
                            currentTouchPosition = Input.mousePosition;
                        }
                    }

                    mousePressing = Input.GetMouseButton(0);

                    movedLastFrame = mousePressing && mousePosition == currentTouchPosition;

                    if (mousePressing)
                    {
                        if (mousePosition != currentTouchPosition)
                        {
                            currentTouchPosition = Input.mousePosition;

                            if (setTouchPhase == false)
                            {
                                hasTouch = true;
                                setTouchPhase = true;
                                lastTouchPhase = TouchPhase.Moved;
                            }
                        }
                        else if (setTouchPhase == false)
                        {
                            hasTouch = true;
                            setTouchPhase = true;
                            lastTouchPhase = TouchPhase.Stationary;
                        }
                    }
                }
                else
                {
                    if (Input.touches.Length > 0)
                    {
                        hasTouch = true;

                        Touch touch = Input.touches[0];

                        movedLastFrame = touch.position != currentTouchPosition;
                        lastTouchPhase = touch.phase;
                        currentTouchPosition = touch.position;

                        if (touch.phase == TouchPhase.Began)
                        {
                            mousePressing = true;
                        }
                        else if (touch.phase == TouchPhase.Ended)
                        {
                            mousePressing = false;
                        }
                    }
                }
            }
        }
    }
}
