﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class RigidBodyCharacterController : MonoBehaviour
    {
        public Transform Camera = null;

        public bool UseGamepad = false;

        public bool MovementRelative = true;

        public float MovementSpeed = 6f;

        public float RunSpeedMultiplier = 2.0f;

        public bool RotationEnabled = true;

        public bool RotateToInput = false;

        public float RotationSpeed = 180f;

        protected Transform mTransform = null;

        protected Rigidbody mRigidBody;

        public void Awake()
        {
            mTransform = gameObject.transform;
            mRigidBody = GetComponent<Rigidbody>();
        }

        public void FixedUpdate()
        {
            if (RotationEnabled)
            {
                float lYaw = (UnityEngine.Input.GetKey(KeyCode.E) ? 1f : 0f);
                lYaw = lYaw - (UnityEngine.Input.GetKey(KeyCode.Q) ? 1f : 0f);

                if (lYaw != 0f)
                {
                    var rotation = Quaternion.AngleAxis(lYaw * RotationSpeed * Time.fixedDeltaTime, Vector3.up);

                    if (mRigidBody != null)
                    {
                        mRigidBody.MoveRotation(mRigidBody.rotation * rotation);
                    }
                    else
                    {
                        mTransform.rotation = mTransform.rotation * rotation;
                    }
                }
            }

            Vector3 lMovement = Vector3.zero;

            lMovement.z = (UnityEngine.Input.GetKey(KeyCode.W) || UnityEngine.Input.GetKey(KeyCode.UpArrow) ? 1f : 0f);
            lMovement.z = lMovement.z - (UnityEngine.Input.GetKey(KeyCode.S) || UnityEngine.Input.GetKey(KeyCode.DownArrow) ? 1f : 0f);

            lMovement.x = (UnityEngine.Input.GetKey(KeyCode.D) || UnityEngine.Input.GetKey(KeyCode.RightArrow) ? 1f : 0f);
            lMovement.x = lMovement.x - (UnityEngine.Input.GetKey(KeyCode.A) || UnityEngine.Input.GetKey(KeyCode.LeftArrow) ? 1f : 0f);

            if (UseGamepad && lMovement.x == 0f && lMovement.z == 0f)
            {
                lMovement.z = UnityEngine.Input.GetAxis("Vertical");
                lMovement.x = UnityEngine.Input.GetAxis("Horizontal");
            }

            if (RotateToInput && Camera != null && lMovement.sqrMagnitude > 0f)
            {
                Quaternion lCameraRotation = Quaternion.Euler(0f, Camera.rotation.eulerAngles.y, 0f);

                var rotation = Quaternion.LookRotation(lCameraRotation * lMovement, Vector3.up); ;

                if (mRigidBody != null)
                {
                    mRigidBody.MoveRotation(rotation);
                }
                else
                {
                    mTransform.rotation = rotation;
                }

                lMovement.z = lMovement.magnitude;
                lMovement.x = 0f;
            }

            if (lMovement.magnitude >= 1f) { lMovement.Normalize(); }
            if (MovementRelative) { lMovement = mTransform.rotation * lMovement; }

            var movement = (lMovement.normalized * (MovementSpeed * ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) ? RunSpeedMultiplier : 1.0f)));

            if (mRigidBody != null)
            {
                var differences = new Vector3(movement.x, mRigidBody.velocity.y, movement.z) - mRigidBody.velocity;

                mRigidBody.AddForce(new Vector3(differences.x, 0, 0), ForceMode.VelocityChange);
                mRigidBody.AddForce(new Vector3(0, 0, differences.z), ForceMode.VelocityChange);
                //mRigidBody.MovePosition(mRigidBody.position + movement);
            }
            else
            {
                mTransform.position = mTransform.position + movement;
            }
        }
    }
}
