﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts.AssetBundleFileSystem
{
    public class AssetBundleFileSystemInitializer : MonoBehaviour
    {
        public bool fetchLoadOrder = true;

        private void Awake()
        {
            AssetBundleFileSystemManager.instance.Initialize(fetchLoadOrder);
        }
    }
}
