﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using FlamingTorchGames.CommonScripts.AssetBundleFileSystem;

namespace FlamingTorchGames.CommonScripts
{
    public static class AssetBundlePipelineEditorUtils
    {
        [MenuItem("Flaming Torch Games/Asset Bundle Pipeline/Build Bundles")]
        public static void BuildBundles()
        {
            try
            {
                Directory.Delete(Path.Combine(Application.streamingAssetsPath, "Assets"), true);
            }
            catch (System.Exception)
            {
            }

            try
            {
                Directory.CreateDirectory(Path.Combine(Application.streamingAssetsPath, "Assets"));
            }
            catch (System.Exception)
            {
            }

            var path = Path.Combine(Application.streamingAssetsPath, "Assets");

#if ASSET_BUNDLE_FILESYSTEM_CI
            path = Path.Combine(path, AssetBundleFileSystemManager.PlatformFolderName);

            try
            {
                Directory.CreateDirectory(path);
            }
            catch (System.Exception)
            {
            }
#endif

            BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
        }
    }
}
