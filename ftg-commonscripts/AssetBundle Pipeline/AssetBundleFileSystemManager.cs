﻿#if USING_NEWTONSOFT_JSON
using Newtonsoft.Json;
#endif
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts.AssetBundleFileSystem
{
    public class AssetBundleFileSystemManager : MonoBehaviour
    {
        [System.Serializable]
        public class ManifestInfo
        {
            public List<string> filesList = new List<string>();
        }

        [System.Serializable]
        public class BundleInfo
        {
            public AssetBundle bundle;
            public string localPath;
            public ManifestInfo manifest;
        }

        private class FileInfo
        {
            public string path;
            public string localFilePath;
            public BundleInfo bundle;
        }

        public List<BundleInfo> loadedBundles = new List<BundleInfo>();

        public string[] loadOrder = new string[0];
        public bool useLocalFiles = false;

        private List<FileInfo> files = new List<FileInfo>();
        private List<string> localFileDirectories = new List<string>();
        private const string contentPath = "Assets/";
        private const string localContentPath = "Assets/AssetBundles/";
        private const string loadOrderPath = "packages.list";
        private bool initialized = false;

        public static string PlatformFolderName
        {
            get
            {
#if UNITY_EDITOR
                switch(UnityEditor.EditorUserBuildSettings.activeBuildTarget)
                {
                    case UnityEditor.BuildTarget.Android:

                        return "Android";

                    case UnityEditor.BuildTarget.iOS:

                        return "iOS";

                    case UnityEditor.BuildTarget.StandaloneLinux64:

                        return "Linux";

                    case UnityEditor.BuildTarget.StandaloneOSX:

                        return "OSX";

                    case UnityEditor.BuildTarget.StandaloneWindows64:

                        return "Windows";
                }
#else
                switch(Application.platform)
                {
                    case RuntimePlatform.Android:

                        return "Android";

                    case RuntimePlatform.IPhonePlayer:

                        return "iOS";

                    case RuntimePlatform.LinuxPlayer:

                        return "Linux";

                    case RuntimePlatform.OSXPlayer:

                        return "OSX";

                    case RuntimePlatform.WindowsPlayer:

                        return "Windows";
                }
#endif

                return "";
            }
        }

        private static AssetBundleFileSystemManager privInstance;
        public static AssetBundleFileSystemManager instance
        {
            get
            {
                if(privInstance != null)
                {
                    return privInstance;
                }

                var gameObject = new GameObject("Asset Bundle FS Manager");

#if UNITY_EDITOR
                gameObject.hideFlags = HideFlags.HideAndDontSave;
#else
                DontDestroyOnLoad(gameObject);
#endif

                privInstance = gameObject.AddComponent<AssetBundleFileSystemManager>();

                return privInstance;
            }
        }

        public void Initialize(bool fetchLoadOrder)
        {
            if(loadedBundles.Any(x => x.bundle == null))
            {
                initialized = false;

                AssetBundle.UnloadAllAssetBundles(true);

                files = new List<FileInfo>();
                loadedBundles = new List<BundleInfo>();
            }

            if (initialized)
            {
                return;
            }

            Debug.Log("Initializing asset bundle filesystem");

            initialized = true;

            if(fetchLoadOrder)
            {
                loadOrder = new string[0];

                try
                {
                    var url = string.Format("{0}/{1}", Application.persistentDataPath, loadOrderPath);

                    var text = File.ReadAllText(url);

                    if (text != null && text.Length > 0)
                    {
                        loadOrder = text.Split("\n".ToCharArray())
                            .Select(x => x.Trim())
                            .ToArray();
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.ToString());
                }

                if (loadOrder.Length == 0)
                {
                    try
                    {
                        var url = string.Format("{0}/{1}", Application.streamingAssetsPath, loadOrderPath);

                        if (Application.platform == RuntimePlatform.IPhonePlayer ||
                           Application.platform == RuntimePlatform.OSXPlayer ||
                           Application.platform == RuntimePlatform.OSXEditor ||
                           Application.platform == RuntimePlatform.LinuxEditor ||
                           Application.platform == RuntimePlatform.LinuxPlayer)
                        {
                            url = string.Format("file://{0}", url);
                        }

                        var webRequest = UnityWebRequest.Get(url);

                        var operation = webRequest.SendWebRequest();

                        while (!operation.isDone) ;

                        if (!operation.webRequest.isNetworkError)
                        {
                            var text = operation.webRequest.downloadHandler.text;

                            if (text != null && text.Length > 0)
                            {
                                loadOrder = text.Split("\n".ToCharArray())
                                    .Select(x => x.Trim())
                                    .ToArray();
                            }
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError(e.ToString());
                    }
                }
            }

            foreach (var path in loadOrder)
            {
                var loaded = false;

                try
                {
                    var localPath = string.Format("{0}/{1}{2}/{2}", Application.persistentDataPath, contentPath, path);

                    loaded = LoadBundle(localPath);
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.ToString());
                }

                if (!loaded)
                {
                    try
                    {
#if ASSET_BUNDLE_FILESYSTEM_CI
                        var localPath = string.Format("{0}/{1}{2}/{3}/{3}", Application.streamingAssetsPath, contentPath, PlatformFolderName, path);
#else
                        var localPath = string.Format("{0}/{1}/{2}/{2}", Application.streamingAssetsPath, contentPath, path);
#endif

                        loaded = LoadBundle(localPath);
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError(e.ToString());
                    }
                }
            }

            foreach (var bundle in loadedBundles)
            {
                foreach (var file in bundle.manifest.filesList)
                {
                    var existingFile = files.FirstOrDefault(x => x.path == file);

                    if (existingFile != null)
                    {
                        existingFile.bundle = bundle;
                    }
                    else
                    {
                        files.Add(new FileInfo()
                        {
                            bundle = bundle,
                            path = file,
                        });
                    }
                }
            }

            Debug.LogFormat("[AssetBundleFileSystem] Loaded {0} asset bundles", loadedBundles.Count);
        }

        public T LoadAsset<T>(string path) where T : Object
        {
            path = path.ToLowerInvariant();

            var file = files.FirstOrDefault(x => x.path == path);

            if (file == null)
            {
                return default;
            }

            if(file.bundle == null)
            {
                var bytes = new byte[0];

                try
                {
                    bytes = File.ReadAllBytes(file.localFilePath);
                }
                catch (System.Exception)
                {
                    return default;
                }

                if (typeof(T) == typeof(Texture2D))
                {
                    var texture = new Texture2D(1, 1);

                    if (!texture.LoadImage(bytes))
                    {
                        return default;
                    }

                    return texture as T;
                }
                else if (typeof(T) == typeof(Sprite))
                {
                    var texture = new Texture2D(1, 1);

                    if (!texture.LoadImage(bytes))
                    {
                        return default;
                    }

                    var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);

                    if (sprite == null)
                    {
                        return default;
                    }

                    return sprite as T;
                }
                else
                {
                    return default;
                }
            }
            else
            {

                try
                {
                    return file.bundle.bundle.LoadAsset<T>(string.Format("{0}{1}", file.bundle.localPath, path));
                }
                catch (System.Exception e)
                {
                    try
                    {
                        UnityThreadHelper.Dispatcher.Dispatch(() =>
                        {
                            Debug.LogError($"Failed to load asset bundle filesystem asset {path}: {e}");
                        });
                    }
                    catch (System.Exception)
                    {
                        Debug.LogError($"Failed to load asset bundle filesystem asset {path}: {e}");
                    }

                    return default;
                }
            }
        }

        public void LoadAssetAsync<T>(string path, System.Action<T> onFinish) where T : Object
        {
            path = path.ToLowerInvariant();
            StartCoroutine(LoadAssetAsyncCoroutine(path, onFinish));
        }

        private IEnumerator LoadAssetAsyncCoroutine<T>(string path, System.Action<T> onFinish) where T : Object
        {
            var file = files.FirstOrDefault(x => x.path == path);

            if (file == null)
            {
                onFinish(default);

                yield break;
            }

            if(file.bundle == null)
            {
                var bytes = new byte[0];

                try
                {
                    bytes = File.ReadAllBytes(file.localFilePath);
                }
                catch(System.Exception)
                {
                    onFinish(default);

                    yield break;
                }

                if(typeof(T) == typeof(Texture2D))
                {
                    var texture = new Texture2D(1, 1);

                    if(!texture.LoadImage(bytes))
                    {
                        onFinish(default);

                        yield break;
                    }

                    onFinish(texture as T);
                }
                else if(typeof(T) == typeof(Sprite))
                {
                    var texture = new Texture2D(1, 1);

                    if (!texture.LoadImage(bytes))
                    {
                        onFinish(default);

                        yield break;
                    }

                    var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);

                    if(sprite == null)
                    {
                        onFinish(default);

                        yield break;
                    }

                    onFinish(sprite as T);
                }
                else
                {
                    onFinish(default);
                }
            }
            else
            {
                AssetBundleRequest request = null;

                try
                {
                    request = file.bundle.bundle.LoadAssetAsync<T>(string.Format("{0}{1}", file.bundle.localPath, path));
                }
                catch (System.Exception e)
                {
                    UnityThreadHelper.Dispatcher.Dispatch(() =>
                    {
                        Debug.LogError($"Failed to load asset bundle filesystem asset {path}: {e}");
                    });

                    onFinish(default);

                    yield break;
                }

                yield return request;

                onFinish((T)request.asset);
            }
        }

        public string[] FindFiles(string filter)
        {
            var pattern = string.Format("^{0}$", Regex.Escape(filter.ToLowerInvariant()).Replace("\\*", ".*"));

            var outFiles = new List<string>();

            foreach(var file in files)
            {
                if(Regex.IsMatch(file.path, pattern))
                {
                    outFiles.Add(file.path);
                }
            }

            return outFiles.ToArray();
        }

        private bool LoadBundle(string path)
        {
            var fileName = Path.GetFileName(path);

            var localPath = string.Format("{0}/{1}/", localContentPath, fileName).Replace("//", "/");

            AssetBundle assetBundle = null;

            try
            {
                assetBundle = AssetBundle.LoadFromFile(path);
            }
            catch (System.Exception)
            {
                Debug.LogErrorFormat("[AssetBundleFileSystem] Failed to asset bundle {0}", fileName);

                return false;
            }

            var manifestPath = string.Format("{0}AssetManifest.json", localPath);

            var manifestAsset = assetBundle.LoadAsset<TextAsset>(manifestPath);

            if (manifestAsset == null)
            {
                Debug.LogErrorFormat("[AssetBundleFileSystem] AssetManifest.json not found for {0}", fileName);

                assetBundle.Unload(true);

                return false;
            }

            ManifestInfo manifest = null;

            try
            {
#if USING_NEWTONSOFT_JSON
                manifest = JsonConvert.DeserializeObject<ManifestInfo>(manifestAsset.text);
#else
                manifest = JsonUtility.FromJson<ManifestInfo>(manifestAsset.text);
#endif
            }
            catch(System.Exception)
            {
            }

            if (manifest == null)
            {
                Debug.LogErrorFormat("[AssetBundleFileSystem] Failed to load manifest for {0}", fileName);

                assetBundle.Unload(true);

                return false;
            }

            localPath = localPath.ToLowerInvariant();

            var expandedFileList = new List<string>();

            foreach(var filePath in manifest.filesList)
            {
                var pattern = string.Format("^{0}$", Regex.Escape(filePath.ToLowerInvariant()).Replace("\\*", ".*"));

                foreach(var file in assetBundle.GetAllAssetNames())
                {
                    var localFile = file.Replace(localPath, "");

                    if(Regex.IsMatch(localFile, pattern))
                    {
                        expandedFileList.Add(localFile);
                    }
                }
            }

            manifest.filesList = expandedFileList;

            loadedBundles.Add(new BundleInfo()
            {
                bundle = assetBundle,
                localPath = localPath,
                manifest = manifest,
            });

            Debug.LogFormat("[AssetBundleFileSystem] Loaded {0}", fileName);

            return true;
        }

        public void Unload(bool forceUnload = true)
        {
            var shouldUnloadAll = false;

            try
            {
                foreach (var bundle in loadedBundles)
                {
                    bundle.bundle.Unload(forceUnload);
                }
            }
            catch(System.Exception)
            {
                shouldUnloadAll = true;
            }

            loadedBundles = new List<BundleInfo>();
            files = new List<FileInfo>();
            initialized = false;

            if(shouldUnloadAll)
            {
                var assetBundles = AssetBundle.GetAllLoadedAssetBundles();

                foreach(var bundle in assetBundles)
                {
                    bundle.Unload(forceUnload);
                }
            }
        }

        public void SetLocalFileDirectories(List<string> fileDirectories)
        {
            localFileDirectories = fileDirectories;

            RefreshLocalFiles();
        }

        public void RefreshLocalFiles()
        {
            if(!useLocalFiles)
            {
                return;
            }

            files = files.Where(x => x.bundle != null).ToList();

            foreach (var filePath in localFileDirectories)
            {
                void IterateFiles(string path)
                {
                    try
                    {
                        var foundFiles = Directory.GetFiles(path);

                        if (foundFiles.Length != 0)
                        {
                            foreach (var file in foundFiles)
                            {
                                var newPath = file.Substring(filePath.Length).ToLowerInvariant();

                                files.Add(new FileInfo()
                                {
                                    localFilePath = file,
                                    path = newPath,
                                });
                            }
                        }

                        var directories = Directory.GetDirectories(path);

                        foreach (var directory in directories)
                        {
                            IterateFiles(directory);
                        }
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
        }

        private void OnDestroy()
        {
            Unload();
        }
    }
}
