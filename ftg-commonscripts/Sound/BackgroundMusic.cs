﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FlamingTorchGames.CommonScripts.AssetBundleFileSystem;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class BackgroundMusic : MonoBehaviour
        {
            [Serializable]
            public class ClipInfo
            {
                public string name;
                public string path;
                public AudioClip clip;
            }

            public List<ClipInfo> clips = new List<ClipInfo>();
            public string startingClip;
            public AudioSource audioSource;
            public bool useAssetBundleFileSystemManager = false;

            public float targetVolume = 1;

            private ClipInfo activeClip;
            private Coroutine currentCoroutine;

            public float Volume
            {
                get
                {
                    return audioSource?.volume ?? 0;
                }

                set
                {
                    if (audioSource != null)
                    {
                        audioSource.volume = value;
                    }
                }
            }

            public bool IsPlaying
            {
                get
                {
                    return audioSource?.isPlaying ?? false;
                }
            }

            public static BackgroundMusic Instance
            {
                get;
                private set;
            }

            private void Awake()
            {
                if(Instance != null)
                {
                    Destroy(gameObject);

                    return;
                }

                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

            public void Initialize()
            {
                foreach (var clip in clips)
                {
                    if (useAssetBundleFileSystemManager)
                    {
                        clip.clip = AssetBundleFileSystemManager.instance.LoadAsset<AudioClip>(clip.path);

                        if(clip.clip == null)
                        {
                            Debug.LogError($"[BackgroundMusic] Unable to load clip at {clip.path}");
                        }
                    }
                    else if(clip.clip == null)
                    {
                        clip.clip = Resources.Load<AudioClip>(clip.path);
                    }
                }

                Play(startingClip, 1);
            }

            /// <summary>
            /// Pauses the BGM or resumes it if it's paused
            /// </summary>
            public void Pause()
            {
                if (audioSource.isPlaying)
                {
                    audioSource.Pause();
                }
                else
                {
                    audioSource.UnPause();
                }
            }

            /// <summary>
            /// Plays a sound with a specific resource
            /// </summary>
            /// <param name="resourceName">The resource to load</param>
            /// <returns>Whether we're playing the sound</returns>
            public bool Play(string name, float fadeTime = 0)
            {
                if(audioSource.isPlaying && fadeTime > 0)
                {
                    FadeOut(fadeTime, () =>
                    {
                        audioSource.Stop();
                        Play(name, fadeTime);
                    });

                    return true;
                }

                audioSource.Stop();

                activeClip = clips.FirstOrDefault(x => x.name == name);

                if (activeClip != null && activeClip.clip != null)
                {
                    audioSource.clip = activeClip.clip;

                    if(fadeTime > 0)
                    {
                        audioSource.volume = 0;
                        audioSource.Play();
                        FadeIn(fadeTime, null);
                    }
                    else
                    {
                        Volume = targetVolume;
                        audioSource.Play();
                    }

                    return true;
                }

                return false;
            }

            /// <summary>
            /// Stops the currently played sound
            /// </summary>
            public void Stop()
            {
                audioSource.Stop();
            }

            /// <summary>
            /// Fades in a currently playing sound
            /// </summary>
            /// <param name="fadeSpeed">The time in seconds it takes for the fading to finish</param>
            /// <param name="onCompleteAction">Optional action to run when completed</param>
            public void FadeIn(float fadeSpeed, Action onCompleteAction)
            {
                if (currentCoroutine != null)
                {
                    StopCoroutine(currentCoroutine);

                    currentCoroutine = null;
                }

                StartCoroutine(DoFadeIn(fadeSpeed, onCompleteAction));
            }

            /// <summary>
            /// Fades out a currently playing sound
            /// </summary>
            /// <param name="fadeSpeed">The time in seconds it takes for the fading to finish</param>
            /// <param name="onCompleteAction">Optional action to run when completed</param>
            public void FadeOut(float fadeSpeed, Action onCompleteAction)
            {
                if (currentCoroutine != null)
                {
                    StopCoroutine(currentCoroutine);

                    currentCoroutine = null;
                }

                StartCoroutine(DoFadeOut(fadeSpeed, onCompleteAction));
            }

            private IEnumerator DoFadeIn(float fadeSpeed, Action onCompleteAction)
            {
                float t = 0;
                bool passedBarrier = false;

                for (;;)
                {
                    Volume = t;

                    if (passedBarrier)
                    {
                        break;
                    }

                    t = Mathf.Clamp(t + Time.deltaTime / fadeSpeed, 0, targetVolume);

                    if (t == targetVolume)
                    {
                        passedBarrier = true;
                    }

                    yield return new WaitForEndOfFrame();
                }

                if (onCompleteAction != null)
                {
                    onCompleteAction();
                }
            }

            private IEnumerator DoFadeOut(float fadeSpeed, Action onCompleteAction)
            {
                float t = targetVolume;
                bool passedBarrier = false;

                for (;;)
                {
                    Volume = t;

                    if (passedBarrier)
                    {
                        break;
                    }

                    t = Mathf.Clamp(t - Time.deltaTime / fadeSpeed, 0, 1);

                    if (t == 0)
                    {
                        passedBarrier = true;
                    }

                    yield return new WaitForEndOfFrame();
                }

                if (onCompleteAction != null)
                {
                    onCompleteAction();
                }
            }
        }
    }
}
